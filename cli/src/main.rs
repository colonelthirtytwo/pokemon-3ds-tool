#[macro_use]
extern crate log;
extern crate pokemon_3ds;
extern crate simplelog;
extern crate structopt;

use std::{
	cell::RefCell,
	fs,
	io,
	path,
	process::exit,
	rc::Rc,
	str::FromStr,
};
use structopt::StructOpt;

use pokemon_3ds::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::{
		self,
		visitor::{
			ExportVisitor,
			ResourceVisitorRunner,
		},
	},
};

#[derive(StructOpt, Debug)]
struct Opt {
	/// Silence log messages
	#[structopt(short = "q")]
	quiet: bool,

	#[structopt(subcommand)]
	cmd: Command,
}

#[derive(StructOpt, Debug)]
enum Command {
	/// Shows information about a file
	#[structopt(name = "info")]
	Info {
		/// File to read
		#[structopt(name = "INFILE", parse(from_os_str))]
		path: path::PathBuf,
	},
	/// Converts a file.
	///
	/// Models and animations are converted to SMD files. Textures are converted
	/// to PNGs. Containers are extracted and converted recursively.
	#[structopt(name = "convert")]
	Convert {
		/// File to read
		#[structopt(name = "INFILE", parse(from_os_str))]
		input_path: path::PathBuf,

		/// File or directory to write to
		#[structopt(name = "OUTPATH", parse(from_os_str))]
		output_path: path::PathBuf,
	},

	/// Converts pokemon models.
	///
	/// Pokemon data spans multiple files. Each pokemon has one model file, one or more
	/// texture files, and one or more animation files, all in consecutive files.
	/// This command assembles that data, converts it into standard formats,
	/// and writes one folder per pokemon.
	#[structopt(name = "convert-pokemon")]
	ConvertPokemon {
		/// File or directory to read
		///
		/// Either a container file or directory of pokemon.
		#[structopt(name = "INFILE", parse(from_os_str))]
		input_path: path::PathBuf,

		/// Directory to write pokemon data to
		#[structopt(name = "OUTPATH", parse(from_os_str))]
		output_path: path::PathBuf,
	},
}

/// Creates a directory at `path` but don't return a failure if it already exists.
fn ensure_dir<P: AsRef<path::Path>>(path: P) -> io::Result<()> {
	let result = fs::create_dir(path);
	match result {
		Err(ref err) if err.kind() == io::ErrorKind::AlreadyExists => Ok(()),
		_ => result,
	}
}

fn real_main() -> ::std::result::Result<(), String> {
	let opt = Opt::from_args();

	simplelog::TermLogger::init(simplelog::LevelFilter::Info, simplelog::Config::default())
		.expect("Could not initialize logging");

	match opt.cmd {
		Command::Info { path } => {
			let file = fs::File::open(path).map_err(|e| format!("Could not open file: {}", e))?;
			let mut reader = io::BufReader::new(file);

			let format_opt = formats::identify(&mut reader)
				.map_err(|e| format!("Could not read file: {}", e))?;

			let format = match format_opt {
				Some(format) => format,
				None => {
					eprintln!("Unrecognized format");
					exit(2);
				}
			};

			println!("Format: {}", format);
			match format {
				formats::Format::Container(typ) => {
					let container = typ
						.read(Rc::new(RefCell::new(reader)))
						.map_err(|e| format!("Could not parse container: {}", e))?;
					let metadata = container.metadata();
					if &metadata != "" {
						println!("{}", metadata);
					}

					println!("Contents:");
					for entry in container.entries().iter() {
						println!("\t{} ({} bytes)", entry.path, entry.size);
					}
				}
				formats::Format::Model(typ) => {
					let model = typ
						.read(&mut reader)
						.map_err(|e| format!("Could not parse model: {}", e))?;

					println!("Mesh info:");
					println!("\tVertices: {}", model.mesh().num_vertices());
					println!("\tTriangles: {}", model.mesh().faces().len());
					println!("Bones:");
					for bone in model.bones().iter() {
						println!("\t{}", bone.name);
					}
					println!("Materials:");
					for material in model.materials().iter() {
						println!("\t{}", material.name);
					}
				}
				formats::Format::Texture(typ) => {
					let texture = typ
						.read(&mut reader)
						.map_err(|e| format!("Could not parse texture: {}", e))?;

					println!("Name: {}", texture.name());
					println!("Size: {}x{}", texture.width(), texture.height());
					println!(
						"Format: {}",
						match texture.data() {
							formats::textures::TextureData::RGB(_) => "RGB",
							formats::textures::TextureData::RGBA(_) => "RGBA",
							formats::textures::TextureData::Grayscale(_) => "Grayscale",
							formats::textures::TextureData::GrayscaleAlpha(_) =>
								"Grayscale + Alpha",
						}
					);
				}
				formats::Format::Animation(typ) => {
					let animation = typ
						.read(&mut reader)
						.map_err(|e| format!("Could not parse animation: {}", e))?;

					println!("Name: {}", animation.name);
					println!("Frames: {}", animation.last_frame());
					println!("Bones:");
					for bone in animation.bones.iter() {
						println!("\t{}", bone.name());
					}
				}
			};
		}
		Command::Convert {
			input_path,
			output_path,
		} => {
			let file =
				fs::File::open(&input_path).map_err(|e| format!("Could not open file: {}", e))?;

			let visitor = ExportVisitor::new(output_path);
			let mut runner = ResourceVisitorRunner::new(visitor, io::BufReader::new(file));
			runner
				.read_all()
				.map_err(|e| format!("Could not convert: {}", e))?;
		}
		Command::ConvertPokemon {
			input_path,
			output_path,
		} => {
			ensure_dir(&output_path).map_err(|e| {
				format!(
					"Could not create output directory {}: {}",
					output_path.display(),
					e
				)
			})?;

			if input_path.is_dir() {
				// Get files and sort by integer order
				let mut files: Vec<(path::PathBuf, u32)> = input_path
					.read_dir()
					.and_then(|iter| {
						iter.map(|res| {
							res.and_then(|entry| {
								let path = entry.path();
								let integer = {
									let filename = path.file_stem().unwrap();
									filename
										.to_str()
										.and_then(|name| u32::from_str(name).ok())
										.ok_or_else(|| {
											io::Error::new(
												io::ErrorKind::Other,
												format!(
													"file {} has non-numeric name",
													path.display()
												),
											)
										})?
								};
								Ok((path, integer))
							})
						})
						.collect::<io::Result<Vec<_>>>()
					})
					.map_err(|e| format!("Could not read {}: {}", input_path.display(), e))?;

				files.sort_unstable_by_key(|(_path, int)| *int);

				let entries_iter = files.into_iter().map(|(path, _int)| {
					let file = fs::File::open(&path)?;
					let reader = io::BufReader::new(file);
					Ok((path.display().to_string(), reader))
				});
				convert_pokemon(entries_iter, &output_path).map_err(|e| format!("{}", e))?;
			} else {
				// TODO: extract container
				unimplemented!();
			}
		}
	}
	Ok(())
}

fn main() {
	match real_main() {
		Ok(()) => (),
		Err(msg) => {
			eprintln!("{}", msg);
			std::process::exit(1);
		}
	}
}

fn convert_pokemon<
	Reader: io::Read + io::Seek,
	Iter: Iterator<Item = io::Result<(String, Reader)>>,
>(
	iter: Iter,
	out_dir: &path::Path,
) -> Result<()> {
	let mut current_model: Option<formats::models::geom::Model> = None;
	let mut current_i = 0;
	let mut current_data_set = 0;
	for result in iter {
		let (name, mut reader) = result.map_err(|e| Error::from(e))?;
		debug!("Parsing file {}", name);
		let _loc = Location::new(format!("file {}", name));

		let format_opt = formats::identify(&mut reader)?;

		let container = match format_opt {
			Some(formats::Format::Container(format)) => {
				format.read(Rc::new(RefCell::new(reader)))?
			}
			Some(format) => {
				warn!(
					"Skipping file {}: expecting a container got type: {}",
					name, format
				);
				continue;
			}
			None => {
				warn!("Skipping file {}: unrecognized format", name);
				continue;
			}
		};

		if container.entries().len() == 0 {
			warn!("Skipping file {}: empty container", name);
			continue;
		}

		let mut first_entry_reader = io::Cursor::new(container.read_entry(0, 0, None)?);

		let first_entry_format_opt = formats::identify(&mut first_entry_reader)?;

		match first_entry_format_opt {
			Some(formats::Format::Model(format)) => {
				// Starting new pokemon
				current_i += 1;
				current_data_set = 0;
				info!("Parsing {}: new pokemon model: {}", name, current_i);

				let model = format.read(&mut first_entry_reader)?;

				let mut path = path::PathBuf::from(out_dir);
				path.push(format!("{:04}", current_i));
				ensure_dir(&path)?;

				path.push("model.smd");

				let file = fs::File::create(&path)?;
				let mut writer = io::BufWriter::new(file);

				formats::export::smd::write_smd_model(&mut writer, &model)?;

				current_model = Some(model);
			}
			_ => {
				// Add textures + animations
				let model = match current_model.as_ref() {
					Some(m) => m,
					None => {
						warn!(
							"Skipping animations file {}: haven't fount a pokemon model yet",
							name
						);
						continue;
					}
				};
				info!("Parsing {}", name);

				let mut data_folder = path::PathBuf::from(out_dir);
				data_folder.push(format!("{:04}", current_i));
				data_folder.push(format!("data-{:02}", current_data_set));
				current_data_set += 1;

				ensure_dir(&data_folder)?;

				for (entry_i, entry) in container.entries().iter().enumerate() {
					let entry_path = &entry.path;
					let _loc = Location::new(format!("path {}", entry_path));

					let data = container.read_entry(entry_i, 0, None)?;
					let mut data_reader = io::Cursor::new(data);

					let data_format_opt = formats::identify(&mut data_reader)?;

					match data_format_opt {
						Some(formats::Format::Texture(format)) => {
							let texture_res = format.read(&mut data_reader);

							if let Err(err) = texture_res {
								if err.is_io() {
									return Err(err);
								} else {
									warn!(
										"Skipping {} entry {}: Error while parsing:\n{}",
										name, entry_path, err
									);
									continue;
								}
							}
							let texture = texture_res.unwrap();

							let mut texture_path = data_folder.clone();
							texture_path.push(format!("{:02}-{}.png", entry_i, texture.name()));

							let file = fs::File::create(&texture_path)?;
							let mut writer = io::BufWriter::new(file);
							texture.write_png(&mut writer)?;
						}
						Some(formats::Format::Animation(format)) => {
							let anim_res = format.read(&mut data_reader);

							if let Err(err) = anim_res {
								if err.is_io() {
									return Err(err);
								} else {
									warn!(
										"Skipping {} animation entry {}: Error while parsing:\n{}",
										name, entry_path, err
									);
									continue;
								}
							}
							let anim = anim_res.unwrap();

							let mut anim_path = data_folder.clone();
							anim_path.push(format!("{:02}-{}.smd", entry_i, anim.name));

							let file = fs::File::create(&anim_path)?;
							let mut writer = io::BufWriter::new(file);

							formats::export::smd::write_smd_animation(
								&mut writer,
								&model.bones(),
								&anim,
							)?;
						}
						Some(format) => {
							warn!("Skipping file {} entry {}: not a texture or animation, type was: {}",
								name, entry.path, format);
							continue;
						}
						None => {
							warn!(
								"Skipping file {} entry {}: unrecognized format",
								name, entry.path
							);
							continue;
						}
					};
				}
			}
		}
	}
	Ok(())
}
