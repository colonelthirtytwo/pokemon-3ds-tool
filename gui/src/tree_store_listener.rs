use gtk::{
	self,
	prelude::*,
};
use id_tree::{
	self,
	NodeId,
};
use pokemon_3ds::{
	formats,
	tree,
};
use std::convert::TryFrom;

use crate::ui::size_to_string;

/// Tree store columns
const COLUMN_NAME: u32 = 0;
const COLUMN_FORMAT_ICON: u32 = 1;
const COLUMN_SIZE: u32 = 2;

pub struct TreeStoreListener {
	store: gtk::TreeStore,
}
impl TreeStoreListener {
	pub fn new(store: gtk::TreeStore) -> Self {
		TreeStoreListener { store }
	}
}

impl tree::ContainerTreeListener for TreeStoreListener {
	fn node_added(&mut self, tree: &id_tree::Tree<tree::NodeData>, node_id: &NodeId) {
		let node = tree.get(node_id).unwrap();
		let parent_iter = node
			.parent()
			.map(|parent| node_id_to_tree_path(tree, parent))
			.map(|path| {
				self.store
					.get_iter(&path)
					.expect("Parent not in tree store")
			});
		let row_iter = self.store.append(parent_iter.as_ref());
		self.store.set(
			&row_iter,
			&[COLUMN_FORMAT_ICON, COLUMN_NAME, COLUMN_SIZE],
			&[
				&icon_for_format(&node.data().format().into()),
				&node.data().entry().path.to_string(),
				&size_to_string(node.data().entry().size),
			],
		);
	}

	fn children_deleting(&mut self, tree: &id_tree::Tree<tree::NodeData>, node_id: &NodeId) {
		let iter = self
			.store
			.get_iter(&node_id_to_tree_path(tree, node_id))
			.expect("Node not in tree store");
		while let Some(child_iter) = self.store.iter_children(Some(&iter)) {
			self.store.remove(&child_iter);
		}
	}

	fn format_set(&mut self, tree: &id_tree::Tree<tree::NodeData>, node_id: &NodeId) {
		let node = tree.get(node_id).unwrap();
		let iter = self
			.store
			.get_iter(&node_id_to_tree_path(tree, node_id))
			.expect("Node not in tree store");
		self.store.set(
			&iter,
			&[COLUMN_FORMAT_ICON],
			&[&icon_for_format(&node.data().format().into())],
		);
	}
}

fn node_id_to_tree_path<T>(tree: &id_tree::Tree<T>, node_id: &NodeId) -> gtk::TreePath {
	let mut node_id = node_id;
	let mut path_indices = vec![];
	while let Some(parent) = tree.get(node_id).unwrap().parent() {
		let index = tree
			.get(parent)
			.unwrap()
			.children()
			.iter()
			.position(|n| n == node_id)
			.unwrap();
		path_indices.push(i32::try_from(index).unwrap());
		node_id = parent;
	}
	path_indices.push(0);
	path_indices.reverse();
	gtk::TreePath::new_from_indicesv(&path_indices)
}

fn icon_for_format(format: &Option<formats::Format>) -> &'static str {
	match format {
		None => "unrecognized-symbolic",
		Some(formats::Format::Container(_)) => "container-symbolic",
		Some(formats::Format::Model(_)) => "model-symbolic",
		Some(formats::Format::Texture(_)) => "texture-symbolic",
		Some(formats::Format::Animation(_)) => "animation-symbolic",
	}
}
