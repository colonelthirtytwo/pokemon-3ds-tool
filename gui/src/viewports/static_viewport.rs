use gtk;
use std::rc::Rc;

use crate::viewports::Viewport;

/// Static viewport, used for viewports that don't change content.
pub struct StaticViewport {
	root: gtk::Widget,
}
impl StaticViewport {
	pub fn new(builder: &gtk::Builder, name: &str) -> Rc<Self> {
		Rc::new(Self {
			root: builder.get_object(name).unwrap(),
		})
	}
}
impl Viewport for StaticViewport {
	fn root(&self) -> &gtk::Widget {
		&self.root
	}
}
