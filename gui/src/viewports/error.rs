use gtk::{
	self,
	LabelExt,
};
use std::rc::Rc;

use crate::viewports::Viewport;

pub struct ErrorViewport {
	root: gtk::Widget,
	message: gtk::Label,
}
impl ErrorViewport {
	pub fn new(builder: &gtk::Builder) -> Rc<Self> {
		Rc::new(Self {
			root: builder.get_object("viewport_parse_error").unwrap(),
			message: builder.get_object("parse_error_message").unwrap(),
		})
	}

	pub fn set(&self, string: &str) {
		self.message.set_text(string);
	}
}
impl Viewport for ErrorViewport {
	fn root(&self) -> &gtk::Widget {
		&self.root
	}
}
