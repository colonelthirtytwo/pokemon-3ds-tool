use gtk;

pub mod error;
pub mod model;
pub mod static_viewport;
pub mod texture;

pub use self::{
	error::ErrorViewport,
	model::ModelViewport,
	static_viewport::StaticViewport,
	texture::TextureViewport,
};

pub trait Viewport {
	fn root(&self) -> &gtk::Widget;
}
