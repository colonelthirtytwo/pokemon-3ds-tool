use gdk;
use gdk_pixbuf;
use glium::{
	self,
	Surface,
};
use gtk::{
	self,
	prelude::*,
};
use nalgebra::{
	self,
	Vector2,
	Vector3,
};
use pokemon_3ds::formats::models;
use std::{
	borrow::Cow,
	cell::{
		Cell,
		RefCell,
	},
	f32::consts::PI,
	rc::Rc,
};

use crate::{
	gtkgl,
	viewports::Viewport,
};

pub struct ModelViewport {
	root: gtk::Widget,
	materials: gtk::TreeView,
	bones: gtk::TreeView,
	info_notebook: gtk::Notebook,
	gl: Rc<gtkgl::GliumImageWidget>,

	/// Static OpenGL objects
	gl_static_objs: RefCell<Option<StaticGlObjects>>,
	/// Model-dependent OpenGL objects
	gl_model_objs: RefCell<Option<ModelGlObjects>>,
	/// Current model being viewed
	current_model: RefCell<Option<models::geom::Model>>,
	/// Which mouse button we are dragging and last seen position
	currently_dragging: Cell<Option<(u32, f64, f64)>>,
	/// Camera orientation
	scene_camera: Cell<Camera>,
	/// Whether a rerender has been queued.
	render_queued: Cell<bool>,
}
impl ModelViewport {
	pub fn new(builder: &gtk::Builder) -> Rc<Self> {
		let this = Rc::new(Self {
			root: builder.get_object("viewport_model").unwrap(),
			materials: builder.get_object("model_materials").unwrap(),
			bones: builder.get_object("model_bones").unwrap(),
			info_notebook: builder.get_object("model_info_notebook").unwrap(),
			gl: gtkgl::GliumImageWidget::from_image(
				builder.get_object("model_gl").unwrap(),
				|builder| {
					builder
						.with_gl_profile(glium::glutin::GlProfile::Core)
						.build()
				},
			)
			.expect("Could not create OpenGL context"),

			gl_static_objs: RefCell::new(None),
			gl_model_objs: RefCell::new(None),
			current_model: RefCell::new(None),
			scene_camera: Cell::new(Default::default()),
			currently_dragging: Cell::new(None),
			render_queued: Cell::new(false),
		});
		let this_weak = Rc::downgrade(&this);

		this.gl.widget().add_events(
			(gdk::EventMask::BUTTON_PRESS_MASK
				| gdk::EventMask::POINTER_MOTION_MASK
				| gdk::EventMask::BUTTON_PRESS_MASK
				| gdk::EventMask::BUTTON_RELEASE_MASK
				| gdk::EventMask::SCROLL_MASK)
				.bits() as i32,
		);

		this.gl
			.widget()
			.connect_event(clone2!(this_weak => move |_, event| {
				let this = upgrade_weak!(this_weak, gtk::Inhibit(false));

				let event = event.clone();
				if event.is::<gdk::EventMotion>() {
					let event = event.downcast::<gdk::EventMotion>().unwrap();

					let (button, prev_x, prev_y) = match this.currently_dragging.get() {
						Some(v) => v,
						None => { return gtk::Inhibit(true); }
					};

					let current_pos = event.get_position();
					let delta_x = current_pos.0 - prev_x;
					let delta_y = current_pos.1 - prev_y;

					let mut camera = this.scene_camera.get();
					match button {
						1 => {
							// LMB, pan
							const SCALE: f32 = 1.;
							camera.add_translation(delta_x as f32 * SCALE, delta_y as f32 * SCALE);
						},
						3 => {
							// RMB, rotate
							const SCALE: f32 = 0.01;
							camera.add_rotation(delta_y as f32 * SCALE, delta_x as f32 * -SCALE);
						},
						_ => { panic!("Should not be dragging button {:?}", button); }
					}
					this.scene_camera.set(camera);
					this.currently_dragging.set(Some((button, current_pos.0, current_pos.1)));
					Self::queue_rerender(&this);

					return gtk::Inhibit(true);
				}
				if event.is::<gdk::EventScroll>() {
					let event = event.downcast::<gdk::EventScroll>().unwrap();

					const ZOOM_INCREMENT: f32 = 10.;

					let zoom_amount = match event.get_direction() {
						gdk::ScrollDirection::Up => -ZOOM_INCREMENT,
						gdk::ScrollDirection::Down => ZOOM_INCREMENT,
						_ => { return gtk::Inhibit(false); }
					};

					let mut camera = this.scene_camera.get();
					camera.add_zoom(zoom_amount);
					this.scene_camera.set(camera);
					Self::queue_rerender(&this);

					return gtk::Inhibit(true);
				}
				if event.is::<gdk::EventButton>() {
					let event = event.downcast::<gdk::EventButton>().unwrap();

					let button = event.get_button();
					if button != 1 && button != 3 {
						// Not LMB or RMB
						return gtk::Inhibit(false);
					}

					match event.get_event_type() {
						gdk::EventType::ButtonPress => {
							let position = event.get_position();
							this.currently_dragging.set(Some((button, position.0, position.1)));
						},
						gdk::EventType::ButtonRelease => {
							if this.currently_dragging.get().map(|v| v.0) == Some(button) {
								this.currently_dragging.set(None);
							}
						},
						_ => { return gtk::Inhibit(false); }
					};
					return gtk::Inhibit(true);
				}

				return gtk::Inhibit(false);
			}));

		this.gl.set_resize_func(clone2!(this_weak => move |_, _| {
			let this = upgrade_weak!(this_weak);
			this.render();
		}));

		this.root
			.connect_parent_set(clone2!(this_weak => move |_, _old_parent| {
				let this = upgrade_weak!(this_weak);

				if this.root.get_parent().is_none() {
					// release resource if cleared
					this.clear();
				}
			}));

		this.info_notebook
			.connect_switch_page(clone2!(this_weak => move |_, _widget, _page| {
				let this = upgrade_weak!(this_weak);
				Self::queue_rerender(&this);
			}));

		this
	}

	/// Enqueues a render job to the GTK idle queue.
	/// Uses the `render_queued` flag to make sure that multiple renders aren't
	/// needlessly queued.
	fn queue_rerender(this: &Rc<Self>) {
		if this.render_queued.get() {
			return;
		}
		let this_weak = Rc::downgrade(this);
		gtk::idle_add(move || {
			let this = upgrade_weak!(this_weak, gtk::Continue(false));
			this.render();
			this.render_queued.set(false);
			gtk::Continue(false)
		});
		this.render_queued.set(true);
	}

	/// Renders a frame
	fn render(&self) {
		let facade = self.gl.facade();
		let mut frame = self.gl.draw();

		frame.clear_color_and_depth((0., 0., 0., 1.), 1.);

		let model_ref = self.current_model.borrow();
		let model = match *model_ref {
			Some(ref m) => m,
			None => {
				// Don't have a model to render
				frame.finish();
				return;
			}
		};

		let mut gl_static_objs_opt = self.gl_static_objs.borrow_mut();
		let gl_static_objs = gl_static_objs_opt.get_or_insert_with(|| StaticGlObjects::new(facade));

		let mut gl_model_objs_opt = self.gl_model_objs.borrow_mut();
		let gl_model_objs =
			gl_model_objs_opt.get_or_insert_with(|| ModelGlObjects::new(facade, &model));

		let gl_allocation = self.gl.widget().get_allocation();
		let aspect = gl_allocation.width as f32 / gl_allocation.height as f32;

		let view_xform = self.scene_camera.get().as_xform();
		let perspective_xform = nalgebra::Perspective3::new(aspect, PI / 2.0, 10.0, 100000.0);
		let xform = perspective_xform.to_homogeneous() * view_xform.to_homogeneous();

		let xform_uniform = mat2uniform(&xform);

		// Draw grid
		frame
			.draw(
				&gl_static_objs.grid,
				&glium::index::NoIndices(glium::index::PrimitiveType::LinesList),
				&gl_static_objs.helper_prog,
				&uniform! {
					matrix: xform_uniform,
					tint_color: [1.,1.,1.,1.0f32],
				},
				&glium::DrawParameters {
					backface_culling: glium::draw_parameters::BackfaceCullingMode::CullingDisabled,
					depth: glium::Depth {
						test: glium::draw_parameters::DepthTest::IfLess,
						write: true,
						..Default::default()
					},
					..Default::default()
				},
			)
			.unwrap();

		// Draw model
		for (i, index_buffer) in gl_model_objs.index_buffers.iter().enumerate() {
			// let material = model.materials[i];
			let color = color_for_material(i, gl_model_objs.index_buffers.len());

			frame
				.draw(
					&gl_model_objs.vertex_buffer,
					index_buffer,
					&gl_static_objs.model_prog,
					&uniform! {
						matrix: xform_uniform,
						color: color,
					},
					&glium::DrawParameters {
						backface_culling:
							glium::draw_parameters::BackfaceCullingMode::CullingDisabled,
						depth: glium::Depth {
							test: glium::draw_parameters::DepthTest::IfLess,
							write: true,
							..Default::default()
						},
						..Default::default()
					},
				)
				.unwrap();
		}

		// Draw bones, if on the bones tab
		if self.info_notebook.get_property_page() == 1 {
			let bones = model.bones();
			for bone in bones.iter() {
				let bone_model_xform = bones.global_xform_matrix(bone);
				let bone_xform = perspective_xform.to_homogeneous()
					* (view_xform.to_homogeneous() * bone_model_xform);
				let bone_uniform = mat2uniform(&bone_xform);

				frame
					.draw(
						&gl_static_objs.bone_axis,
						&glium::index::NoIndices(glium::index::PrimitiveType::LinesList),
						&gl_static_objs.helper_prog,
						&uniform! {
							matrix: bone_uniform,
							tint_color: [1.,1.,1.,1.0f32],
						},
						&Default::default(),
					)
					.unwrap();
			}
		}

		frame.finish();
	}

	pub fn set(&self, model: models::geom::Model) {
		self.materials
			.set_model(Some(&create_materials_store(model.materials())));
		self.bones.set_model(Some(&create_bones_store(&model)));
		self.scene_camera.set(Camera::centered_on(model.mesh()));
		*self.current_model.borrow_mut() = Some(model);
		*self.gl_model_objs.borrow_mut() = None;
		self.render();
	}

	pub fn clear(&self) {
		self.materials.set_model::<gtk::TreeStore, _>(None);
		self.bones.set_model::<gtk::TreeStore, _>(None);
		*self.current_model.borrow_mut() = None;
		*self.gl_model_objs.borrow_mut() = None;
	}
}
impl Viewport for ModelViewport {
	fn root(&self) -> &gtk::Widget {
		&self.root
	}
}

/// OpenGL objects that don't depend on the models.
struct StaticGlObjects {
	pub model_prog: glium::Program,
	pub helper_prog: glium::Program,
	pub grid: glium::VertexBuffer<HelperVertex>,
	pub bone_axis: glium::VertexBuffer<HelperVertex>,
}
impl StaticGlObjects {
	pub fn new<F: glium::backend::Facade>(facade: &F) -> Self {
		let model_prog = glium::Program::from_source(
			facade,
			r#"
			#version 140
			
			in vec3 position;
			in vec3 normal;
			uniform mat4 matrix;
			out vec3 v2f_normal;
			
			void main() {
				v2f_normal = normal;
				gl_Position = matrix * vec4(position, 1.0);
			}
			
		"#,
			r#"
			#version 140
			
			in vec3 v2f_normal;
			uniform vec3 color;
			out vec4 out_color;
			
			void main() {
				const vec3 LIGHT_DIR = -normalize(vec3(-1, -1, -1));
				vec3 light_color = color;
				vec3 dark_color = color * 0.1;
				
				float brightness = dot(v2f_normal, LIGHT_DIR)*0.5+0.5;
				out_color = vec4(mix(dark_color, light_color, brightness), 1);
			}
		"#,
			None,
		)
		.expect("Could not compile model glsl program");

		let helper_prog = glium::Program::from_source(
			facade,
			r#"
			#version 140
			
			in vec3 position;
			in vec3 base_color;
			uniform mat4 matrix;
			out vec3 v2f_base_color;
			
			void main() {
				v2f_base_color = base_color;
				gl_Position = matrix * vec4(position, 1.0);
			}
		"#,
			r#"
			#version 140
			
			in vec3 v2f_base_color;
			uniform vec4 tint_color;
			out vec4 out_color;
			
			void main() {
				out_color = vec4(v2f_base_color, 1.0) * tint_color;
			}
			
		"#,
			None,
		)
		.expect("Could not compile helper glsl program");

		const GRID_CELL_SIZE: f32 = 10.0;
		const GRID_NUM_CELLS: i32 = 10;
		const GRID_SIZE: f32 = GRID_NUM_CELLS as f32 * GRID_CELL_SIZE;
		const GRID_COLOR: [f32; 3] = [0.5, 0.5, 0.5];
		const GRID_X_COLOR: [f32; 3] = [1., 0., 0.];
		const GRID_Y_COLOR: [f32; 3] = [0., 1., 0.];
		const GRID_Z_COLOR: [f32; 3] = [0., 0., 1.];

		let mut grid_verts = vec![];
		for i in (-GRID_NUM_CELLS)..(GRID_NUM_CELLS + 1) {
			let v = i as f32 * GRID_CELL_SIZE;
			let x_color = if i == 0 { GRID_X_COLOR } else { GRID_COLOR };
			let z_color = if i == 0 { GRID_Z_COLOR } else { GRID_COLOR };

			// X gridline
			grid_verts.push(HelperVertex {
				position: [-GRID_SIZE, 0., v],
				base_color: x_color,
			});
			grid_verts.push(HelperVertex {
				position: [GRID_SIZE, 0., v],
				base_color: x_color,
			});
			// Z gridline
			grid_verts.push(HelperVertex {
				position: [v, 0., -GRID_SIZE],
				base_color: z_color,
			});
			grid_verts.push(HelperVertex {
				position: [v, 0., GRID_SIZE],
				base_color: z_color,
			});
		}
		// Y gridline
		grid_verts.push(HelperVertex {
			position: [0., -GRID_SIZE, 0.],
			base_color: GRID_Y_COLOR,
		});
		grid_verts.push(HelperVertex {
			position: [0., GRID_SIZE, 0.],
			base_color: GRID_Y_COLOR,
		});

		let grid = glium::VertexBuffer::new(facade, &grid_verts)
			.expect("Could not create grid vertex buffer");

		const BONE_HELPER_SIZE: f32 = 10.;
		let bone_axis_verts = vec![
			HelperVertex {
				position: [0., 0., 0.],
				base_color: [1., 0., 0.],
			},
			HelperVertex {
				position: [BONE_HELPER_SIZE, 0., 0.],
				base_color: [1., 0., 0.],
			},
			HelperVertex {
				position: [0., 0., 0.],
				base_color: [0., 1., 0.],
			},
			HelperVertex {
				position: [0., BONE_HELPER_SIZE, 0.],
				base_color: [0., 1., 0.],
			},
			HelperVertex {
				position: [0., 0., 0.],
				base_color: [0., 0., 1.],
			},
			HelperVertex {
				position: [0., 0., BONE_HELPER_SIZE],
				base_color: [0., 0., 1.],
			},
		];
		let bone_axis = glium::VertexBuffer::new(facade, &bone_axis_verts)
			.expect("Could not create bone vertex buffer");

		Self {
			model_prog,
			helper_prog,
			grid,
			bone_axis,
		}
	}
}

/// OpenGL objects that are derived from the model.
struct ModelGlObjects {
	vertex_buffer: glium::VertexBuffer<ModelVertex>,
	/// Corresponds to materials
	index_buffers: Vec<glium::IndexBuffer<u32>>,
}
impl ModelGlObjects {
	pub fn new<F: glium::backend::Facade>(facade: &F, model: &models::geom::Model) -> Self {
		let mesh = model.mesh();
		let positions = mesh
			.positions()
			.map(|slice| Cow::Borrowed(slice))
			.unwrap_or_else(|| Cow::Owned(vec![nalgebra::zero(); mesh.num_vertices()]));

		// TODO: better normals for meshes lacking normal data.
		let normals = mesh
			.normals()
			.map(|slice| Cow::Borrowed(slice))
			.unwrap_or_else(|| Cow::Owned(vec![Vector3::y(); mesh.num_vertices()]));

		let vertices = (0..mesh.num_vertices())
			.map(|i| ModelVertex {
				position: [positions[i][0], positions[i][1], positions[i][2]],
				normal: [normals[i][0], normals[i][1], normals[i][2]],
			})
			.collect::<Vec<_>>();
		let vertex_buffer = glium::VertexBuffer::new(facade, &vertices)
			.expect("Could not create model vertex buffer");

		let mut index_buffers = vec![];
		for i in 0..model.materials().len() {
			let indices = mesh
				.faces()
				.iter()
				.filter(|f| f.material == i)
				.flat_map(|f| f.indices.iter().map(|v| *v as u32))
				.collect::<Vec<_>>();
			let index_buffer = glium::IndexBuffer::new(
				facade,
				glium::index::PrimitiveType::TrianglesList,
				&indices,
			)
			.expect("Could not create model index buffer");
			index_buffers.push(index_buffer);
		}

		Self {
			vertex_buffer,
			index_buffers,
		}
	}
}

/// Camera orientation
#[derive(Debug, Clone, Copy)]
struct Camera {
	/// Point that the camera is looking at
	pub view_center: Vector3<f32>,
	/// Pitch + yaw of camera, in radians
	pub rotation: Vector2<f32>,
	/// Distance away from `view_center`
	pub distance: f32,
}
impl Camera {
	pub fn as_xform(&self) -> nalgebra::Isometry3<f32> {
		let rotation_quat = self.rotation_as_quat();

		let center = nalgebra::Point3::from(self.view_center);
		let eye = nalgebra::Point3::from(
			self.view_center - rotation_quat * Vector3::new(0., 0., self.distance),
		);
		nalgebra::Isometry3::look_at_rh(&eye, &center, &Vector3::new(0., 1., 0.))
	}

	pub fn add_zoom(&mut self, amount: f32) -> &mut Self {
		const MIN_ZOOM: f32 = 10.;
		const MAX_ZOOM: f32 = 10000.;

		self.distance += amount;
		if self.distance < MIN_ZOOM {
			self.distance = MIN_ZOOM;
		} else if self.distance > MAX_ZOOM {
			self.distance = MAX_ZOOM;
		}
		self
	}

	pub fn add_rotation(&mut self, pitch: f32, yaw: f32) -> &mut Self {
		const MAX_PITCH: f32 = ::std::f32::consts::PI / 2.0 - 0.01;
		self.rotation[0] += pitch;
		self.rotation[1] += yaw;
		if self.rotation[0] < -MAX_PITCH {
			self.rotation[0] = -MAX_PITCH;
		} else if self.rotation[0] > MAX_PITCH {
			self.rotation[0] = MAX_PITCH
		}
		self
	}

	pub fn add_translation(&mut self, x: f32, y: f32) -> &mut Self {
		let rotation_quat = self.rotation_as_quat();
		let vec = rotation_quat * Vector3::new(x, y, 0.);
		self.view_center += vec;
		self
	}

	pub fn centered_on(mesh: &models::geom::Mesh) -> Self {
		let bounds = mesh
			.bounds()
			.unwrap_or((nalgebra::zero(), nalgebra::zero()));
		let center = (bounds.0 + bounds.1) / 2.0;
		Self {
			view_center: center,
			..Default::default()
		}
	}

	fn rotation_as_quat(&self) -> nalgebra::UnitQuaternion<f32> {
		nalgebra::UnitQuaternion::new(Vector3::y() * self.rotation[1])
			* nalgebra::UnitQuaternion::new(Vector3::x() * self.rotation[0])
	}
}
impl Default for Camera {
	fn default() -> Self {
		const DEFAULT_ZOOM: f32 = 100.;
		Self {
			view_center: Vector3::new(0., 0., 0.),
			rotation: Vector2::new(0., PI),
			distance: DEFAULT_ZOOM,
		}
	}
}

#[allow(deprecated)] // implement_vertex uses std::mem::uninitialized
mod vertices {
	#[derive(Debug, Clone, Copy)]
	pub struct ModelVertex {
		pub position: [f32; 3],
		pub normal: [f32; 3],
	}
	implement_vertex!(ModelVertex, position, normal);

	#[derive(Debug, Clone, Copy)]
	pub struct HelperVertex {
		pub position: [f32; 3],
		pub base_color: [f32; 3],
	}
	implement_vertex!(HelperVertex, position, base_color);
}
pub use vertices::*;

fn create_materials_store(materials: &[models::geom::Material]) -> gtk::TreeStore {
	const ICON_SIZE: u32 = 15;

	let store = gtk::TreeStore::new(&[
		gtk::Type::String,
		gtk::Type::Bool, // Icon visible?
		gdk_pixbuf::Pixbuf::static_type(),
	]);

	for (i, material) in materials.iter().enumerate() {
		let color = color_for_material(i, materials.len());
		let color_u8 = [
			(color[0] * 255.) as u8,
			(color[1] * 255.) as u8,
			(color[2] * 255.) as u8,
		];
		let pixels = (0..(ICON_SIZE * ICON_SIZE))
			.flat_map(|_| color_u8.iter().cloned())
			.collect::<Vec<_>>();
		let icon = gdk_pixbuf::Pixbuf::new_from_vec(
			pixels,
			gdk_pixbuf::Colorspace::Rgb,
			false,
			8,
			ICON_SIZE as i32,
			ICON_SIZE as i32,
			ICON_SIZE as i32 * 3,
		);

		let iter =
			store.insert_with_values(None, None, &[0, 1, 2], &[&material.name, &true, &icon]);

		for texture in material.textures.iter() {
			store.insert_with_values(Some(&iter), None, &[0, 1], &[&texture.name, &false]);
		}
	}
	store
}
fn color_for_material(i: usize, out_of: usize) -> [f32; 3] {
	hsv2rgb((i as f32) / (out_of as f32), 1., 1.)
}

fn create_bones_store(model: &models::geom::Model) -> gtk::TreeStore {
	let bones = model.bones();
	let store = gtk::TreeStore::new(&[gtk::Type::String]);
	let mut next_bones: Vec<(&models::geom::Bone, Option<gtk::TreeIter>)> = bones
		.children_of(None)
		.map(|bone| (bone, None))
		.collect::<Vec<_>>();
	while !next_bones.is_empty() {
		let (bone, parent_iter) = next_bones.pop().unwrap();
		let iter = store.insert_with_values(parent_iter.as_ref(), None, &[0], &[&bone.name]);

		next_bones.extend(
			bones
				.children_of(Some(bone))
				.map(|bone| (bone, Some(iter.clone()))),
		);
	}
	store
}

/// 0 <= h,s,v <= 1
fn hsv2rgb(h: f32, s: f32, v: f32) -> [f32; 3] {
	if s <= 0. {
		return [0., 0., 0.];
	}

	let hh = h / (1.0 / 6.0);
	let i = hh as u8;
	let ff = hh - i as f32;

	let p = v * (1. - s);
	let q = v * (1. - s * ff);
	let t = v * (1. - s * (1. - ff));

	return match i {
		0 => [v, t, p],
		1 => [q, v, p],
		2 => [p, v, t],
		3 => [p, q, v],
		4 => [t, p, v],
		5 => [v, p, q],
		_ => {
			panic!();
		}
	};
}

fn mat2uniform(matrix: &nalgebra::Matrix4<f32>) -> [[f32; 4]; 4] {
	[
		[matrix[0], matrix[1], matrix[2], matrix[3]],
		[matrix[4], matrix[5], matrix[6], matrix[7]],
		[matrix[8], matrix[9], matrix[10], matrix[11]],
		[matrix[12], matrix[13], matrix[14], matrix[15]],
	]
}
