use gdk_pixbuf;
use gtk::{
	self,
	prelude::*,
};
use pokemon_3ds::formats::textures;
use rgb::{
	ComponentBytes,
	RGB8,
};
use std::{
	cell::RefCell,
	rc::Rc,
};

use crate::viewports::Viewport;

pub struct TextureViewport {
	root: gtk::Widget,
	name: gtk::Label,
	width: gtk::Label,
	height: gtk::Label,
	format: gtk::Label,
	image: gtk::Image,

	current_texture: RefCell<Option<textures::Texture>>,
}
impl TextureViewport {
	pub fn new(builder: &gtk::Builder) -> Rc<Self> {
		let this = Rc::new(Self {
			root: builder.get_object("viewport_texture").unwrap(),
			name: builder.get_object("texture_name").unwrap(),
			width: builder.get_object("texture_width").unwrap(),
			height: builder.get_object("texture_height").unwrap(),
			format: builder.get_object("texture_format").unwrap(),
			image: builder.get_object("texture_image").unwrap(),

			current_texture: RefCell::new(None),
		});

		let cbthis = Rc::downgrade(&this);
		this.root.connect_parent_set(move |_, _old_parent| {
			let this = match cbthis.upgrade() {
				Some(this) => this,
				None => {
					return;
				}
			};

			if this.root.get_parent().is_none() {
				// release resource if cleared
				this.clear();
			}
		});

		this
	}

	pub fn set(&self, texture: textures::Texture) {
		self.name.set_text(texture.name());
		self.width.set_text(&format!("{}px", texture.width()));
		self.height.set_text(&format!("{}px", texture.height()));
		self.format.set_text(match texture.data() {
			textures::TextureData::RGB(_) => "RGB",
			textures::TextureData::RGBA(_) => "RGBA",
			textures::TextureData::Grayscale(_) => "Grayscale",
			textures::TextureData::GrayscaleAlpha(_) => "Grayscale + Alpha",
		});
		self.image
			.set_from_pixbuf(Some(&self.create_pixbuf(&texture)));

		*self.current_texture.borrow_mut() = Some(texture);
	}

	pub fn clear(&self) {
		*self.current_texture.borrow_mut() = None;
		self.image
			.set_from_icon_name("gtk-missing-image", gtk::IconSize::Button.into());
	}

	fn create_pixbuf(&self, texture: &textures::Texture) -> gdk_pixbuf::Pixbuf {
		const CHECKER_COLOR_1: RGB8 = RGB8 {
			r: 0x88,
			g: 0x88,
			b: 0x88,
		};
		const CHECKER_COLOR_2: RGB8 = RGB8 {
			r: 0xcc,
			g: 0xcc,
			b: 0xcc,
		};
		const CHECKER_SIZE: usize = 8;

		let mut rgb_data = Vec::<u8>::with_capacity(texture.width() * texture.height() * 3);
		for y in 0..texture.height() {
			for x in 0..texture.width() {
				let src_pix = texture.get_rgba(x, y).unwrap();
				let checker_idx = ((x / CHECKER_SIZE) % 2 == 0) ^ ((y / CHECKER_SIZE) % 2 == 0);
				let checker_pix = if checker_idx {
					CHECKER_COLOR_1
				} else {
					CHECKER_COLOR_2
				};

				let dest_pix = RGB8 {
					r: blend_alpha(src_pix.r, checker_pix.r, src_pix.a),
					g: blend_alpha(src_pix.g, checker_pix.g, src_pix.a),
					b: blend_alpha(src_pix.b, checker_pix.b, src_pix.a),
				};
				rgb_data.extend([dest_pix].as_bytes().iter());
			}
		}

		gdk_pixbuf::Pixbuf::new_from_vec(
			rgb_data,
			gdk_pixbuf::Colorspace::Rgb,
			false,
			8,
			texture.width() as i32,
			texture.height() as i32,
			(texture.width() * 3) as i32,
		)
	}
}
impl Viewport for TextureViewport {
	fn root(&self) -> &gtk::Widget {
		&self.root
	}
}

fn blend_alpha(v1: u8, v2: u8, a: u8) -> u8 {
	let a = a as u16;
	let v = (v1 as u16) * a + (v2 as u16) * (0xff - a);
	(v / 0xff) as u8
}
