/// Clones some values then creates a closure.
///
/// Useful for creating `Weak` objects to move into closures.
///
/// # Example
///
/// ```
/// use std::rc::Rc;
///
/// let rc = Rc::new(123)
///
/// let closure = clone2!(rc, move || {
/// 	return *rc;
/// });
///
/// // The rc was not moved
/// assert_eq!(*rc, 123);
/// // The closure has a clone
/// assert_eq!(closure(), 123);
/// ```
macro_rules! clone2 {
	($($n:ident),+ => $func:expr) => (
		{
			$( let $n = $n.clone(); )+
			$func
		}
	);
}

// upgrade weak reference or return
macro_rules! upgrade_weak {
	($x:ident, $r:expr) => {{
		match $x.upgrade() {
			Some(o) => o,
			None => return $r,
			}
		}};
	($x:ident) => {
		upgrade_weak!($x, ())
	};
}

/// Creates a structure of widgets to load from a `gtk::Builder`.
///
/// Creates a structure with a `load` function that takes a `gkt::Builder`.
/// Field names correspond to widget IDs to load from the builder. All fields are
/// public. If a field isn't found in the builder, `load` will panic.
///
/// # Example
/// ```rust
/// #[macro_use]
///
/// widgets!(MyWindowWidgets {
/// 	window: gtk::Window,
/// 	ok_button: gtk::Button,
/// });
///
/// fn load_widgets(src: &str) -> MyWindowWidgets {
/// 	let builder = gtk::Builder::new_from_string(src);
/// 	MyWindowWidgets::load(&builder)
/// }
/// ```
macro_rules! widgets {
	($name:ident { $($widget_name:ident : $widget_typ:ty),+ , }) => {
		struct $name {
			$( pub $widget_name : $widget_typ, )+
		}
		impl $name {
			pub fn load(builder: &::gtk::Builder) -> Self {
				Self {
					$( $widget_name : builder.get_object::<$widget_typ>(stringify!($widget_name))
						.unwrap_or_else(|| panic!("Widget {} not found or not of type {}", stringify!($widget_name), stringify!($widget_typ))), )+
				}
			}
		}
	};
}
