use id_tree::NodeId;
use pokemon_3ds::{
	error,
	formats,
	tree,
};
use std::{
	fs,
	io::{
		self,
		Seek,
	},
	path::PathBuf,
};

use crate::tree_store_listener::TreeStoreListener;

pub struct Exporter(tree::VisitorState<TreeStoreListener, ExporterVisitor>);
impl Exporter {
	pub fn new(root: NodeId, path: PathBuf) -> Self {
		Self(tree::VisitorState::new(
			root,
			ExporterVisitor {
				current_path: path,
				current_depth: 0,
			},
		))
	}

	pub fn drive(
		&mut self,
		tree: &mut tree::ContainerTree<TreeStoreListener>,
	) -> error::Result<bool> {
		self.0.drive(tree)
	}
}

struct ExporterVisitor {
	current_path: PathBuf,
	current_depth: usize,
}
impl<L: tree::ContainerTreeListener> tree::Visitor<L> for ExporterVisitor {
	fn enter_collection(
		&mut self,
		tree: &mut tree::ContainerTree<L>,
		node_id: &NodeId,
	) -> error::Result<()> {
		let node = tree.tree().get(node_id).unwrap();
		let component_name = node.data().entry().path.to_string().replace('/', "_");

		// Only modify current_path if we succeed, otherwise leave it untouched.
		let mut new_path = self.current_path.clone();
		if self.current_depth != 0 {
			new_path.push(component_name);
		}
		fs::create_dir(&new_path).or_else(|e| {
			if e.kind() == io::ErrorKind::AlreadyExists {
				Ok(())
			} else {
				Err(e)
			}
		})?;

		self.current_path = new_path;
		self.current_depth += 1;
		Ok(())
	}

	fn exit_collection(
		&mut self,
		_tree: &mut tree::ContainerTree<L>,
		_node_id: &NodeId,
	) -> error::Result<()> {
		self.current_path.pop();
		self.current_depth -= 1;
		Ok(())
	}

	fn visit_node(
		&mut self,
		tree: &mut tree::ContainerTree<L>,
		node_id: &NodeId,
	) -> error::Result<()> {
		let stream_ref = tree.stream_ref(node_id)?;
		let mut reader = stream_ref.borrow_mut();
		reader.seek(io::SeekFrom::Start(0))?;

		let node = tree.tree().get(node_id).unwrap();
		let component_name = node.data().entry().path.to_string().replace('/', "_");
		let mut out_path = self.current_path.clone();
		if self.current_depth != 0 {
			out_path.push(component_name);
		}

		let format = tree.tree().get(node_id).unwrap().data().format().into();
		match format {
			None => {}
			Some(formats::Format::Container(_)) => {
				unreachable!();
			}
			Some(formats::Format::Model(f)) => {
				let model = f.read(&mut *reader)?;

				if self.current_depth != 0 {
					out_path.set_extension("smd");
				}
				let file = fs::File::create(&out_path)?;
				let mut writer = io::BufWriter::new(file);
				formats::export::smd::write_smd_model(&mut writer, &model)?;
			}
			Some(formats::Format::Texture(f)) => {
				let tex = f.read(&mut *reader)?;

				if self.current_depth != 0 {
					out_path.set_extension("png");
				}
				let file = fs::File::create(&out_path)?;
				let mut writer = io::BufWriter::new(file);
				tex.write_png(&mut writer)?;
			}
			Some(formats::Format::Animation(_f)) => {
				// TODO
			}
		}
		Ok(())
	}
}
