use gdk;
use gio::{
	self,
	FileExt,
};
use gtk::{
	self,
	prelude::*,
};
use id_tree::NodeId;
use pokemon_3ds::{
	error,
	formats,
	tree,
};
use std::{
	cell::RefCell,
	convert::TryFrom,
	fs,
	io,
	path,
	rc::{
		Rc,
		Weak,
	},
};

use crate::{
	export::Exporter,
	tree_store_listener::TreeStoreListener,
	util,
	viewports::{
		self,
		Viewport,
	},
};

fn tree_path_to_path(path: &gtk::TreePath) -> impl IntoIterator<Item = usize> {
	path.get_indices()
		.into_iter()
		.map(|i| usize::try_from(i).expect("Invalid tree path component"))
}

pub struct Ui {
	this_weak: Option<Weak<RefCell<Ui>>>,

	container_tree: Option<tree::ContainerTree<TreeStoreListener>>,
	currently_viewing: Option<NodeId>,

	/// Is the UI exporting an object?
	exporter: Option<Exporter>,
	/// Is the identifier idle callback registered?
	identifier_task_running: bool,

	/// TreeStore for the container panel.
	///
	/// No relation to the IRL store.
	container_store: gtk::TreeStore,

	widgets: Widgets,
	viewports: ViewportManager,
}
impl Ui {
	pub fn new(application: &gtk::Application) -> Rc<RefCell<Self>> {
		setup_icons();

		let glade_src = include_str!("ui.glade");
		let builder = gtk::Builder::new_from_string(glade_src);
		let widgets = Widgets::load(&builder);
		let viewports = ViewportManager::new(widgets.viewport_pane.clone(), &builder);

		let css_src = include_bytes!("ui.css");
		let css_provider = gtk::CssProvider::new();
		css_provider.load_from_data(css_src).unwrap();
		gtk::StyleContext::add_provider_for_screen(
			&gdk::Screen::get_default().unwrap(),
			&css_provider,
			gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
		);

		let this = Rc::new(RefCell::new(Self {
			this_weak: None,
			container_tree: None,
			currently_viewing: None,
			exporter: None,
			identifier_task_running: false,
			container_store: gtk::TreeStore::new(&[
				gtk::Type::String, // COLUMN_NAME
				gtk::Type::String, // COLUMN_FORMAT_ICON
				gtk::Type::String, // COLUMN_SIZE
			]),
			widgets,
			viewports,
		}));

		{
			let mut this_mut = this.borrow_mut();
			this_mut.this_weak = Some(Rc::downgrade(&this));
			this_mut.widgets.main_window.set_application(application);
			this_mut
				.widgets
				.container_tree
				.set_model(Some(&this_mut.container_store));
			setup_about_box(&this_mut.widgets.about_window);
			this_mut.close();

			let this_weak = this_mut.weak_rc();

			this_mut
				.widgets
				.menu_quit
				.connect_activate(clone2!(this_weak => move |_| {
					let this = upgrade_weak!(this_weak);
					this.borrow_mut().widgets.main_window.close();
				}));

			this_mut
				.widgets
				.menu_open
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();
					this.show_open_dialog();
				}));

			this_mut
				.widgets
				.menu_close
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();
					this.close();
				}));

			this_mut.widgets.container_tree.connect_row_activated(
				clone2!(this_weak => move |_, path, _column| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					this.open_entry_tree_path(&path);
				}),
			);

			this_mut.widgets.main_window.drag_dest_set(
				gtk::DestDefaults::ALL,
				&[],
				gdk::DragAction::COPY,
			);
			this_mut.widgets.main_window.drag_dest_add_uri_targets();
			this_mut.widgets.main_window.connect_drag_data_received(
				clone2!(this_weak => move |_, drag_ctx, _x, _y, selection, _info, time| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();
					let urls = selection.get_uris();
					if urls.len() != 1 {
						drag_ctx.drag_finish(false, false, time);
						return;
					}
					let url_str = urls.into_iter().next().unwrap();

					let filepath = {
						let gnome_file = gio::File::new_for_uri(&url_str);
						match gnome_file.get_path() {
							Some(p) => p,
							None => {
								this.widgets.show_error(&format!("Could not get path for file"));
								drag_ctx.drag_finish(false, false, time);
								return;
							}
						}
					};

					let filename = filepath
						.iter()
						.next_back()
						.expect("filepath does not have any components")
						.to_string_lossy()
						.into_owned();

					let file = match fs::File::open(filepath) {
						Ok(f) => f,
						Err(err) => {
							this.widgets.show_error(&format!("Could not open file: {}", err));
							drag_ctx.drag_finish(false, false, time);
							return;
						}
					};

					this.open_top_level(filename, io::BufReader::new(file));
					drag_ctx.drag_finish(true, false, time);
				}),
			);

			this_mut
				.widgets
				.menu_about
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let this = this_rc.borrow();

					this.widgets.about_window.set_transient_for(Some(&this.widgets.main_window));
					this.widgets.about_window.show_all();
					this.widgets.about_window.run();
					this.widgets.about_window.hide();
				}));

			this_mut
				.widgets
				.menu_export_all
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let root_id = this.container_tree.as_ref().unwrap().tree().root_node_id().unwrap().clone();
					this.export_single(&root_id, false);
				}));

			this_mut
				.widgets
				.menu_export_viewport
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let viewing = match (&this.container_tree, &this.currently_viewing) {
						(Some(_), Some(ref viewing)) => {
							viewing.clone()
						},
						_ => {
							return;
						}
					};
					this.export_single(&viewing, false);
				}));

			this_mut.widgets.menu_export_viewport_raw.connect_activate(
				clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let viewing = match (&this.container_tree, &this.currently_viewing) {
						(Some(_), Some(ref viewing)) => {
							viewing.clone()
						},
						_ => {
							return;
						}
					};
					this.export_single(&viewing, true);
				}),
			);

			this_mut.widgets.container_tree.connect_button_press_event(
				clone2!(this_weak => move |_widget, event| {
					let this_rc = upgrade_weak!(this_weak, gtk::Inhibit(false));
					let this = this_rc.borrow();

					if event.get_event_type() != gdk::EventType::ButtonPress || event.get_button() != 3 {
						// Not pressing right click, ignore
						return gtk::Inhibit(false);
					}

					// Get the row we right-clicked over.
					let pos = event.get_position();
					let path_opt = this.widgets.container_tree.get_path_at_pos(pos.0 as i32, pos.1 as i32)
						.and_then(|(path_opt, _column_opt, _cell_x, _cell_y)| path_opt);
					let path = match path_opt {
						Some(p) => p,
						None => { return gtk::Inhibit(false); },
					};

					// If right-clicking an unselected row, replace the selection
					let selection = this.widgets.container_tree.get_selection();
					if !selection.path_is_selected(&path) {
						selection.unselect_all();
						selection.select_path(&path);
					}

					// Only activate the Open menu item if one row is selected, since
					// we can only view one at a time.
					let (selected, _) = selection.get_selected_rows();
					this.widgets.container_tree_menu_open.set_sensitive(selected.len() == 1);

					// Show menu
					//this.container_tree_menu.attach_to_widget(widget);
					this.widgets.container_tree_menu.popup_easy(event.get_button(), event.get_time());
					return gtk::Inhibit(true);
				}),
			);

			this_mut.widgets.container_tree_menu_open.connect_activate(
				clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let selection = this.widgets.container_tree.get_selection();
					let (selected, _) = selection.get_selected_rows();
					if selected.len() != 1 {
						// Multiple things selected. Shouldn't happen.
						return;
					}
					this.open_entry_tree_path(&selected[0]);
				}),
			);

			this_mut
				.widgets
				.container_tree_menu_export
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let selection = this.widgets.container_tree.get_selection();
					let (selected, _) = selection.get_selected_rows();
					if selected.len() != 1 {
						// Multiple things selected. Shouldn't happen.
						return;
					}

					let note_id = this.container_tree.as_ref().unwrap()
						.path_to_node_id(tree_path_to_path(&selected[0]))
						.unwrap()
						.clone();
					this.export_single(&note_id, false);
				}));

			this_mut
				.widgets
				.container_tree_menu_export_raw
				.connect_activate(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();

					let selection = this.widgets.container_tree.get_selection();
					let (selected, _) = selection.get_selected_rows();
					if selected.len() != 1 {
						// Multiple things selected. Shouldn't happen.
						return;
					}

					let note_id = this.container_tree.as_ref().unwrap()
						.path_to_node_id(tree_path_to_path(&selected[0]))
						.unwrap()
						.clone();
					this.export_single(&note_id, true);
				}));

			this_mut
				.widgets
				.export_cancel
				.connect_clicked(clone2!(this_weak => move |_| {
					let this_rc = upgrade_weak!(this_weak);
					let mut this = this_rc.borrow_mut();
					this.exporter = None;
				}));
		}

		this
	}

	/// Gets a weak reference to this object
	pub fn weak_rc(&self) -> Weak<RefCell<Self>> {
		self.this_weak.as_ref().unwrap().clone()
	}

	/// Gets the window object
	pub fn window(&self) -> &gtk::ApplicationWindow {
		&self.widgets.main_window
	}

	/// Shows the open file dialog to the user and, if the user chooses, opens
	/// a new file.
	fn show_open_dialog(&mut self) {
		let dialog = gtk::FileChooserDialog::with_buttons(
			Some("Open File"),
			Some(&self.widgets.main_window),
			gtk::FileChooserAction::Open,
			&[
				("_Cancel", gtk::ResponseType::Cancel),
				("_Open", gtk::ResponseType::Accept),
			],
		);

		if dialog.run() != gtk::ResponseType::Accept.into() {
			dialog.destroy();
			return;
		}

		let filepath = match dialog.get_filename() {
			Some(f) => f,
			None => {
				dialog.destroy();
				return;
			}
		};
		dialog.destroy();

		let filename = filepath
			.iter()
			.next_back()
			.expect("filepath does not have any components")
			.to_string_lossy()
			.into_owned();

		let file = match fs::File::open(filepath) {
			Ok(f) => f,
			Err(err) => {
				self.widgets
					.show_error(&format!("Could not open file: {}", err));
				return;
			}
		};

		self.open_top_level(filename, io::BufReader::new(file));
	}

	/// Opens a file at the top-level, replacing any existing open file.
	fn open_top_level(&mut self, name: String, file: io::BufReader<fs::File>) {
		let result: error::Result<()> = (|| {
			self.close();

			let container_tree = tree::ContainerTree::new(
				file,
				name,
				TreeStoreListener::new(self.container_store.clone()),
			)?;

			let root_id = container_tree.tree().root_node_id().unwrap().clone();
			self.container_tree = Some(container_tree);

			self.start_identifier_task();
			self.viewports
				.set_viewport(&self.viewports.no_entry_viewport);
			self.widgets.container_tree.set_sensitive(true);
			self.widgets.menu_close.set_sensitive(true);
			self.widgets.menu_export_all.set_sensitive(true);

			self.open_entry(&root_id);

			Ok(())
		})();

		if let Err(err) = result {
			if err.is_io() {
				self.close();
				self.widgets
					.show_error(&format!("Could not open entry:\n{}", err));
			} else {
				self.viewports
					.set_viewport(&self.viewports.parse_error_viewport);
				self.viewports.parse_error_viewport.set(&err.to_string());
			}
		}
	}

	fn open_entry_tree_path(&mut self, path: &gtk::TreePath) {
		self.open_entry_path(tree_path_to_path(path))
	}

	fn open_entry_path<I: IntoIterator<Item = usize>>(&mut self, path: I) {
		let node_id = self
			.container_tree
			.as_ref()
			.expect("open_entry_path: no container tree")
			.path_to_node_id(path)
			.expect("open_entry_path: no path to node")
			.clone();
		self.open_entry(&node_id);
	}

	/// Opens an entry in the container tree
	fn open_entry(&mut self, node_id: &NodeId) {
		let result: error::Result<()> = (|| {
			let container_tree = self
				.container_tree
				.as_mut()
				.expect("open_entry with nothing open");
			match container_tree.get_or_guess_format(node_id)? {
				Some(formats::Format::Container(_)) => {
					self.widgets.menu_export_viewport.set_sensitive(false);
					self.widgets.menu_export_viewport_raw.set_sensitive(false);

					container_tree.open_container(node_id)?;
					self.start_identifier_task();

					self.viewports
						.set_viewport(&self.viewports.no_entry_viewport);
					//self.widgets.container_tree.expand_row(&node_id, false);
				}
				f @ _ => {
					let stream_ref = container_tree.stream_ref(node_id)?;
					let mut reader = stream_ref.borrow_mut();

					self.widgets.menu_export_viewport.set_sensitive(true);
					self.widgets.menu_export_viewport_raw.set_sensitive(true);
					self.show_resource_from_reader(&mut *reader, f)?;
					self.currently_viewing = Some(node_id.clone());
				}
			}
			Ok(())
		})();

		if let Err(err) = result {
			if err.is_io() {
				self.close();
				self.widgets
					.show_error(&format!("Could not open entry:\n{}", err));
			} else {
				self.widgets.menu_export_viewport.set_sensitive(false);
				self.widgets.menu_export_viewport_raw.set_sensitive(false);
				self.viewports
					.set_viewport(&self.viewports.parse_error_viewport);
				self.viewports.parse_error_viewport.set(&err.to_string());
			}
		}
	}

	/// Shows a resource in the viewport
	fn show_resource_from_reader<Reader: 'static + io::Read + io::Seek>(
		&mut self,
		reader: &mut Reader,
		format: Option<formats::Format>,
	) -> error::Result<()> {
		match format {
			Some(formats::Format::Container(_)) => {
				panic!("Can't use show_resource_from_reader on container");
			}
			Some(formats::Format::Model(format)) => {
				let model = format.read(reader)?;

				self.viewports.set_viewport(&self.viewports.model_viewport);
				self.viewports.model_viewport.set(model);
			}
			Some(formats::Format::Texture(format)) => {
				// Open texture viewport
				let texture = format.read(reader)?;

				self.viewports
					.set_viewport(&self.viewports.texture_viewport);
				self.viewports.texture_viewport.set(texture);
			}
			Some(formats::Format::Animation(_format)) => {
				// NYI
			}
			None => {
				self.viewports
					.set_viewport(&self.viewports.unrecognized_viewport);
			}
		}
		Ok(())
	}

	/// Closes the open file, if any
	pub fn close(&mut self) {
		self.viewports
			.set_viewport(&self.viewports.no_file_viewport);
		self.container_store.clear();
		self.widgets.menu_close.set_sensitive(false);
		self.widgets.menu_export_all.set_sensitive(false);
		self.widgets.menu_export_viewport.set_sensitive(false);
		self.widgets.menu_export_viewport_raw.set_sensitive(false);
		self.widgets.container_tree.set_sensitive(false);
		self.container_tree = None;
	}

	/// Starts the identifier task if it isn't running.
	///
	/// Should be called after the `to_be_identified` queue is added to, to ensure
	/// the queue is processed.
	fn start_identifier_task(&mut self) {
		if self.identifier_task_running {
			return;
		}
		self.identifier_task_running = true;

		let this_weak = self.weak_rc();
		gtk::idle_add(move || {
			let this_rc = upgrade_weak!(this_weak, gtk::Continue(false));
			let mut this = this_rc.borrow_mut();
			assert!(this.identifier_task_running);

			let do_continue: bool = (|| {
				// TODO
				return false;
			})();

			if do_continue {
				return gtk::Continue(true);
			} else {
				this.identifier_task_running = false;
				return gtk::Continue(false);
			}
		});
	}

	fn export_single(&mut self, node_id: &NodeId, raw: bool) {
		assert!(self.exporter.is_none());

		let container_tree = self.container_tree.as_mut().unwrap();
		let format = if raw {
			None
		} else {
			match container_tree.get_or_guess_format(node_id) {
				Ok(f) => f,
				Err(err) => {
					self.widgets
						.show_error(&format!("Could not export: {}", err));
					return;
				}
			}
		};
		let node = container_tree.tree().get(node_id).unwrap();
		let name = node.data().entry().path.to_string();

		let path = match dialog_export_single(&self.widgets.main_window, &name, format) {
			Some(p) => p,
			None => {
				return;
			}
		};

		if format.is_none() {
			let res: error::Result<()> = (|| {
				let stream_ref = container_tree.stream_ref(node_id)?;
				let mut reader = stream_ref.borrow_mut();
				reader.seek(io::SeekFrom::Start(0))?;

				let file = fs::File::create(&path)?;
				let mut writer = io::BufWriter::new(file);

				io::copy(&mut *reader, &mut writer)?;
				Ok(())
			})();

			if let Err(err) = res {
				self.widgets
					.show_error(&format!("Could not export: {}", err));
				return;
			}
		} else {
			self.exporter = Some(Exporter::new(node_id.clone(), path));
			self.widgets.main_window.set_sensitive(false);
			self.widgets
				.export_window
				.set_transient_for(Some(&self.widgets.main_window));
			//self.widgets.export_progress.set_fraction(0.0);
			self.widgets.export_window.show();

			let this_weak = self.weak_rc();
			gtk::idle_add(move || {
				let this_rc = upgrade_weak!(this_weak, gtk::Continue(false));
				let mut this = this_rc.borrow_mut();

				let export_window = this.widgets.export_window.clone();

				let do_continue: bool = (|| {
					let this = &mut *this;
					let exporter = this.exporter.as_mut()?;
					let container_tree = this.container_tree.as_mut()?;
					match exporter.drive(container_tree) {
						Ok(c) => Some(c),
						Err(err) if err.is_io() => {
							let dialog = gtk::MessageDialog::new(
								Some(&export_window),
								gtk::DialogFlags::MODAL,
								gtk::MessageType::Error,
								gtk::ButtonsType::Ok,
								&format!("Error while exporting:\n{}", err),
							);
							dialog.run();
							dialog.destroy();
							Some(false)
						}
						Err(_err) => {
							//error!("Error while exporting: {}", err);
							Some(true)
						}
					}
				})()
				.unwrap_or(false);

				if !do_continue {
					this.widgets.export_window.hide();
					this.widgets.main_window.set_sensitive(true);
				}
				return gtk::Continue(do_continue);
			});
		}
	}
}

/// Fills out an `AboutDialog` with info from the `Cargo.toml` file.
fn setup_about_box(dialog: &gtk::AboutDialog) {
	fn str_to_opt(string: &str) -> Option<&str> {
		if string.is_empty() {
			None
		} else {
			Some(string)
		}
	}

	let authors = env!("CARGO_PKG_AUTHORS").split(":").collect::<Vec<_>>();
	dialog.set_authors(&authors);
	dialog.set_version(str_to_opt(env!("CARGO_PKG_VERSION")));
	dialog.set_website(str_to_opt(env!("CARGO_PKG_HOMEPAGE")));
	dialog.set_program_name(env!("CARGO_PKG_NAME"));
	dialog.set_comments(str_to_opt(env!("CARGO_PKG_DESCRIPTION")));
}

fn setup_icons() {
	let theme = gtk::IconTheme::get_default().unwrap();
	theme.add_resource_path("/col32/pokemon-3ds-gui/resources/icons/");
	theme.rescan_if_needed();
}

// Widgets in the main window
widgets!(Widgets {
	main_window: gtk::ApplicationWindow,
	about_window: gtk::AboutDialog,

	viewport_pane: gtk::Paned,
	container_tree: gtk::TreeView,

	menu_open: gtk::MenuItem,
	menu_close: gtk::MenuItem,
	menu_quit: gtk::MenuItem,
	menu_about: gtk::MenuItem,
	menu_export_all: gtk::MenuItem,
	menu_export_viewport: gtk::MenuItem,
	menu_export_viewport_raw: gtk::MenuItem,

	container_tree_menu: gtk::Menu,
	container_tree_menu_open: gtk::MenuItem,
	container_tree_menu_export: gtk::MenuItem,
	container_tree_menu_export_raw: gtk::MenuItem,

	export_window: gtk::Window,
	//export_progress: gtk::ProgressBar,
	export_cancel: gtk::Button,
});
impl Widgets {
	/// Shows a modal error box with the specified text, blocking until the user
	/// presses OK.
	pub fn show_error(&self, msg: &str) {
		let dialog = gtk::MessageDialog::new(
			Some(&self.main_window),
			gtk::DialogFlags::MODAL,
			gtk::MessageType::Error,
			gtk::ButtonsType::Ok,
			msg,
		);
		dialog.run();
		dialog.destroy();
	}
}

/// Stores the viewports and controls viewport changing
struct ViewportManager {
	viewport_pane: gtk::Paned,

	pub no_file_viewport: Rc<viewports::StaticViewport>,
	pub no_entry_viewport: Rc<viewports::StaticViewport>,
	pub unrecognized_viewport: Rc<viewports::StaticViewport>,
	pub texture_viewport: Rc<viewports::TextureViewport>,
	pub model_viewport: Rc<viewports::ModelViewport>,
	pub parse_error_viewport: Rc<viewports::ErrorViewport>,
}
impl ViewportManager {
	pub fn new(viewport_pane: gtk::Paned, builder: &gtk::Builder) -> Self {
		let this = Self {
			viewport_pane,

			no_file_viewport: viewports::StaticViewport::new(builder, "viewport_no_file"),
			no_entry_viewport: viewports::StaticViewport::new(builder, "viewport_no_entry"),
			unrecognized_viewport: viewports::StaticViewport::new(builder, "viewport_unrecognized"),
			texture_viewport: viewports::TextureViewport::new(builder),
			model_viewport: viewports::ModelViewport::new(builder),
			parse_error_viewport: viewports::ErrorViewport::new(builder),
		};
		this.viewport_pane.add2(this.no_file_viewport.root());
		this
	}

	/// Shows a viewport
	pub fn set_viewport<V: Viewport, R: AsRef<V>>(&self, new_viewport: R) {
		let new = new_viewport.as_ref().root();
		let existing = &self.viewport_pane.get_children()[1];
		if new != existing {
			self.viewport_pane.remove(existing);
			self.viewport_pane.add2(new);
		}
	}
}

/// Shows a save dialog to export a single file.
/// Returns the selected path.
fn dialog_export_single(
	parent: &gtk::ApplicationWindow,
	name: &str,
	format: Option<formats::Format>,
) -> Option<path::PathBuf> {
	let dialog = util::TempDialog::new(gtk::FileChooserDialog::with_buttons(
		Some("Export File"),
		Some(parent),
		gtk::FileChooserAction::Save,
		&[
			("_Cancel", gtk::ResponseType::Cancel),
			("_Save", gtk::ResponseType::Accept),
		],
	));
	dialog.set_do_overwrite_confirmation(true);

	// Add filters
	let default_extension = match format {
		None | Some(formats::Format::Animation(_)) => {
			let filter = gtk::FileFilter::new();
			gtk::FileFilterExt::set_name(&filter, Some("Binary file (*.bin)"));
			filter.add_pattern("*.bin");
			dialog.add_filter(&filter);
			"bin"
		}
		Some(formats::Format::Container(_)) => {
			dialog.set_action(gtk::FileChooserAction::CreateFolder);
			""
		}
		Some(formats::Format::Model(_)) => {
			let filter = gtk::FileFilter::new();
			gtk::FileFilterExt::set_name(&filter, Some("Studiomdl Data (*.smd)"));
			filter.add_pattern("*.smd");
			dialog.add_filter(&filter);
			"smd"
		}
		Some(formats::Format::Texture(_)) => {
			let filter = gtk::FileFilter::new();
			gtk::FileFilterExt::set_name(&filter, Some("PNG Image (*.png)"));
			filter.add_pattern("*.png");
			dialog.add_filter(&filter);
			"png"
		}
	};

	// Add all files filter, if we're not making a folder
	if dialog.get_action() != gtk::FileChooserAction::CreateFolder {
		let filter = gtk::FileFilter::new();
		gtk::FileFilterExt::set_name(&filter, Some("All Files"));
		filter.add_pattern("*");
		dialog.add_filter(&filter);
	}

	let mut name_path = path::PathBuf::from(name.replace('/', "_"));
	name_path.set_extension(default_extension);
	dialog.set_current_name(&name_path);

	if dialog.run() != gtk::ResponseType::Accept.into() {
		return None;
	}
	return dialog.get_filename();
}

/// Converts a byte size to a user-friendly string
pub fn size_to_string(size: u64) -> String {
	if size >= 1024 * 1024 * 1024 {
		return format!("{:.1} GiB", size as f64 / (1024.0 * 1024.0 * 1024.0));
	}
	if size >= 1024 * 1024 {
		return format!("{:.1} MiB", size as f64 / (1024.0 * 1024.0));
	}
	if size >= 1024 {
		return format!("{:.1} KiB", size as f64 / 1024.0);
	}
	return format!("{} B", size);
}
