use gtk::{
	self,
	prelude::*,
};
use std::ops;

/// Wrapper for `gtk::Dialog` that destroys the dialog when dropped.
///
/// Useful for temporary dialogs that should not stay visible after exiting a
/// function.
#[derive(Debug, Clone)]
pub struct TempDialog<W: gtk::WidgetExt> {
	pub dialog: W,
}
impl<W: WidgetExt> TempDialog<W> {
	pub fn new(dialog: W) -> Self {
		Self { dialog }
	}
}
impl<W: WidgetExt> ops::Deref for TempDialog<W> {
	type Target = W;

	fn deref(&self) -> &Self::Target {
		&self.dialog
	}
}
impl<W: WidgetExt> ops::Drop for TempDialog<W> {
	fn drop(&mut self) {
		self.dialog.destroy();
	}
}
