use byteorder::{
	ByteOrder,
	NativeEndian,
};
use cairo;
use glium::{
	self,
	backend::glutin::headless::Headless,
	buffer::Content,
	glutin,
	Surface,
};
use gtk::{
	self,
	prelude::*,
};
use std::{
	cell::{
		self,
		Cell,
		RefCell,
	},
	error::Error,
	mem,
	ops,
	rc::Rc,
	slice,
};

use self::rentals::FBO;

/// Object used for rendering
pub struct Frame<'a> {
	widget: &'a GliumImageWidget,
	backend: cell::RefMut<'a, BackendBuffer>,
	was_drawn: Cell<bool>,
}
impl<'a> Frame<'a> {
	pub fn finish(self) {
		let widget = self.widget;
		self.was_drawn.set(true);
		drop(self);
		widget.finish_drawing();
	}
}
/// Forward surface implementation. Can't just implement DerefMut, since that a)
/// doesn't work with the borrow checker and b) would allow people to reassign
/// the FBO.
impl<'z> glium::Surface for Frame<'z> {
	fn clear(
		&mut self,
		rect: Option<&glium::Rect>,
		color: Option<(f32, f32, f32, f32)>,
		color_srgb: bool,
		depth: Option<f32>,
		stencil: Option<i32>,
	) {
		self.backend
			.fbo
			.rent_mut(|fbo| fbo.clear(rect, color, color_srgb, depth, stencil))
	}

	fn get_dimensions(&self) -> (u32, u32) {
		self.backend.fbo.suffix().get_dimensions()
	}

	fn get_depth_buffer_bits(&self) -> Option<u16> {
		self.backend.fbo.suffix().get_depth_buffer_bits()
	}

	fn get_stencil_buffer_bits(&self) -> Option<u16> {
		self.backend.fbo.suffix().get_stencil_buffer_bits()
	}

	fn draw<'a, 'b, V, I, U>(
		&mut self,
		v: V,
		i: I,
		program: &glium::Program,
		uniforms: &U,
		draw_parameters: &glium::draw_parameters::DrawParameters,
	) -> Result<(), glium::DrawError>
	where
		V: glium::vertex::MultiVerticesSource<'b>,
		I: Into<glium::index::IndicesSource<'a>>,
		U: glium::uniforms::Uniforms,
	{
		self.backend
			.fbo
			.rent_mut(|fbo| fbo.draw(v, i, program, uniforms, draw_parameters))
	}

	fn blit_from_frame(
		&self,
		source_rect: &glium::Rect,
		target_rect: &glium::BlitTarget,
		filter: glium::uniforms::MagnifySamplerFilter,
	) {
		self.backend
			.fbo
			.suffix()
			.blit_from_frame(source_rect, target_rect, filter)
	}

	fn blit_from_simple_framebuffer(
		&self,
		source: &glium::framebuffer::SimpleFrameBuffer,
		source_rect: &glium::Rect,
		target_rect: &glium::BlitTarget,
		filter: glium::uniforms::MagnifySamplerFilter,
	) {
		self.backend.fbo.suffix().blit_from_simple_framebuffer(
			source,
			source_rect,
			target_rect,
			filter,
		)
	}

	fn blit_from_multioutput_framebuffer(
		&self,
		source: &glium::framebuffer::MultiOutputFrameBuffer,
		source_rect: &glium::Rect,
		target_rect: &glium::BlitTarget,
		filter: glium::uniforms::MagnifySamplerFilter,
	) {
		self.backend.fbo.suffix().blit_from_multioutput_framebuffer(
			source,
			source_rect,
			target_rect,
			filter,
		)
	}

	fn blit_color<S>(
		&self,
		source_rect: &glium::Rect,
		target: &S,
		target_rect: &glium::BlitTarget,
		filter: glium::uniforms::MagnifySamplerFilter,
	) where
		S: glium::Surface,
	{
		self.backend
			.fbo
			.suffix()
			.blit_color(source_rect, target, target_rect, filter)
	}
}
impl<'a> ops::Drop for Frame<'a> {
	fn drop(&mut self) {
		if !self.was_drawn.get() {
			panic!("Frame object dropped without calling finish()");
		}
	}
}

/// Structures that are allocated for a specific size
struct BackendBuffer {
	fbo: FBO,
	surface: cairo::ImageSurface,
	size: (u32, u32),
}
impl BackendBuffer {
	fn from_allocation(facade: &Headless, allocation: &gtk::Allocation) -> Self {
		assert!(allocation.width >= 0);
		assert!(allocation.height >= 0);
		let size = (allocation.width as u32, allocation.height as u32);

		Self::from_size(facade, size)
	}

	fn from_size(facade: &Headless, size: (u32, u32)) -> Self {
		let color = glium::texture::Texture2d::empty_with_format(
			facade,
			glium::texture::UncompressedFloatFormat::U8U8U8U8,
			glium::texture::MipmapsOption::NoMipmap,
			size.0,
			size.1,
		)
		.expect("Could not create backend texture");

		let depth_stencil = glium::framebuffer::DepthRenderBuffer::new(
			facade,
			glium::texture::DepthFormat::I24,
			size.0,
			size.1,
		)
		.expect("Could not create backend renderbuffer");

		let fbo_handle = FBO::new(Box::new((color, depth_stencil)), |buffers| {
			glium::framebuffer::SimpleFrameBuffer::with_depth_buffer(facade, &buffers.0, &buffers.1)
				.expect("Could not create backend fbo")
		});

		assert!(fbo_handle.suffix().get_depth_buffer_bits().is_some());

		let surface =
			cairo::ImageSurface::create(cairo::Format::ARgb32, size.0 as i32, size.1 as i32)
				.expect("Could not create cairo image surface");

		BackendBuffer {
			fbo: fbo_handle,
			surface,
			size,
		}
	}
}

/// Wraps an Image widget and provides an OpenGL context for rendering to the
/// widget.
pub struct GliumImageWidget {
	widget: gtk::DrawingArea,
	facade: Headless,
	backend: RefCell<BackendBuffer>,
	last_frame: RefCell<Option<glium::texture::pixel_buffer::PixelBuffer<(u8, u8, u8, u8)>>>,

	func_resize: RefCell<Box<dyn Fn(&GliumImageWidget, (u32, u32))>>,
}
impl GliumImageWidget {
	/// Wraps an existing widget.
	pub fn from_image<CreateCtx>(
		widget: gtk::DrawingArea,
		create_ctx_func: CreateCtx,
	) -> Result<Rc<Self>, Box<dyn Error>>
	where
		CreateCtx: FnOnce(
			glutin::HeadlessRendererBuilder,
		) -> Result<glutin::HeadlessContext, glutin::CreationError>,
	{
		// glium doesn't support resizing the headless context buffer, so make a
		// tiny one and render to a framebuffer instead.
		let builder = glium::glutin::HeadlessRendererBuilder::new(2, 2);
		let context = create_ctx_func(builder)?;
		let facade = Headless::new(context)?;

		let backend = BackendBuffer::from_allocation(&facade, &widget.get_allocation());

		let this = Rc::new(Self {
			widget,
			facade,
			backend: RefCell::new(backend),
			last_frame: RefCell::new(None),

			func_resize: RefCell::new(Box::new(|_, _| {})),
		});

		Self::set_callbacks(&this);

		Ok(this)
	}

	fn set_callbacks(this: &Rc<Self>) {
		let this_weak = Rc::downgrade(this);

		this.widget
			.connect_draw(clone2!(this_weak => move |_,cairo_ctx| {
				let this = upgrade_weak!(this_weak, gtk::Inhibit(false));
				this.draw_widget(cairo_ctx);

				gtk::Inhibit(true)
			}));

		this.widget
			.connect_size_allocate(clone2!(this_weak => move |_, rect| {
				let this = upgrade_weak!(this_weak);
				assert!(rect.width >= 0);
				assert!(rect.height >= 0);

				// Resize backend and clear out any existing wrong sized frame
				let size = (rect.width as u32, rect.height as u32);
				*this.backend.borrow_mut() = BackendBuffer::from_size(&this.facade, size);
				*this.last_frame.borrow_mut() = None;

				// Tell the user that the widget changed size
				(this.func_resize.borrow())(&*this, size);
			}));
	}

	/// Sets a function to call during a resize.
	///
	/// Will panic if called from within the existing resize function.
	pub fn set_resize_func<F: Fn(&GliumImageWidget, (u32, u32)) + 'static>(&self, func: F) {
		*self.func_resize.borrow_mut() = Box::new(func);
	}

	/// Begins drawing a frame.
	pub fn draw<'a>(&'a self) -> Frame<'a> {
		let backend = self.get_backend();

		Frame {
			widget: self,
			backend,
			was_drawn: Cell::new(false),
		}
	}

	/// Gets the glium facade object.
	pub fn facade(&self) -> &impl glium::backend::Facade {
		&self.facade
	}

	/// Gets the widget that this renders to.
	pub fn widget(&self) -> &gtk::DrawingArea {
		&self.widget
	}

	/// Gets the backend, resizing it to the current size if needed
	fn get_backend<'a>(&'a self) -> cell::RefMut<'a, BackendBuffer> {
		let mut backend = self
			.backend
			.try_borrow_mut()
			.expect("Could not get backend. Is there a Frame that wasn't dropped?");

		let current_allocation = self.widget.get_allocation();
		assert!(current_allocation.width >= 0);
		assert!(current_allocation.height >= 0);
		let current_size = (
			current_allocation.width as u32,
			current_allocation.height as u32,
		);

		if backend.size != current_size {
			*backend = BackendBuffer::from_size(&self.facade, current_size);
		}
		backend
	}

	/// Finishes drawing, called when `Frame.finish` is called
	fn finish_drawing(&self) {
		let backend = self.backend.borrow();

		// TODO: glium forces us to make a new pixel buffer every time we want to read...
		*self.last_frame.borrow_mut() = Some(backend.fbo.head().0.read_to_pixel_buffer());
		self.widget.queue_draw();
	}

	/// Copies the rendered frame to the cairo surface, if a frame is available.
	fn read_frame_to_surface(&self, backend: &mut BackendBuffer) {
		let mut frame = match self.last_frame.borrow_mut().take() {
			Some(f) => f,
			None => {
				return;
			}
		};

		let surface_stride = backend.surface.get_stride() as usize;
		let mut surface_data = backend.surface.get_data().unwrap();

		let pbo_size = backend.size.0 as usize * backend.size.1 as usize * 4;
		assert_eq!(frame.get_size(), pbo_size);

		let pbo_map = frame.map_read();
		// TODO: How to safely read PBO data without copying? Seems like a huge
		// oversight that there's no function for this...
		let pbo_data: &[(u8, u8, u8, u8)] =
			unsafe { slice::from_raw_parts(mem::transmute(pbo_map.to_void_ptr()), pbo_size) };

		// Copy data over, moving RGBA to ARGB and flipping OpenGL's Y-up to
		// GTK's Y-down.
		for y in 0..(backend.size.1 as usize) {
			let surface_row_offset = (backend.size.1 as usize - 1 - y) * surface_stride;
			for x in 0..(backend.size.0 as usize) {
				let src_pixel = pbo_data[y * backend.size.0 as usize + x];

				// ARGB is 32 bits in native endian in ARGB order from MSB to LSB,
				// with premultiplied alpha.
				// Store in an integer to get the native endianness.
				let num = ((src_pixel.3 as u32) << 24)
					| ((multiply_alpha(src_pixel.0, src_pixel.3) as u32) << 16)
					| ((multiply_alpha(src_pixel.1, src_pixel.3) as u32) << 8)
					| ((multiply_alpha(src_pixel.2, src_pixel.3) as u32) << 0);
				let mut buf = [0, 0, 0, 0];
				NativeEndian::write_u32(&mut buf, num);

				surface_data[surface_row_offset + x * 4 + 0] = buf[0];
				surface_data[surface_row_offset + x * 4 + 1] = buf[1];
				surface_data[surface_row_offset + x * 4 + 2] = buf[2];
				surface_data[surface_row_offset + x * 4 + 3] = buf[3];
			}
		}
	}

	/// Draws the widget.
	fn draw_widget(&self, cairo_ctx: &cairo::Context) {
		let mut backend = self
			.backend
			.try_borrow_mut()
			.expect("Could not get backend. Is there a Frame that wasn't dropped?");
		self.read_frame_to_surface(&mut backend);

		let pattern = cairo::SurfacePattern::create(&backend.surface);
		cairo_ctx.set_source(&pattern);
		cairo_ctx.paint();
	}
}

fn multiply_alpha(component: u8, alpha: u8) -> u8 {
	(component as u16 * alpha as u16 / 0xff) as u8
}

rental! {
	mod rentals {
		use super::*;

		/// FBO type. Also contains the texture and render buffer that it was created from.
		/// Needs to be rented since the FBO needs to be stored in the same struct
		/// as the attachments that it depends on.
		#[rental(covariant)]
		pub struct FBO {
			buffers: Box<(glium::texture::Texture2d, glium::framebuffer::DepthRenderBuffer)>,
			framebuffer: glium::framebuffer::SimpleFrameBuffer<'buffers>,
		}
	}
}
