#[macro_use]
extern crate glium;
//#[macro_use] extern crate log;
#[macro_use]
extern crate rental;

#[macro_use]
mod macros;

mod export;
mod gtkgl;
mod tree_store_listener;
mod ui;
mod util;
mod viewports;

use gio::prelude::*;
use gtk::prelude::*;
use std::{
	cell::RefCell,
	env::args,
	rc::Rc,
};

thread_local!(static THE_UI: RefCell<Option<Rc<RefCell<ui::Ui>>>> = RefCell::new(None));
fn main() {
	simplelog::TermLogger::init(simplelog::LevelFilter::Info, simplelog::Config::default())
		.expect("Could not initialize logging");

	load_resource(include_bytes!(concat!(
		env!("OUT_DIR"),
		"/resources.gresource"
	)));

	let application =
		gtk::Application::new("net.col32.pokemon-3ds-gui", gio::ApplicationFlags::empty())
			.expect("Could not initialize GTK application");
	application.connect_startup(|app| {
		let the_ui = ui::Ui::new(app);
		the_ui.borrow().window().show_all();
		THE_UI.with(|cell| {
			*cell.borrow_mut() = Some(the_ui);
		});
	});
	application.connect_activate(|_app| {});
	application.connect_shutdown(|_| {
		THE_UI.with(|cell| {
			*cell.borrow_mut() = None;
		});
	});
	application.run(&args().collect::<Vec<_>>());
}

fn load_resource(blob: &'static [u8]) {
	let blob = glib::Bytes::from_static(blob);
	let resource = gio::Resource::new_from_data(&blob).unwrap();
	gio::resources_register(&resource);
}
