use std::{
	env,
	fs::read_to_string,
	path::Path,
	process::Command,
};

fn main() {
	let out_dir = env::var("OUT_DIR").unwrap();

	let out_blob = Path::new(&out_dir).join("resources.gresource");
	let out_deps = Path::new(&out_dir).join("resources.d");

	let status = Command::new("glib-compile-resources")
		.arg("--generate")
		.arg("--dependency-file")
		.arg(&out_deps)
		.arg("--target")
		.arg(&out_blob)
		.arg("resources.gresource.xml")
		.status()
		.expect("Could not execute `glib-compile-resources`, is it installed?");
	if !status.success() {
		panic!("glib-compile-resources exited with code {}", status);
	}

	println!("cargo:rerun-if-changed=resources.gresource.xml");

	let deps = read_to_string(out_deps).expect("Could not read deps file");
	let deps = deps
		.trim_start()
		.trim_start_matches("resources.gresource.xml:")
		.trim_start()
		.trim_end();
	for dep in deps.split_whitespace() {
		println!("cargo:rerun-if-changed={}", dep);
	}
}
