extern crate pokemon_3ds;

use pokemon_3ds::formats::{
	containers::{
		gfmodel_container::GfModelContainer,
		Container,
	},
	identify,
	ContainerFormat,
	Format,
};
use std::{
	cell::RefCell,
	fs,
	io,
	rc::Rc,
};

fn open() -> io::BufReader<fs::File> {
	io::BufReader::new(
		fs::File::open("./test-data/professor_overworld.gfmodelcontainer")
			.expect("Could not open ./test-data/professor_overworld.gfmodelcontainer. Note that this file is not included in the repo.")
	)
}

#[test]
#[ignore]
fn test_identify_professor_overworld() {
	let mut file = open();
	let format = identify(&mut file).expect("Could not identify file");
	assert_eq!(
		format,
		Some(Format::Container(ContainerFormat::GfModelContainer))
	);
}

#[test]
#[ignore]
fn test_read_professor_overworld() {
	let file = open();
	let file = Rc::new(RefCell::new(file));
	let container = GfModelContainer::new(file).expect("Could not parse container");

	let entries = container.entries();
	assert_eq!(entries.len(), 9);
	assert!(entries[0].path.to_string().contains("tr0004_00_fi"));
	assert!(entries[1].path.to_string().contains("tr0004_00_Eye"));
	assert!(entries[2].path.to_string().contains("Chara_Rim_Black_fi"));
	assert!(entries[3].path.to_string().contains("tr0004_00_Body"));
	assert!(entries[4].path.to_string().contains("Chara_Rim_1_fi"));
	assert!(entries[5].path.to_string().contains("FieldChara"));
	assert!(entries[6].path.to_string().contains("FieldChara"));
	assert!(entries[7].path.to_string().contains("CDC_fi_BodyALWF"));
	assert!(entries[8]
		.path
		.to_string()
		.contains("CDC_fi_Body_DoctorGREF"));

	let model_raw = container
		.read_entry(0, 0, None)
		.expect("Could not read entry");
	let model_expected = fs::read("./test-data/professor_overworld.gfmodel")
		.expect("Could not read ./test-data/professor_overworld.gfmodel");
	assert_eq!(model_raw, model_expected);
}
