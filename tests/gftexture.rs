extern crate png;
extern crate pokemon_3ds;
extern crate rgb;

use png::HasParameters;
use pokemon_3ds::formats::{
	identify,
	textures::{
		gftexture,
		TextureData,
	},
	Format,
	TextureFormat,
};
use rgb::FromSlice;
use std::{
	fs,
	io,
};

fn open() -> io::BufReader<fs::File> {
	io::BufReader::new(
		fs::File::open("./test-data/bulbasaur_body.gftexture")
			.expect("Could not open ./test-data/bulbasaur_body.gftexture. Note that this file is not included in the repo.")
	)
}

#[test]
#[ignore]
fn test_identify_bulbasaur_body_texture() {
	let mut file = open();
	let format = identify(&mut file).expect("Could not identify file");
	assert_eq!(format, Some(Format::Texture(TextureFormat::GfTexture)));
}

#[test]
#[ignore]
fn test_read_bulbasaur_body_texture() {
	let mut file = open();
	let texture = gftexture::read_gftexture(&mut file).expect("Could not parse texture");
	assert_eq!(texture.name(), "pm0001_00_BodyA1.tga");
	assert_eq!(texture.width(), 256);
	assert_eq!(texture.height(), 256);
	let decoded_data = match texture.data() {
		TextureData::RGBA(ref data) => data,
		_ => {
			panic!("Unexpected texture data type");
		}
	};

	let mut decoder = png::Decoder::new(fs::File::open("./test-data/bulbasaur_body.png").unwrap());
	decoder.set(png::Transformations::IDENTITY);
	let (info, mut reader) = decoder.read_info().unwrap();
	let mut expected_data_raw = vec![0; info.buffer_size()];
	reader.next_frame(&mut expected_data_raw).unwrap();
	let expected_data = expected_data_raw.as_slice().as_rgba();

	assert_eq!(decoded_data.as_slice(), expected_data);
}
