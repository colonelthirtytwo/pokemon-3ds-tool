extern crate pokemon_3ds;

use pokemon_3ds::formats::{
	identify,
	models::gfmodel,
	Format,
	ModelFormat,
};
use std::{
	fs,
	io,
};

fn open() -> io::BufReader<fs::File> {
	io::BufReader::new(
		fs::File::open("./test-data/bulbasaur.gfmodel")
			.expect("Could not open ./test-data/bulbasaur.gfmodel. Note that this file is not included in the repo.")
	)
}

#[test]
#[ignore]
fn test_identify_bulbasaur_model() {
	let mut file = open();
	let format = identify(&mut file).expect("Could not identify file");
	assert_eq!(format, Some(Format::Model(ModelFormat::GfModel)));
}

#[test]
#[ignore]
fn test_read_bulbasaur_model() {
	let mut file = open();
	let model = gfmodel::read_gfmodel(&mut file, &mut vec![]).expect("Could not parse model");

	assert_eq!(model.materials().len(), 5);
	assert_eq!(model.materials()[0].name, "BodyA");
	assert_eq!(model.materials()[1].name, "BodyB");
	assert_eq!(model.materials()[2].name, "Eye");
	assert_eq!(model.materials()[3].name, "LIris");
	assert_eq!(model.materials()[4].name, "RIris");

	assert_eq!(model.bones().len(), 55);
	assert_eq!(model.bones()[0].name, "pm0001_00");
	assert_eq!(model.bones()[1].name, "Origin");

	assert_eq!(model.mesh().num_vertices(), 3589);
	assert_eq!(model.mesh().faces().len(), 5372);
	assert!(model.mesh().positions().is_some());
	assert!(model.mesh().normals().is_some());
	assert!(model.mesh().texture_coords(0).is_some());
	assert!(model.mesh().bone_weights().is_some());
}
