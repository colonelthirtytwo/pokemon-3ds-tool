extern crate pokemon_3ds;

use pokemon_3ds::formats::{
	containers::{
		pokemon_container::PokemonContainer,
		Container,
	},
	identify,
	ContainerFormat,
	Format,
};
use std::{
	cell::RefCell,
	fs,
	io,
	rc::Rc,
};

fn open() -> io::BufReader<fs::File> {
	io::BufReader::new(fs::File::open("./test-data/bulbasaur.pc").expect(
		"Could not open ./test-data/bulbasaur.pc. Note that this file is not included in the repo.",
	))
}

#[test]
#[ignore]
fn test_identify_bulbasaur_container() {
	let mut file = open();
	let format = identify(&mut file).expect("Could not identify file");
	assert_eq!(format, Some(Format::Container(ContainerFormat::Pokemon)));
}

#[test]
#[ignore]
fn test_read_bulbasaur_container() {
	let file = open();
	let file = Rc::new(RefCell::new(file));
	let container = PokemonContainer::new(file).expect("Could not parse container");

	let entries = container.entries();
	assert_eq!(entries.len(), 5);
	assert_eq!(entries[0].size, 243072);
	assert_eq!(entries[1].size, 32128);
	assert_eq!(entries[2].size, 4096);
	assert_eq!(entries[3].size, 2688);
	assert_eq!(entries[4].size, 128);

	let model_raw = container
		.read_entry(0, 0, None)
		.expect("Could not read entry");
	let model_expected = fs::read("./test-data/bulbasaur.gfmodel")
		.expect("Could not read ./test-data/bulbasaur.gfmodel");
	assert_eq!(model_raw, model_expected);
}
