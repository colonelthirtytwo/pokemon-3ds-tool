
Most of the tests in this folder operate on game assets found off of cartridges, to ensure compatibility. These assets cannot be included in the repository due to copyright issues. Thus, these tests are marked as `#[ignore]`.
