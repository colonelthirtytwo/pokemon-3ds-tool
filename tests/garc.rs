extern crate pokemon_3ds;

use pokemon_3ds::formats::{
	containers::{
		garc::GarcContainer,
		Container,
	},
	identify,
	ContainerFormat,
	Format,
};
use std::{
	cell::RefCell,
	fs,
	io,
	rc::Rc,
};

fn open() -> io::BufReader<fs::File> {
	io::BufReader::new(fs::File::open("./test-data/0_1_4.garc").expect(
		"Could not open ./test-data/0_1_4.garc. Note that this file is not included in the repo.",
	))
}

#[test]
#[ignore]
fn test_identify_0_1_4_container() {
	let mut file = open();
	let format = identify(&mut file).expect("Could not identify file");
	assert_eq!(format, Some(Format::Container(ContainerFormat::GARC)));
}

#[test]
#[ignore]
fn test_read_0_1_4_container() {
	let file = open();
	let file = Rc::new(RefCell::new(file));
	let container = GarcContainer::new(file).expect("Could not parse container");

	let entries = container.entries();
	assert_eq!(entries.len(), 961);
	assert_eq!(entries[0].size, 64);

	let data_raw = container
		.read_entry(0, 0, None)
		.expect("Could not read entry");
	let data_expected =
		fs::read("./test-data/0_1_4_0.bin").expect("Could not read ./test-data/0_1_4_0.bin");
	assert_eq!(data_raw, data_expected);
}
