use id_tree::{
	Node,
	NodeId,
	Tree,
};
use std::{
	cell::RefCell,
	collections::VecDeque,
	fmt,
	fs,
	io,
	rc::{
		Rc,
		Weak,
	},
};

use crate::{
	error,
	formats,
	formats::containers::{
		Entry,
		EntryPath,
	},
	ioutils::ReadSeek,
};

/// Shared reference to a Read+Seek object.
///
/// Should seek before using since other references will leave it at an indeterminate position.
pub type StreamRef = Rc<RefCell<Box<dyn ReadSeek>>>;
type WeakStreamRef = Weak<RefCell<Box<dyn ReadSeek>>>;

/// Tree of data, acting as a sort of filesystem.
///
/// Allows loading and traversing of container and their contents recursively.
pub struct ContainerTree<L: ContainerTreeListener> {
	tree: Tree<NodeData>,
	_root_stream_ref: StreamRef,
	stream_lru_cache: VecDeque<StreamRef>,
	listener: L,
}
impl<L: ContainerTreeListener> ContainerTree<L> {
	pub fn new(
		top_file: io::BufReader<fs::File>,
		top_file_name: String,
		mut listener: L,
	) -> error::Result<Self> {
		let top_stream: StreamRef = Rc::new(RefCell::new(Box::new(top_file)));
		let top_stream_weak = Rc::downgrade(&top_stream);

		let root_entry = {
			let mut stream = top_stream.borrow_mut();
			let size = stream.seek(io::SeekFrom::End(0))?;
			stream.seek(io::SeekFrom::Start(0))?;
			Entry {
				index: 0,
				path: EntryPath::single_component(top_file_name).unwrap(),
				size,
				format_override: None,
			}
		};

		let root_node = NodeData {
			entry: root_entry,
			format: NodeFormat::PendingScan,
			stream_ref: top_stream_weak,
			container: None,
		};

		let mut tree = Tree::new();
		let root_node_id = tree
			.insert(Node::new(root_node), id_tree::InsertBehavior::AsRoot)
			.expect("Tree insert failed");

		listener.node_added(&tree, &root_node_id);

		Ok(Self {
			tree: tree,
			_root_stream_ref: top_stream,
			stream_lru_cache: VecDeque::new(),
			listener,
		})
	}

	pub fn tree(&self) -> &Tree<NodeData> {
		&self.tree
	}

	/// Gets a node id from a path (a sequence of indices to traverse).
	///
	/// None if the path does not exist
	pub fn path_to_node_id<I: IntoIterator<Item = usize>>(&self, path: I) -> Option<&NodeId> {
		let mut iter = path.into_iter();

		// Root node
		if iter.next() != Some(0) {
			return None;
		}

		let mut node_id = self.tree.root_node_id().unwrap();
		for index in iter {
			node_id = self.tree.get(node_id).unwrap().children().get(index)?;
		}
		Some(node_id)
	}

	/// Gets the reader for reading the data for a node
	pub fn stream_ref(&mut self, id: &NodeId) -> error::Result<StreamRef> {
		let (parent_id, child_index) = {
			let node = self.tree.get(id).unwrap();
			if let Some(stream_ref) = node.data().stream_ref.upgrade() {
				return Ok(stream_ref);
			}
			let parent_id = node
				.parent()
				.expect("root's stream_ref was invalid")
				.clone();
			let child_index = node.data().entry.index;
			(parent_id, child_index)
		};

		let stream_ref = {
			let parent_node = self.tree.get(&parent_id).unwrap();
			let container = parent_node
				.data()
				.container
				.as_ref()
				.expect("parent node does not have a container");
			let data = container.read_entry(child_index, 0, None)?;
			Rc::new(RefCell::new(
				Box::new(io::Cursor::new(data)) as Box<dyn ReadSeek>
			)) as StreamRef
		};

		self.stream_lru_cache.push_back(Rc::clone(&stream_ref));
		if self.stream_lru_cache.len() > CACHE_LIMIT {
			self.stream_lru_cache.pop_front();
		}

		let node = self.tree.get_mut(id).unwrap();
		node.data_mut().stream_ref = Rc::downgrade(&stream_ref);
		Ok(stream_ref)
	}

	pub fn get_or_guess_format(&mut self, id: &NodeId) -> error::Result<Option<formats::Format>> {
		// If we already have the format set/cached, return it.
		match self.tree.get(id).unwrap().data().format {
			NodeFormat::Unrecognized => {
				return Ok(None);
			}
			NodeFormat::Recognized(ref f) => {
				return Ok(Some(f.clone()));
			}
			_ => {}
		}

		// Guess format
		let stream_ref = self.stream_ref(id)?;
		let mut stream = stream_ref.borrow_mut();
		stream.seek(io::SeekFrom::Start(0))?;
		let format = formats::identify(&mut *stream)?;

		let node = self.tree.get_mut(id).unwrap();
		node.data_mut().format = if let Some(ref f) = format {
			NodeFormat::Recognized(f.clone())
		} else {
			NodeFormat::Unrecognized
		};
		self.listener.format_set(&self.tree, id);
		Ok(format)
	}

	pub fn set_format(&mut self, id: &NodeId, format: formats::Format) {
		let children = {
			let node = self.tree.get_mut(id).unwrap();
			let new_format = NodeFormat::Recognized(format);

			if node.data().format == new_format {
				// Updating to same format, ignore
				return;
			}

			self.listener.children_deleting(&self.tree, id);

			let node = self.tree.get_mut(id).unwrap();
			node.data_mut().format = NodeFormat::Recognized(format);
			node.data_mut().container = None;
			node.children().clone()
		};

		for id in children.into_iter() {
			self.tree
				.remove_node(id, id_tree::RemoveBehavior::DropChildren)
				.unwrap();
		}

		self.listener.format_set(&self.tree, id);
	}

	pub fn open_container(&mut self, id: &NodeId) -> error::Result<()> {
		if self.tree.get(id).unwrap().data().container.is_some() {
			return Ok(());
		}

		let stream_ref = self.stream_ref(id)?;
		stream_ref.borrow_mut().seek(io::SeekFrom::Start(0))?;

		let container_format = match self.tree.get(id).unwrap().data().format {
			NodeFormat::Recognized(formats::Format::Container(f)) => f.clone(),
			ref f @ _ => panic!("Can't use open_container on item with format {:?}", f),
		};

		let container = container_format.read(stream_ref)?;
		for entry in container.entries() {
			let node = Node::new(NodeData {
				entry: entry.clone(),
				format: if let Some(f) = entry.format_override {
					NodeFormat::Recognized(f.clone())
				} else {
					NodeFormat::PendingScan
				},
				stream_ref: Weak::new(),
				container: None,
			});
			let id = self
				.tree
				.insert(node, id_tree::InsertBehavior::UnderNode(id))
				.unwrap();
			self.listener.node_added(&self.tree, &id);
		}

		self.tree.get_mut(id).unwrap().data_mut().container = Some(container);

		Ok(())
	}
}

const CACHE_LIMIT: usize = 64;

pub struct NodeData {
	entry: Entry,
	format: NodeFormat,
	stream_ref: WeakStreamRef,
	container: Option<Box<dyn formats::containers::Container>>,
}
impl NodeData {
	pub fn entry(&self) -> &Entry {
		&self.entry
	}

	pub fn format(&self) -> NodeFormat {
		self.format.clone()
	}

	pub fn container(&self) -> Option<&dyn formats::containers::Container> {
		self.container.as_ref().map(|b| &**b)
	}
}
impl fmt::Debug for NodeData {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.debug_struct("NodeData")
			.field("entry", &self.entry)
			.field("format", &self.format)
			.field("stream_ref", &"...")
			.field("container", &self.container.is_some())
			.finish()
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum NodeFormat {
	PendingScan,
	Unrecognized,
	Recognized(formats::Format),
}
impl Into<Option<formats::Format>> for NodeFormat {
	fn into(self) -> Option<formats::Format> {
		match self {
			NodeFormat::Recognized(f) => Some(f),
			_ => None,
		}
	}
}

/// Trait for observing changes to the tree.
///
/// Can be used, for example, to update a `gtk::TreeStore`
pub trait ContainerTreeListener: Sized {
	/// Node added
	fn node_added(&mut self, tree: &Tree<NodeData>, node_id: &NodeId);
	/// Deleting all children of a node
	fn children_deleting(&mut self, tree: &Tree<NodeData>, node_id: &NodeId);
	/// Setting the format of a node
	fn format_set(&mut self, tree: &Tree<NodeData>, node_id: &NodeId);
}

/// Implementation of `ContainerTreeListener` that does nothing.
impl ContainerTreeListener for () {
	fn node_added(&mut self, _tree: &Tree<NodeData>, _node_id: &NodeId) {}

	fn children_deleting(&mut self, _tree: &Tree<NodeData>, _node_id: &NodeId) {}

	fn format_set(&mut self, _tree: &Tree<NodeData>, _node_id: &NodeId) {}
}

/// Depth-first iterator-like object that doesn't borrow the tree.
///
/// Allows mutable access to the tree while iterating.
pub struct NodeIdDfsIter(NodeIdDfsIterState);
impl NodeIdDfsIter {
	pub fn new(root: NodeId) -> Self {
		Self(NodeIdDfsIterState::Start(root))
	}

	pub fn next<T>(&mut self, tree: &Tree<T>) -> Option<NodeId> {
		match self.0 {
			NodeIdDfsIterState::Start(ref id) => {
				let id = id.clone();
				self.0 = NodeIdDfsIterState::Progress(vec![(id.clone(), 0)]);
				return Some(id);
			}
			NodeIdDfsIterState::Progress(ref mut stack) => loop {
				let (ref mut top_id, ref mut top_child_index) = stack.last_mut()?.clone();
				let children = tree.get(top_id).unwrap().children();
				if let Some(next_id) = children.get(*top_child_index) {
					*top_child_index += 1;

					stack.push((next_id.clone(), 0));
					return Some(next_id.clone());
				} else {
					stack.pop();
				}
			},
		}
	}
}

enum NodeIdDfsIterState {
	Start(NodeId),
	Progress(Vec<(NodeId, usize)>),
}

pub trait Visitor<L: ContainerTreeListener> {
	fn enter_collection(
		&mut self,
		tree: &mut ContainerTree<L>,
		node_id: &NodeId,
	) -> error::Result<()>;
	fn exit_collection(
		&mut self,
		tree: &mut ContainerTree<L>,
		node_id: &NodeId,
	) -> error::Result<()>;
	fn visit_node(&mut self, tree: &mut ContainerTree<L>, node_id: &NodeId) -> error::Result<()>;
}

pub struct VisitorState<L: ContainerTreeListener, V: Visitor<L>> {
	first: Option<NodeId>,
	state: Vec<(NodeId, usize)>,
	visitor: V,
	_why: std::marker::PhantomData<fn(&L)>,
}
impl<L: ContainerTreeListener, V: Visitor<L>> VisitorState<L, V> {
	pub fn new(root: NodeId, visitor: V) -> Self {
		Self {
			first: Some(root),
			state: vec![],
			visitor,
			_why: Default::default(),
		}
	}

	pub fn drive(&mut self, tree: &mut ContainerTree<L>) -> error::Result<bool> {
		// Find a node to examine
		let next_node;
		loop {
			// First node: bootstrap
			if let Some(n) = self.first.take() {
				next_node = n;
				break;
			}

			// Stack is empty, we've finished.
			if self.state.is_empty() {
				return Ok(false);
			}

			// Grab next ID from top of stack
			let (ref parent_id, ref mut next_idx) = self.state.last_mut().unwrap();
			if let Some(next_node_id) = tree
				.tree()
				.get(parent_id)
				.unwrap()
				.children()
				.get(*next_idx)
			{
				// Next child
				next_node = next_node_id.clone();
				*next_idx += 1;
				break;
			} else {
				// No more children, exit this container and go back to the next level up
				self.visitor.exit_collection(tree, parent_id)?;
				self.state.pop();
			}
		}

		// Get node format
		let fmt = tree.get_or_guess_format(&next_node)?;

		if let Some(formats::Format::Container(_)) = fmt {
			// Descend into container
			tree.open_container(&next_node)?;
			self.visitor.enter_collection(tree, &next_node)?;
			self.state.push((next_node, 0));
		} else {
			// Visit node
			self.visitor.visit_node(tree, &next_node)?;
		}
		Ok(true)
	}

	pub fn visitor(&self) -> &V {
		&self.visitor
	}

	pub fn visitor_mut(&mut self) -> &mut V {
		&mut self.visitor
	}
}
