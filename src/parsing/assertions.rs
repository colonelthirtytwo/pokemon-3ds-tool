use std::{
	cmp::PartialEq,
	fmt::Debug,
};

use crate::error::{
	Error,
	Result,
};

pub fn assert_eq<T: PartialEq + Debug>(actual: T, expected: T, message: &str) -> Result<()> {
	if actual != expected {
		return Err(Error::parsing(format!(
			"{}. Expected {:?}, read {:?}",
			message, expected, actual
		)));
	}
	return Ok(());
}

pub fn assert_valid_range<T: PartialOrd + Debug>(lower: T, upper: T, message: &str) -> Result<()> {
	if lower > upper {
		return Err(Error::parsing(format!(
			"{}. Lower range of {:?} is greater than upper range of {:?}",
			message, lower, upper
		)));
	}
	return Ok(());
}
