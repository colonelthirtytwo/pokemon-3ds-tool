#![allow(dead_code)] // I made a bunch of functions but they aren't all used yet

/// Bunch of functions for reading data from a stream
use std::fmt::Debug;
use std::{
	convert::TryInto,
	io,
};

use crate::error::{
	Error,
	Location,
	Result,
};

/// Reads from a string and compares it to a magic byte sequence, failing if the read data does not match.
///
/// Sets up a `Location` with the magic sequence.
pub fn magic<R: io::Read>(reader: &mut R, magic: &[u8]) -> Result<()> {
	let mut buf = vec![0; magic.len()];
	with_loc!(format!("magic {:X?}", magic), reader.read_exact(&mut buf))?;
	if buf.as_slice() != magic {
		return Err(Error::parsing(format!(
			"Expected magic {:X?}, got {:X?}",
			magic,
			buf.as_slice()
		)));
	}
	Ok(())
}

/// Reads an amount of bytes into a vector
pub fn take<R: io::Read>(reader: &mut R, size: usize) -> Result<Vec<u8>> {
	let mut buf = vec![0; size];
	reader.read_exact(&mut buf)?;
	Ok(buf)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Endian {
	Big,
	Little,
}

macro_rules! impl_endian_reader {
	($ty:ty, $num_bytes:literal, $dyn_ident:ident, $be_ident:ident, $le_ident:ident) => {
		/// Reads an integer from a stream with an endian specified at runtime
		pub fn $dyn_ident<R: io::Read>(reader: &mut R, endian: Endian) -> Result<$ty> {
			match endian {
				Endian::Big => $be_ident(reader),
				Endian::Little => $le_ident(reader),
			}
		}

		/// Reads a big-endian integer from a stream
		pub fn $be_ident<R: io::Read>(reader: &mut R) -> Result<$ty> {
			let mut buf: [u8; $num_bytes] = Default::default();
			reader.read_exact(&mut buf)?;
			Ok(<$ty>::from_be_bytes(buf))
		}

		/// Reads a little-endian integer from a stream
		pub fn $le_ident<R: io::Read>(reader: &mut R) -> Result<$ty> {
			let mut buf: [u8; $num_bytes] = Default::default();
			reader.read_exact(&mut buf)?;
			Ok(<$ty>::from_le_bytes(buf))
		}
	};
}

impl_endian_reader!(u16, 2, read_u16, read_be_u16, read_le_u16);
impl_endian_reader!(i16, 2, read_i16, read_be_i16, read_le_i16);
impl_endian_reader!(u32, 4, read_u32, read_be_u32, read_le_u32);
impl_endian_reader!(i32, 4, read_i32, read_be_i32, read_le_i32);
impl_endian_reader!(u64, 8, read_u64, read_be_u64, read_le_u64);
impl_endian_reader!(i64, 8, read_i64, read_be_i64, read_le_i64);
impl_endian_reader!(f32, 4, read_f32, read_be_f32, read_le_f32);
impl_endian_reader!(f64, 8, read_f64, read_be_f64, read_le_f64);

/// Reads a byte from the stream
pub fn read_u8<R: io::Read>(reader: &mut R) -> Result<u8> {
	let mut buf: [u8; 1] = Default::default();
	reader.read_exact(&mut buf)?;
	Ok(buf[0])
}

/// Reads a signed byte from the stream
pub fn read_i8<R: io::Read>(reader: &mut R) -> Result<i8> {
	let mut buf: [u8; 1] = Default::default();
	reader.read_exact(&mut buf)?;
	Ok(buf[0] as i8)
}

/// Reads a pascal string (i.e. a length prefix then the string data) from a stream.
///
/// Specify one of the `read_$endian_$size` functions for the `size_reader` argument.
///
/// Uses `String::from_utf8_lossy` for conversion.
pub fn read_pascal_str<
	R: io::Read,
	I: Clone + Debug + TryInto<usize>,
	F: FnOnce(&mut R) -> Result<I>,
>(
	reader: &mut R,
	size_reader: F,
) -> Result<String> {
	let size = {
		let _loc = Location::new("string size");
		let size_raw = size_reader(reader)?;
		size_raw
			.clone()
			.try_into()
			.map_err(|_| Error::parsing(format!("Too large string of size {:?}", size_raw)))?
	};
	let buf = with_loc!(format!("string of length {}", size), take(reader, size)?);
	Ok(String::from_utf8_lossy(&buf).into())
}

/// Reads a null terminated string from a stream.
pub fn read_null_terminated_str<R: io::Read>(reader: &mut R) -> Result<String> {
	let _loc = Location::new("null-terminated string");

	let mut buf = vec![];
	loop {
		let len = buf.len();
		buf.resize(len + 1, 0);
		reader.read_exact(&mut buf[len..len + 1])?;
		if buf[len] == 0 {
			buf.resize(len, 0);
			break;
		}
	}
	Ok(String::from_utf8_lossy(&buf).into())
}

pub fn repeat<I: Clone + Debug + TryInto<usize>, T, F: FnMut() -> Result<T>>(
	count: I,
	item_name: &str,
	mut f: F,
) -> Result<Vec<T>> {
	let num_elements = count
		.clone()
		.try_into()
		.map_err(|_| Error::parsing(format!("Too many elements: {:?}", count)))?;
	(0..num_elements)
		.map(|i| with_loc!(format!("{} {}", item_name, i), f()))
		.collect::<Result<Vec<T>>>()
}
