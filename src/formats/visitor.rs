use std::{
	cell::RefCell,
	fs,
	io::{
		self,
		Read,
		Write,
	},
	path,
	rc::Rc,
};

use crate::{
	error::{
		self,
		Error,
	},
	formats::{
		self,
		containers::{
			Container,
			EntryPath,
		},
	},
	ioutils::ReadSeek,
};

/// Defines a visitor that can recursively iterate over a file.
///
/// Use with `ResourceVisitorRunner` to recursively and incrementally iterate
/// over a container or other resource.
///
/// All methods of this trait take slices indicating the container indices
/// and `EntryPath`s leading up to the current item.
///
/// All functions may return an error, in which case iterating is permanently
/// stopped.
pub trait ResourceVisitor {
	/// Visitor error type, returned to stop iteration.
	type Error;

	/// Called when the runner identifies a file as a container and begins to
	/// recurse into it.
	fn container_begin(
		&mut self,
		format: &formats::ContainerFormat,
		container: &dyn Container,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when exiting a container
	fn container_end(
		&mut self,
		container: &dyn Container,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when the runner reads a model
	fn model(
		&mut self,
		format: &formats::ModelFormat,
		model: formats::models::geom::Model,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when the runner reads a texture
	fn texture(
		&mut self,
		format: &formats::TextureFormat,
		texture: formats::textures::Texture,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when the runner reads an animation
	fn animation(
		&mut self,
		format: &formats::AnimationFormat,
		animation: formats::animations::geom::Animation,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when the runner reads an unrecognized file.
	///
	/// The raw data is passed to the function.
	fn unrecognized(
		&mut self,
		data: &[u8],
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when a parsing error occurs.
	///
	/// Called in lieu of other methods on this object. The raw data is passed
	/// to the function, unless the parsing error came from reading the container
	/// entry.
	fn parse_error(
		&mut self,
		err: Error,
		data: Option<&[u8]>,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Result<(), Self::Error>;

	/// Called when an IO error occurs.
	///
	/// This function must return an error, as IO errors indicate a problem with
	/// the underlying file itself.
	fn io_error(
		&mut self,
		err: Error,
		container_path: &[usize],
		file_path: &[EntryPath],
	) -> Self::Error;
}

/// Incrementally parses a file and reports progress to a `ResourceVisitor`.
pub struct ResourceVisitorRunner<Visitor: ResourceVisitor> {
	visitor: Visitor,
	done: bool,
	root_reader: Option<Box<dyn ReadSeek>>,
	container_stack: Vec<Box<dyn Container>>,
	container_path_stack: Vec<usize>,
	file_path_stack: Vec<EntryPath>,
}

impl<Visitor: ResourceVisitor> ResourceVisitorRunner<Visitor> {
	pub fn new<Reader: 'static + io::Read + io::Seek>(visitor: Visitor, reader: Reader) -> Self {
		Self {
			visitor,
			done: false,
			root_reader: Some(Box::new(reader)),
			container_stack: vec![],
			container_path_stack: vec![],
			file_path_stack: vec![],
		}
	}

	pub fn new_from_locked<Reader: 'static + io::Read + io::Seek>(
		visitor: Visitor,
		reader: Rc<RefCell<Reader>>,
	) -> Self {
		Self {
			visitor,
			done: false,
			root_reader: Some(Box::new(LockedReader::new(reader))),
			container_stack: vec![],
			container_path_stack: vec![],
			file_path_stack: vec![],
		}
	}

	/// Tries to read one entry.
	///
	/// This does not necessairly correspond to one `Visitor` callback.
	///
	/// # Returns
	///
	/// If the visitor is finished, returns `false`. If the visitor returned an
	/// error, this function passes that error here.
	pub fn read_one(&mut self) -> Result<bool, Visitor::Error> {
		if self.done {
			// Already done
			return Ok(false);
		}

		assert_eq!(self.container_stack.len(), self.container_path_stack.len());
		assert_eq!(
			self.file_path_stack.len(),
			self.container_path_stack.len().saturating_sub(1)
		);

		// Read the file or entry.
		// Done in a closure to help with error handling
		let result: Result<(), Visitor::Error> = (|| {
			// Increment the entry we are on, if applicable
			if let Some(index) = self.container_path_stack.last_mut() {
				*index += 1;
			}
			// Roll over to next entry if we are at the end
			while !self.container_stack.is_empty()
				&& *self.container_path_stack.last().unwrap()
					>= self.container_stack.last().unwrap().entries().len()
			{
				self.visitor.container_end(
					&**self.container_stack.last().unwrap(),
					&self.container_path_stack,
					&self.file_path_stack,
				)?;
				self.container_stack.pop();
				self.container_path_stack.pop();
				self.file_path_stack.pop();
				if let Some(index) = self.container_path_stack.last_mut() {
					*index += 1;
				} else {
					// Left the last container
					self.done = true;
					return Ok(());
				}
			}

			// If we're at an entry, push its name onto the stack
			if let Some(index) = self.container_path_stack.last() {
				let container = self.container_stack.last().unwrap();
				self.file_path_stack
					.push(container.entries()[*index].path.clone());
			}

			let mut reader = if self.container_stack.is_empty() {
				if let Some(root_reader) = self.root_reader.take() {
					Box::new(root_reader)
				} else {
					self.done = true;
					return Ok(());
				}
			} else {
				let container = self.container_stack.last().unwrap();
				let index = self.container_path_stack.last().unwrap();

				match container.read_entry(*index, 0, None) {
					Ok(data) => Box::new(io::Cursor::new(data)) as Box<dyn ReadSeek>,
					Err(err) => {
						return mod_err_to_visitor_error::<_, io::Cursor<Vec<u8>>>(
							&mut self.visitor,
							None,
							Error::from(err),
							&self.container_path_stack,
							&self.file_path_stack,
						);
					}
				}
			};

			let format = formats::identify(&mut reader).map_err(|err| {
				self.visitor.io_error(
					Error::from(err),
					&self.container_path_stack,
					&self.file_path_stack,
				)
			})?;

			match format {
				None => {
					let mut data = vec![];
					reader.read_to_end(&mut data).map_err(|err| {
						self.visitor.io_error(
							Error::from(err),
							&self.container_path_stack,
							&self.file_path_stack,
						)
					})?;
					self.visitor.unrecognized(
						&data,
						&self.container_path_stack,
						&self.file_path_stack,
					)?;
				}
				Some(formats::Format::Container(format)) => {
					// TODO: this double-wraps Rc<RefCell<T>>.
					let locked_reader = Rc::new(RefCell::new(reader));
					let container = match format.read(Rc::clone(&locked_reader)) {
						Ok(c) => c,
						Err(err) => {
							let mut reader = locked_reader.borrow_mut();
							return mod_err_to_visitor_error(
								&mut self.visitor,
								Some(&mut *reader),
								err,
								&self.container_path_stack,
								&self.file_path_stack,
							);
						}
					};

					self.visitor.container_begin(
						&format,
						&*container,
						&self.container_path_stack,
						&self.file_path_stack,
					)?;
					self.container_stack.push(container);
					self.container_path_stack.push(0);

					let current_path = self.file_path_stack.last().map(|v| v.clone());
					if let Some(current_path) = current_path {
						self.file_path_stack.push(current_path);
					}
				}
				Some(formats::Format::Model(format)) => {
					let model = match format.read(&mut reader) {
						Ok(m) => m,
						Err(err) => {
							return mod_err_to_visitor_error(
								&mut self.visitor,
								Some(&mut reader),
								err,
								&self.container_path_stack,
								&self.file_path_stack,
							);
						}
					};

					self.visitor.model(
						&format,
						model,
						&self.container_path_stack,
						&self.file_path_stack,
					)?;
				}
				Some(formats::Format::Texture(format)) => {
					let texture = match format.read(&mut reader) {
						Ok(m) => m,
						Err(err) => {
							return mod_err_to_visitor_error(
								&mut self.visitor,
								Some(&mut reader),
								err,
								&self.container_path_stack,
								&self.file_path_stack,
							);
						}
					};

					self.visitor.texture(
						&format,
						texture,
						&self.container_path_stack,
						&self.file_path_stack,
					)?;
				}
				Some(formats::Format::Animation(format)) => {
					let animation = match format.read(&mut reader) {
						Ok(m) => m,
						Err(err) => {
							return mod_err_to_visitor_error(
								&mut self.visitor,
								Some(&mut reader),
								err,
								&self.container_path_stack,
								&self.file_path_stack,
							);
						}
					};

					self.visitor.animation(
						&format,
						animation,
						&self.container_path_stack,
						&self.file_path_stack,
					)?;
				}
			}

			Ok(())
		})();
		// If the visitor reported an error, stop forever.
		if let Err(err) = result {
			self.done = true;
			return Err(err);
		}

		// Pop off the entry name we just processed
		self.file_path_stack.pop();
		Ok(true)
	}

	/// Reads the entire file at once.
	///
	/// Shortcut for calling `read_one` in a loop.
	pub fn read_all(&mut self) -> Result<(), Visitor::Error> {
		loop {
			match self.read_one() {
				Ok(true) => {}
				Ok(false) => {
					return Ok(());
				}
				Err(err) => {
					return Err(err);
				}
			}
		}
	}

	/// Calculates approximately how far into the file the runner is in.
	///
	/// Useful for progress bars
	///
	/// # Returns
	/// Returns a fraction from 0 to 1.
	pub fn progress(&self) -> f64 {
		if self.done {
			return 1.;
		}
		if self.container_stack.is_empty() {
			return 0.;
		}

		let mut total_factor = 0.;
		let mut current_relative_factor = 1.;
		for (container, &i) in self
			.container_stack
			.iter()
			.zip(self.container_path_stack.iter())
		{
			let this_factor = i as f64 / container.entries().len() as f64;
			total_factor += current_relative_factor * this_factor;
			let i_factor = 1 as f64 / container.entries().len() as f64;
			current_relative_factor = i_factor * current_relative_factor;
		}
		return total_factor;
	}
}

fn mod_err_to_visitor_error<V: ResourceVisitor, Reader: io::Read + io::Seek>(
	visitor: &mut V,
	reader: Option<&mut Reader>,
	err: Error,
	container_path: &[usize],
	file_path: &[EntryPath],
) -> Result<(), V::Error> {
	if err.is_io() {
		return Err(visitor.io_error(err, container_path, file_path));
	} else {
		let buf = if let Some(reader) = reader {
			reader
				.seek(io::SeekFrom::Start(0))
				.map_err(|err| visitor.io_error(Error::from(err), container_path, file_path))?;
			let mut buf = vec![];
			reader
				.read_to_end(&mut buf)
				.map_err(|err| visitor.io_error(Error::from(err), container_path, file_path))?;
			Some(buf)
		} else {
			None
		};
		return visitor.parse_error(
			err,
			buf.as_ref().map(|v| v.as_slice()),
			container_path,
			file_path,
		);
	}
}

/// Wraps `Rc<RefCell<Reader>>` and implements `io::Read+io::Seek` on it.
/// Reads and seeks borrow the refcell before
struct LockedReader<Reader: io::Read + io::Seek> {
	pub reader: Rc<RefCell<Reader>>,
}
impl<Reader: io::Read + io::Seek> LockedReader<Reader> {
	pub fn new(reader: Rc<RefCell<Reader>>) -> Self {
		Self { reader }
	}
}
impl<Reader: io::Read + io::Seek> io::Read for LockedReader<Reader> {
	fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
		self.reader.borrow_mut().read(buf)
	}
}
impl<Reader: io::Read + io::Seek> io::Seek for LockedReader<Reader> {
	fn seek(&mut self, pos: io::SeekFrom) -> io::Result<u64> {
		self.reader.borrow_mut().seek(pos)
	}
}

/// `ResourceVisitor` that exports files to a folder.
pub struct ExportVisitor {
	out_path: path::PathBuf,
}
impl ExportVisitor {
	/// Creates a new visitor that exports to the passed folder
	pub fn new(out_path: path::PathBuf) -> Self {
		Self { out_path }
	}

	fn path_for_entry(&self, file_path: &[EntryPath]) -> path::PathBuf {
		let entry_path_as_file_paths = file_path.iter().flat_map(|entry_path| {
			entry_path
				.iter()
				.map(|component| path::Path::new(component))
		});
		(&[&*self.out_path])
			.iter()
			.cloned()
			.chain(entry_path_as_file_paths)
			.collect::<path::PathBuf>()
	}

	fn dir_for_entry(&self, file_path: &[EntryPath]) -> path::PathBuf {
		let mut file_path = self.path_for_entry(file_path);
		file_path.pop();
		file_path
	}
}
impl ResourceVisitor for ExportVisitor {
	type Error = error::Error;

	fn container_begin(
		&mut self,
		_format: &formats::ContainerFormat,
		_container: &dyn Container,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		info!("Entering container {}", paths_to_str(file_path));
		fs::create_dir_all(&self.path_for_entry(file_path)).map_err(|e| Error::from(e))
	}

	fn container_end(
		&mut self,
		_container: &dyn Container,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		info!("Leaving container {}", paths_to_str(file_path));
		Ok(())
	}

	fn model(
		&mut self,
		_format: &formats::ModelFormat,
		model: formats::models::geom::Model,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		let mut out_path = self.path_for_entry(file_path);
		if !file_path.is_empty() {
			out_path.set_extension("smd");
		}

		info!(
			"Saving model {} to {}",
			paths_to_str(file_path),
			out_path.display()
		);

		fs::create_dir_all(&self.dir_for_entry(file_path))?;

		let file = fs::File::create(&out_path).map_err(|e| Error::from(e))?;
		let mut writer = io::BufWriter::new(file);
		formats::export::smd::write_smd_model(&mut writer, &model).map_err(|e| Error::from(e))?;
		Ok(())
	}

	fn texture(
		&mut self,
		_format: &formats::TextureFormat,
		texture: formats::textures::Texture,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		let mut out_path = self.path_for_entry(file_path);
		if !file_path.is_empty() {
			out_path.set_extension("png");
		}

		info!(
			"Saving texture {} to {}",
			paths_to_str(file_path),
			out_path.display()
		);

		fs::create_dir_all(&self.dir_for_entry(file_path))?;

		let file = fs::File::create(&out_path).map_err(|e| Error::from(e))?;
		let mut writer = io::BufWriter::new(file);
		texture.write_png(&mut writer).map_err(|e| Error::from(e))?;
		Ok(())
	}

	fn animation(
		&mut self,
		_format: &formats::AnimationFormat,
		_animation: formats::animations::geom::Animation,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		warn!("Skipping {}: Animation export NYI", paths_to_str(file_path));
		Ok(())
	}

	fn unrecognized(
		&mut self,
		data: &[u8],
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		if file_path.is_empty() {
			return Err(Error::parsing("Unrecognized file"));
		}
		let mut out_path = self.path_for_entry(file_path);
		out_path.set_extension("bin");

		warn!(
			"Unrecognized format for {}, saving raw data to {}",
			paths_to_str(file_path),
			out_path.display()
		);

		fs::create_dir_all(&self.dir_for_entry(file_path))?;

		let file = fs::File::create(&out_path).map_err(|e| Error::from(e))?;
		let mut writer = io::BufWriter::new(file);
		writer.write_all(data).map_err(|e| Error::from(e))?;
		Ok(())
	}

	fn parse_error(
		&mut self,
		err: Error,
		data: Option<&[u8]>,
		_container_path: &[usize],
		file_path: &[EntryPath],
	) -> error::Result<()> {
		if file_path.is_empty() {
			return Err(err);
		}

		if let Some(data) = data {
			let mut out_path = self.path_for_entry(file_path);
			out_path.set_extension("bin");

			warn!(
				"Could not parse {}, saving raw data to {}\n{}",
				paths_to_str(file_path),
				out_path.display(),
				err
			);

			fs::create_dir_all(&self.dir_for_entry(file_path))?;

			let file = fs::File::create(&out_path).map_err(|e| Error::from(e))?;
			let mut writer = io::BufWriter::new(file);
			writer.write_all(data).map_err(|e| Error::from(e))?;
		} else {
			warn!("Could not parse {}\n{}", paths_to_str(file_path), err);
		}
		Ok(())
	}

	fn io_error(
		&mut self,
		err: Error,
		_container_path: &[usize],
		_file_path: &[EntryPath],
	) -> Self::Error {
		err
	}
}

fn paths_to_str(paths: &[EntryPath]) -> String {
	if paths.is_empty() {
		"/".to_string()
	} else {
		paths.iter().collect::<EntryPath>().to_string()
	}
}
