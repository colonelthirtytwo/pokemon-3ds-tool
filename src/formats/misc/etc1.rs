//! Module for reading 3DS-style ETC1-compressed textures.
//!
//! ETC1 is a compressed format that stores 4x4 RGB tiles using 8 bytes per tile.
//! See [the EXT1 OpenGL extension](https://www.khronos.org/registry/OpenGL/extensions/OES/OES_compressed_ETC1_RGB8_texture.txt)
//! for information on the format.
//!
//! The 3DS format has two major differences from the standard ETC1 format: each
//! block is stored in little endian (traditional is big-endian), and the blocks
//! themselves are organized in a tile pattern rather than a flat grid.

use rgb::{
	ComponentMap,
	RGB8,
	RGBA8,
};
use std::{
	cmp,
	io,
};

use crate::error::Result;

fn clamp<T: Ord>(v: T, min: T, max: T) -> T {
	cmp::min(cmp::max(v, min), max)
}

const ETC1_LUT: &'static [[i16; 4]] = &[
	[2, 8, -2, -8],
	[5, 17, -5, -17],
	[9, 29, -9, -29],
	[13, 42, -13, -42],
	[18, 60, -18, -60],
	[24, 80, -24, -80],
	[33, 106, -33, -106],
	[47, 183, -47, -183],
];

/// Decodes an ETC1-formatted texture.
pub fn read_etc1<Reader: io::Read>(
	reader: &mut Reader,
	width: usize,
	height: usize,
) -> Result<Vec<RGB8>> {
	let mut output: Vec<RGB8> = vec![Default::default(); width * height];

	for tile_y in 0..(height / 8) {
		for tile_x in 0..(width / 8) {
			for block_y in 0..2 {
				for block_x in 0..2 {
					let mut color_block: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
					reader.read_exact(&mut color_block)?;
					color_block.reverse(); // endian reverse

					let tile = read_etc1_block(&color_block);

					for pixel_y in 0..4 {
						for pixel_x in 0..4 {
							let tile_px = tile_x * 8;
							let tile_py = tile_y * 8;
							let block_px = tile_px + block_x * 4;
							let block_py = tile_py + block_y * 4;
							let x = block_px + pixel_x;
							let y = block_py + pixel_y;
							if x >= width || y >= height {
								// out of bounds
								continue;
							}

							let in_offset = pixel_y * 4 + pixel_x;
							let out_offset = y * width + x;

							let color = tile[in_offset];
							output[out_offset] = color;
						}
					}
				}
			}
		}
	}

	Ok(output)
}

/// Decodes an ETC1A4-formatted texture.
///
/// This is similar to ETC1, except that each tile has an additional 8 bytes for
/// specifying the alpha of each pixel, using 4 bits per pixel.
pub fn read_etc1a4<Reader: io::Read>(
	reader: &mut Reader,
	width: usize,
	height: usize,
) -> Result<Vec<RGBA8>> {
	let mut output: Vec<RGBA8> = vec![Default::default(); width * height];

	for tile_y in 0..(height / 8) {
		for tile_x in 0..(width / 8) {
			for block_y in 0..2 {
				for block_x in 0..2 {
					let mut alpha_block: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
					reader.read_exact(&mut alpha_block)?;
					let mut color_block: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
					reader.read_exact(&mut color_block)?;
					color_block.reverse(); // endian reverse

					let tile = read_etc1_block(&color_block);

					for pixel_y in 0..4 {
						for pixel_x in 0..4 {
							let tile_px = tile_x * 8;
							let tile_py = tile_y * 8;
							let block_px = tile_px + block_x * 4;
							let block_py = tile_py + block_y * 4;
							let x = block_px + pixel_x;
							let y = block_py + pixel_y;
							if x >= width || y >= height {
								// out of bounds
								continue;
							}

							let in_offset = pixel_y * 4 + pixel_x;
							let out_offset = y * width + x;
							let alpha_offset = (pixel_x * 4 + pixel_y) / 2;
							let alpha_msb = pixel_y % 2 != 0;

							let color = tile[in_offset];
							let alpha = if alpha_msb {
								alpha_block[alpha_offset] >> 4
							} else {
								alpha_block[alpha_offset] & 0x0f
							};

							output[out_offset] = color.alpha(alpha | (alpha << 4));
						}
					}
				}
			}
		}
	}

	Ok(output)
}

fn read_etc1_block(block: &[u8]) -> [RGB8; 16] {
	assert_eq!(block.len(), 8);
	let flip = (block[3] & 0x1) != 0;
	let difference = (block[3] & 0x2) != 0;
	let table0 = ((block[3] >> 5) & 0x7) as usize;
	let table1 = ((block[3] >> 2) & 0x7) as usize;

	// Get base colors
	let (subblock_color0, subblock_color1) = if difference {
		let base_color = (&block[0..3])
			.iter()
			.map(|&v| {
				// Base color occupies the top 5 bits
				let raw_base = v >> 3;
				// Scale number to 8 bytes, fill lower bits with repetition of MSB.
				let eight_bit_component = (raw_base << 3) | (raw_base >> 2);
				eight_bit_component
			})
			.collect::<RGB8>();

		let delta_color = (&block[0..3])
			.iter()
			.map(|&v| {
				let raw_base = v >> 3;

				// Delta color occupies the bottom 3 bits as a 2's comp. integer
				let raw_delta = (v & 0x7) as i8;

				// Negate the 3-bit number if the top bit was set
				let signed_delta = if raw_delta >= 4 {
					raw_delta - 8
				} else {
					raw_delta
				};

				// Add base + delta
				let summed = clamp(raw_base as i16 + signed_delta as i16, 0, 0x1f) as u8;

				// Scale number to 8 bytes, fill lower bits with repetition of MSB.
				let eight_bit_component = (summed << 3) | (summed >> 2);
				eight_bit_component
			})
			.collect::<RGB8>();

		(base_color, delta_color)
	} else {
		let color0 = (&block[0..3])
			.iter()
			.map(|&v| {
				// Color 0 occupies the top 4 bits
				let raw = v >> 4;
				// Scale number to 8 bytes, fill lower bits with repetition of MSB.
				raw | (raw << 4)
			})
			.collect::<RGB8>();

		let color1 = (&block[0..3])
			.iter()
			.map(|&v| {
				// Color 0 occupies the bottom 4 bits
				let raw = v & 0x0f;
				// Scale number to 8 bytes, fill lower bits with repetition of MSB.
				raw | (raw << 4)
			})
			.collect::<RGB8>();

		(color0, color1)
	};

	// Apply intensity modifier tables to the colors
	let colors0 = iter_to_4_array(ETC1_LUT[table0].iter().map(|modifier| {
		subblock_color0.map(|component| clamp(component as i16 + modifier, 0, 0xff) as u8)
	}));

	let colors1 = iter_to_4_array(ETC1_LUT[table1].iter().map(|modifier| {
		subblock_color1.map(|component| clamp(component as i16 + modifier, 0, 0xff) as u8)
	}));

	// Sample the tables at each position
	let grid_iter = (0..4).flat_map(|y| (0..4).map(move |x| (x, y)));
	let output = iter_to_16_array(grid_iter.map(|(x, y)| {
		// Get which subblock pallete to use
		let subblock_table = if flip {
			// first two rows use subblock0, last two rows use subblock 1
			if y < 2 {
				&colors0
			} else {
				&colors1
			}
		} else {
			// first two columns use subblock0, last two columns use subblock 1
			if x < 2 {
				&colors0
			} else {
				&colors1
			}
		};

		// Get pallete index to use
		let coord_index = x * 4 + y;
		let bit_index = coord_index % 8;
		let msb_byte = if coord_index < 8 { 5 } else { 4 };
		let lsb_byte = if coord_index < 8 { 7 } else { 6 };
		let msb = if block[msb_byte] & (1 << bit_index) != 0 {
			1
		} else {
			0
		};
		let lsb = if block[lsb_byte] & (1 << bit_index) != 0 {
			1
		} else {
			0
		};
		let palette_index = ((msb << 1) | lsb) as usize;

		subblock_table[palette_index]
	}));
	output
}

// Takes an iterator of 4 values and returns a length-4 array.
// Since `[T;4]` doesn't implement `FromIterator`
fn iter_to_4_array<T, I: Iterator<Item = T>>(mut iter: I) -> [T; 4] {
	[
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
	]
}

/// Same as `iter_to_4_array` but with 16 elements.
fn iter_to_16_array<T, I: Iterator<Item = T>>(mut iter: I) -> [T; 16] {
	[
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
		iter.next().unwrap(),
	]
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::fmt::Write;

	fn test_block_decode(test_data: [u8; 8], expected: [RGB8; 16]) {
		let actual = read_etc1_block(&test_data[..]);
		for i in 0..16 {
			assert_eq!(
				expected[i],
				actual[i],
				"\nNot equal index {:?}\nEncoded: {:02x?}\n{}",
				i,
				test_data,
				format_expected(&expected[..], &actual[..])
			);
		}
	}

	fn format_expected(expected: &[RGB8], actual: &[RGB8]) -> String {
		let mut string = "Expected | Actual\n".to_string();
		for (expected_v, actual_v) in expected.iter().zip(actual.iter()) {
			write!(
				&mut string,
				"{:02x} {:02x} {:02x} | {:02x} {:02x} {:02x}\n",
				expected_v.r, expected_v.g, expected_v.b, actual_v.r, actual_v.g, actual_v.b,
			)
			.unwrap();
		}
		string
	}

	#[test]
	fn block_decode_red() {
		let test_data: [u8; 8] = [0xff, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xfd,
				g: 0x00,
				b: 0x00,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_green() {
		let test_data: [u8; 8] = [0x00, 0xff, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
			RGB8 {
				r: 0x00,
				g: 0xfd,
				b: 0x00,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_blue() {
		let test_data: [u8; 8] = [0x00, 0x00, 0xff, 0x00, 0xff, 0xff, 0x00, 0x00];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
			RGB8 {
				r: 0x00,
				g: 0x00,
				b: 0xfd,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_rand1() {
		let test_data: [u8; 8] = [0x88, 0x78, 0x95, 0x91, 0x78, 0x5a, 0x67, 0x22];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0x9a,
				g: 0x89,
				b: 0xab,
			},
			RGB8 {
				r: 0x76,
				g: 0x65,
				b: 0x87,
			},
			RGB8 {
				r: 0xc4,
				g: 0xb3,
				b: 0xd5,
			},
			RGB8 {
				r: 0x76,
				g: 0x65,
				b: 0x87,
			},
			RGB8 {
				r: 0x4c,
				g: 0x3b,
				b: 0x5d,
			},
			RGB8 {
				r: 0xc4,
				g: 0xb3,
				b: 0xd5,
			},
			RGB8 {
				r: 0xc4,
				g: 0xb3,
				b: 0xd5,
			},
			RGB8 {
				r: 0x4c,
				g: 0x3b,
				b: 0x5d,
			},
			RGB8 {
				r: 0x9a,
				g: 0x9a,
				b: 0x67,
			},
			RGB8 {
				r: 0x76,
				g: 0x76,
				b: 0x43,
			},
			RGB8 {
				r: 0xc4,
				g: 0xc4,
				b: 0x91,
			},
			RGB8 {
				r: 0x4c,
				g: 0x4c,
				b: 0x19,
			},
			RGB8 {
				r: 0x76,
				g: 0x76,
				b: 0x43,
			},
			RGB8 {
				r: 0x9a,
				g: 0x9a,
				b: 0x67,
			},
			RGB8 {
				r: 0x76,
				g: 0x76,
				b: 0x43,
			},
			RGB8 {
				r: 0x9a,
				g: 0x9a,
				b: 0x67,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_rand2() {
		let test_data: [u8; 8] = [0x99, 0xb6, 0x89, 0x6c, 0x37, 0xc4, 0xf0, 0xb4];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0xa6,
				g: 0xc8,
				b: 0x95,
			},
			RGB8 {
				r: 0xc3,
				g: 0xe5,
				b: 0xb2,
			},
			RGB8 {
				r: 0x8c,
				g: 0x59,
				b: 0x8c,
			},
			RGB8 {
				r: 0x6f,
				g: 0x3c,
				b: 0x6f,
			},
			RGB8 {
				r: 0xa6,
				g: 0xc8,
				b: 0x95,
			},
			RGB8 {
				r: 0xc3,
				g: 0xe5,
				b: 0xb2,
			},
			RGB8 {
				r: 0x8c,
				g: 0x59,
				b: 0x8c,
			},
			RGB8 {
				r: 0x6f,
				g: 0x3c,
				b: 0x6f,
			},
			RGB8 {
				r: 0x6f,
				g: 0x91,
				b: 0x5e,
			},
			RGB8 {
				r: 0x8c,
				g: 0xae,
				b: 0x7b,
			},
			RGB8 {
				r: 0x8c,
				g: 0x59,
				b: 0x8c,
			},
			RGB8 {
				r: 0xc3,
				g: 0x90,
				b: 0xc3,
			},
			RGB8 {
				r: 0xa6,
				g: 0xc8,
				b: 0x95,
			},
			RGB8 {
				r: 0x6f,
				g: 0x91,
				b: 0x5e,
			},
			RGB8 {
				r: 0xa6,
				g: 0x73,
				b: 0xa6,
			},
			RGB8 {
				r: 0xc3,
				g: 0x90,
				b: 0xc3,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_rand3() {
		let test_data: [u8; 8] = [0x88, 0x56, 0xb7, 0xc9, 0x32, 0x1a, 0x6d, 0x1c];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0xa9,
				g: 0x76,
				b: 0xdc,
			},
			RGB8 {
				r: 0x1e,
				g: 0x00,
				b: 0x51,
			},
			RGB8 {
				r: 0xf2,
				g: 0xbf,
				b: 0xff,
			},
			RGB8 {
				r: 0x67,
				g: 0x34,
				b: 0x9a,
			},
			RGB8 {
				r: 0x67,
				g: 0x34,
				b: 0x9a,
			},
			RGB8 {
				r: 0xa9,
				g: 0x76,
				b: 0xdc,
			},
			RGB8 {
				r: 0x67,
				g: 0x34,
				b: 0x9a,
			},
			RGB8 {
				r: 0x1e,
				g: 0x00,
				b: 0x51,
			},
			RGB8 {
				r: 0xa5,
				g: 0x83,
				b: 0x94,
			},
			RGB8 {
				r: 0x91,
				g: 0x6f,
				b: 0x80,
			},
			RGB8 {
				r: 0xa5,
				g: 0x83,
				b: 0x94,
			},
			RGB8 {
				r: 0xa5,
				g: 0x83,
				b: 0x94,
			},
			RGB8 {
				r: 0x6b,
				g: 0x49,
				b: 0x5a,
			},
			RGB8 {
				r: 0x91,
				g: 0x6f,
				b: 0x80,
			},
			RGB8 {
				r: 0xa5,
				g: 0x83,
				b: 0x94,
			},
			RGB8 {
				r: 0x91,
				g: 0x6f,
				b: 0x80,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_two_shades_of_red() {
		let test_data: [u8; 8] = [0xf0, 0x00, 0x00, 0x02, 0xff, 0xff, 0x33, 0x33];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xef,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
			RGB8 {
				r: 0xf5,
				g: 0x00,
				b: 0x00,
			},
		];
		test_block_decode(test_data, expected);
	}

	#[test]
	fn block_decode_banding_issue() {
		let test_data: [u8; 8] = [0x77, 0x87, 0x88, 0x03, 0xaa, 0xae, 0x00, 0x00];
		let expected: [RGB8; 16] = [
			RGB8 {
				r: 0x75,
				g: 0x86,
				b: 0x8e,
			},
			RGB8 {
				r: 0x75,
				g: 0x86,
				b: 0x8e,
			},
			RGB8 {
				r: 0x75,
				g: 0x86,
				b: 0x8e,
			},
			RGB8 {
				r: 0x75,
				g: 0x86,
				b: 0x8e,
			},
			RGB8 {
				r: 0x71,
				g: 0x82,
				b: 0x8a,
			},
			RGB8 {
				r: 0x71,
				g: 0x82,
				b: 0x8a,
			},
			RGB8 {
				r: 0x71,
				g: 0x82,
				b: 0x8a,
			},
			RGB8 {
				r: 0x71,
				g: 0x82,
				b: 0x8a,
			},
			RGB8 {
				r: 0x69,
				g: 0x79,
				b: 0x8a,
			},
			RGB8 {
				r: 0x6d,
				g: 0x7d,
				b: 0x8e,
			},
			RGB8 {
				r: 0x6d,
				g: 0x7d,
				b: 0x8e,
			},
			RGB8 {
				r: 0x6d,
				g: 0x7d,
				b: 0x8e,
			},
			RGB8 {
				r: 0x69,
				g: 0x79,
				b: 0x8a,
			},
			RGB8 {
				r: 0x69,
				g: 0x79,
				b: 0x8a,
			},
			RGB8 {
				r: 0x69,
				g: 0x79,
				b: 0x8a,
			},
			RGB8 {
				r: 0x69,
				g: 0x79,
				b: 0x8a,
			},
		];
		test_block_decode(test_data, expected);
	}
}
