//! PICA200 GPU instruction set emulator.

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use nalgebra::base::Vector4;
use num_traits::FromPrimitive;
use std::{
	io::Read,
	iter,
	ops,
};

use crate::error::{
	Error,
	Result,
};

/// Parser for the PICA200 GPU instruction set. Models store PICA200 instructions
/// to define the mesh format and other rendering details.
///
/// See (3dbrew.org's page of GPU registers)[https://www.3dbrew.org/wiki/GPU/Internal_Registers]
/// for the description of registers.
pub struct PicaBuffer {
	commands: Vec<u32>,
	lookup_table: Vec<u32>,
}
impl PicaBuffer {
	/// Reads and emulates instructions from a reader.
	///
	/// # Parameters
	///
	/// * `total_instructions`: Number of instructions to read and emulate.
	/// * `align`: If true, after reading an odd number of extra_data bytes, skip
	///   a byte to align the next read.
	pub fn read<Reader: Read>(
		reader: &mut Reader,
		total_instructions: usize,
		align: bool,
	) -> Result<Self> {
		let mut this = Self {
			commands: vec![0; 0x10000],
			lookup_table: vec![0; 256],
		};
		this.do_read(reader, total_instructions, align)?;
		Ok(this)
	}

	fn do_read<Reader: Read>(
		&mut self,
		reader: &mut Reader,
		total_instructions: usize,
		align: bool,
	) -> Result<()> {
		let mut float_uniforms: Vec<Vec<f32>> = vec![vec![]; 96];
		let mut instructions_read = 0;
		let mut words_read = 0;
		let mut current_uniform = 0;

		while instructions_read < total_instructions {
			let mut lookup_table_index = 0;

			if total_instructions - instructions_read < 2 {
				return Err(Error::parsing(format!(
					"not enough data passed to PICA reader (during opcode)"
				)));
			}
			let parameter = reader.read_u32::<LittleEndian>()?;
			let header = reader.read_u32::<LittleEndian>()?;
			instructions_read += 2;
			words_read += 2;

			let mut id = (header & 0xffff) as u16;
			let mask = (header >> 16) & 0xf;
			let extra_parameters = (header >> 20) & 0x7ff;
			let consecutive_writing = (header & 0x80000000) != 0;

			self.commands[id as usize] =
				(self[id] & (!mask & 0xf)) | (parameter & (0xfffffff0 | mask));
			match id {
				consts::BLOCKEND => {
					break;
				}
				consts::VERTEXSHADERFLOATUNIFORMCONFIG => {
					current_uniform = parameter & 0x7fffffff;
				}
				consts::VERTEXSHADERFLOATUNIFORMDATA => {
					float_uniforms[current_uniform as usize].push(f32::from_bits(self[id]))
				}
				consts::FRAGMENTSHADERLOOKUPTABLEDATA => {
					self.lookup_table[lookup_table_index] = self[id];
					lookup_table_index += 1;
				}
				_ => (),
			}

			for _ in 0..extra_parameters {
				if consecutive_writing {
					id += 1;
				}

				if total_instructions - instructions_read == 0 {
					return Err(Error::parsing(format!(
						"not enough data passed to PICA reader (during extra parameters)"
					)));
				}

				let extra_parameter = reader.read_u32::<LittleEndian>()?;
				self.commands[id as usize] =
					(self[id] & (!mask & 0xf)) | (extra_parameter & (0xfffffff0 | mask));
				instructions_read += 1;
				words_read += 1;

				if (id as u16) > consts::VERTEXSHADERFLOATUNIFORMCONFIG
					&& (id as u16) < consts::VERTEXSHADERFLOATUNIFORMDATA + 8
				{
					float_uniforms[current_uniform as usize].push(f32::from_bits(self[id]));
				} else if id as u16 == consts::FRAGMENTSHADERLOOKUPTABLEDATA {
					self.lookup_table[lookup_table_index] = self[id];
					lookup_table_index += 1;
				}
			}

			if align {
				while words_read % 2 == 1 {
					reader.read_u32::<LittleEndian>()?;
					words_read += 1;
				}
			}
		}
		Ok(())
	}

	/// Returns the number of vertex attributes in a buffer.
	fn buffer_num_attributes(&self, buffer: u8) -> usize {
		let v = self[consts::VERTEXSHADERATTRIBUTESBUFFER0STRIDE + buffer as u16 * 3] as usize;
		(v >> 28) as usize
	}

	/// Returns a list of indexes for a buffer.
	/// These indexes are used to index into several other lists.
	fn buffer_indices(&self, buffer: u8) -> impl Iterator<Item = usize> {
		let indices_lo = self[consts::VERTEXSHADERATTRIBUTESBUFFER0PERMUTATION + buffer as u16 * 3];
		let indices_hi = self[consts::VERTEXSHADERATTRIBUTESBUFFER0STRIDE + buffer as u16 * 3];
		let indices_bits = indices_lo as u64 | ((indices_hi as u64 & 0xffff) << 32);
		let num_attributes = self.buffer_num_attributes(buffer);
		(0..num_attributes).map(move |i| {
			let index = (indices_bits >> (i * 4)) & 0xf;
			index as usize
		})
	}

	fn buffer_attribute_storage(&self, index: usize) -> (VertexAttributeStorageType, u32) {
		let formats_lo = self[consts::VERTEXSHADERATTRIBUTESBUFFERFORMATLOW];
		let formats_hi = self[consts::VERTEXSHADERATTRIBUTESBUFFERFORMATHIGH];
		let formats_bits = (formats_lo as u64) | ((formats_hi as u64) << 32);

		let value = (formats_bits >> (index * 4)) & 0xf;
		// Following unwrap should never fail: we have an enum for all 4 possible values
		let typ = VertexAttributeStorageType::from_u64(value & 0b11).unwrap();
		let length = (value >> 2) + 1;
		(typ, length as u32)
	}

	fn buffer_attribute_value_type(&self, index: usize) -> VertexAttributeValueType {
		let types_lo = self[consts::VERTEXSHADERATTRIBUTESPERMUTATIONLOW];
		let types_hi = self[consts::VERTEXSHADERATTRIBUTESPERMUTATIONHIGH];
		let types_bits = (types_lo as u64) | ((types_hi as u64) << 32);

		let value = (types_bits >> (index * 4)) & 0xf;
		// Following unwrap should never fail: we have an enum for all possible values
		VertexAttributeValueType::from_u64(value).unwrap()
	}

	/// Gets the list of vertex attributes in a vertex.
	pub fn vertex_buffer_attributes(&self, buffer: u8) -> Vec<VertexAttribute> {
		self.buffer_indices(buffer)
			.map(|index| {
				let (storage_type, num_items) = self.buffer_attribute_storage(index);
				let value_type = self.buffer_attribute_value_type(index);
				VertexAttribute {
					storage_type,
					value_type,
					num_items,
				}
			})
			.collect()
	}

	/// Gets the number of bytes between each vertex.
	pub fn vertex_buffer_vertex_stride(&self, buffer: u8) -> usize {
		let value = self[consts::VERTEXSHADERATTRIBUTESBUFFER0STRIDE + (buffer as u16 * 3)];
		((value >> 16) & 0xff) as usize
	}

	/// Index buffer data type.
	pub fn index_buffer_format(&self) -> IndexAttributeType {
		IndexAttributeType::from_u32(self[consts::INDEXBUFFERCONFIG] >> 31).unwrap()
	}

	/// Total number of indices.
	pub fn index_buffer_num_vertices(&self) -> u32 {
		self[consts::INDEXBUFFERTOTALVERTICES]
	}

	/// Primitive type.
	pub fn index_buffer_primitive_type(&self) -> Result<PrimitiveType> {
		let mode = (self[consts::PRIMITIVECONFIG] >> 8) & 0x3;
		PrimitiveType::from_u32(mode)
			.ok_or_else(|| Error::parsing(format!("Unrecognized primitive mode: {:x}", mode)))
	}
}
impl ops::Index<u16> for PicaBuffer {
	type Output = u32;

	fn index(&self, index: u16) -> &u32 {
		&self.commands[index as usize]
	}
}

#[allow(unused)]
mod consts {
	pub const CULLING: u16 = 0x40;
	pub const POLYGONOFFSETENABLE: u16 = 0x4c;
	pub const POLYGONOFFSETZSCALE: u16 = 0x4d;
	pub const POLYGONOFFSETZBIAS: u16 = 0x4e;
	pub const TEXUNITSCONFIG: u16 = 0x80;
	pub const TEXUNIT0BORDERCOLOR: u16 = 0x81;
	pub const TEXUNIT0SIZE: u16 = 0x82;
	pub const TEXUNIT0PARAM: u16 = 0x83;
	pub const TEXUNIT0LEVELOFDETAIL: u16 = 0x84;
	pub const TEXUNIT0ADDRESS: u16 = 0x85;
	pub const TEXUNIT0TYPE: u16 = 0x8e;
	pub const TEXUNIT1BORDERCOLOR: u16 = 0x91;
	pub const TEXUNIT1SIZE: u16 = 0x92;
	pub const TEXUNIT1PARAM: u16 = 0x93;
	pub const TEXUNIT1LEVELOFDETAIL: u16 = 0x94;
	pub const TEXUNIT1ADDRESS: u16 = 0x95;
	pub const TEXUNIT1TYPE: u16 = 0x96;
	pub const TEXUNIT2BORDERCOLOR: u16 = 0x99;
	pub const TEXUNIT2SIZE: u16 = 0x9a;
	pub const TEXUNIT2PARAM: u16 = 0x9b;
	pub const TEXUNIT2LEVELOFDETAIL: u16 = 0x9c;
	pub const TEXUNIT2ADDRESS: u16 = 0x9d;
	pub const TEXUNIT2TYPE: u16 = 0x9e;
	pub const TEVSTAGE0SOURCE: u16 = 0xc0;
	pub const TEVSTAGE0OPERAND: u16 = 0xc1;
	pub const TEVSTAGE0COMBINE: u16 = 0xc2;
	pub const TEVSTAGE0CONSTANT: u16 = 0xc3;
	pub const TEVSTAGE0SCALE: u16 = 0xc4;
	pub const TEVSTAGE1SOURCE: u16 = 0xc8;
	pub const TEVSTAGE1OPERAND: u16 = 0xc9;
	pub const TEVSTAGE1COMBINE: u16 = 0xca;
	pub const TEVSTAGE1CONSTANT: u16 = 0xcb;
	pub const TEVSTAGE1SCALE: u16 = 0xcc;
	pub const TEVSTAGE2SOURCE: u16 = 0xd0;
	pub const TEVSTAGE2OPERAND: u16 = 0xd1;
	pub const TEVSTAGE2COMBINE: u16 = 0xd2;
	pub const TEVSTAGE2CONSTANT: u16 = 0xd3;
	pub const TEVSTAGE2SCALE: u16 = 0xd4;
	pub const TEVSTAGE3SOURCE: u16 = 0xd8;
	pub const TEVSTAGE3OPERAND: u16 = 0xd9;
	pub const TEVSTAGE3COMBINE: u16 = 0xda;
	pub const TEVSTAGE3CONSTANT: u16 = 0xdb;
	pub const TEVSTAGE3SCALE: u16 = 0xdc;
	pub const FRAGMENTBUFFERINPUT: u16 = 0xe0;
	pub const TEVSTAGE4SOURCE: u16 = 0xf0;
	pub const TEVSTAGE4OPERAND: u16 = 0xf1;
	pub const TEVSTAGE4COMBINE: u16 = 0xf2;
	pub const TEVSTAGE4CONSTANT: u16 = 0xf3;
	pub const TEVSTAGE4SCALE: u16 = 0xf4;
	pub const TEVSTAGE5SOURCE: u16 = 0xf8;
	pub const TEVSTAGE5OPERAND: u16 = 0xf9;
	pub const TEVSTAGE5COMBINE: u16 = 0xfa;
	pub const TEVSTAGE5CONSTANT: u16 = 0xfb;
	pub const TEVSTAGE5SCALE: u16 = 0xfc;
	pub const FRAGMENTBUFFERCOLOR: u16 = 0xfd;
	pub const COLOROUTPUTCONFIG: u16 = 0x100;
	pub const BLENDCONFIG: u16 = 0x101;
	pub const COLORLOGICOPERATIONCONFIG: u16 = 0x102;
	pub const BLENDCOLOR: u16 = 0x103;
	pub const ALPHATESTCONFIG: u16 = 0x104;
	pub const STENCILTESTCONFIG: u16 = 0x105;
	pub const STENCILOPERATIONCONFIG: u16 = 0x106;
	pub const DEPTHTESTCONFIG: u16 = 0x107;
	pub const CULLMODECONFIG: u16 = 0x108;
	pub const FRAMEBUFFERINVALIDATE: u16 = 0x110;
	pub const FRAMEBUFFERFLUSH: u16 = 0x111;
	pub const COLORBUFFERREAD: u16 = 0x112;
	pub const COLORBUFFERWRITE: u16 = 0x113;
	pub const DEPTHBUFFERREAD: u16 = 0x114;
	pub const DEPTHBUFFERWRITE: u16 = 0x115;
	pub const DEPTHTESTCONFIG2: u16 = 0x126;
	pub const FRAGMENTSHADERLOOKUPTABLECONFIG: u16 = 0x1c5;
	pub const FRAGMENTSHADERLOOKUPTABLEDATA: u16 = 0x1c8;
	pub const LUTSAMPLERABSOLUTE: u16 = 0x1d0;
	pub const LUTSAMPLERINPUT: u16 = 0x1d1;
	pub const LUTSAMPLERSCALE: u16 = 0x1d2;
	pub const VERTEXSHADERATTRIBUTESBUFFERADDRESS: u16 = 0x200;
	pub const VERTEXSHADERATTRIBUTESBUFFERFORMATLOW: u16 = 0x201;
	pub const VERTEXSHADERATTRIBUTESBUFFERFORMATHIGH: u16 = 0x202;
	pub const VERTEXSHADERATTRIBUTESBUFFER0ADDRESS: u16 = 0x203;
	pub const VERTEXSHADERATTRIBUTESBUFFER0PERMUTATION: u16 = 0x204;
	pub const VERTEXSHADERATTRIBUTESBUFFER0STRIDE: u16 = 0x205;
	pub const VERTEXSHADERATTRIBUTESBUFFER1ADDRESS: u16 = 0x206;
	pub const VERTEXSHADERATTRIBUTESBUFFER1PERMUTATION: u16 = 0x207;
	pub const VERTEXSHADERATTRIBUTESBUFFER1STRIDE: u16 = 0x208;
	pub const VERTEXSHADERATTRIBUTESBUFFER2ADDRESS: u16 = 0x209;
	pub const VERTEXSHADERATTRIBUTESBUFFER2PERMUTATION: u16 = 0x20a;
	pub const VERTEXSHADERATTRIBUTESBUFFER2STRIDE: u16 = 0x20b;
	pub const VERTEXSHADERATTRIBUTESBUFFER3ADDRESS: u16 = 0x20c;
	pub const VERTEXSHADERATTRIBUTESBUFFER3PERMUTATION: u16 = 0x20d;
	pub const VERTEXSHADERATTRIBUTESBUFFER3STRIDE: u16 = 0x20e;
	pub const VERTEXSHADERATTRIBUTESBUFFER4ADDRESS: u16 = 0x20f;
	pub const VERTEXSHADERATTRIBUTESBUFFER4PERMUTATION: u16 = 0x210;
	pub const VERTEXSHADERATTRIBUTESBUFFER4STRIDE: u16 = 0x211;
	pub const VERTEXSHADERATTRIBUTESBUFFER5ADDRESS: u16 = 0x212;
	pub const VERTEXSHADERATTRIBUTESBUFFER5PERMUTATION: u16 = 0x213;
	pub const VERTEXSHADERATTRIBUTESBUFFER5STRIDE: u16 = 0x214;
	pub const VERTEXSHADERATTRIBUTESBUFFER6ADDRESS: u16 = 0x215;
	pub const VERTEXSHADERATTRIBUTESBUFFER6PERMUTATION: u16 = 0x216;
	pub const VERTEXSHADERATTRIBUTESBUFFER6STRIDE: u16 = 0x217;
	pub const VERTEXSHADERATTRIBUTESBUFFER7ADDRESS: u16 = 0x218;
	pub const VERTEXSHADERATTRIBUTESBUFFER7PERMUTATION: u16 = 0x219;
	pub const VERTEXSHADERATTRIBUTESBUFFER7STRIDE: u16 = 0x21a;
	pub const VERTEXSHADERATTRIBUTESBUFFER8ADDRESS: u16 = 0x21b;
	pub const VERTEXSHADERATTRIBUTESBUFFER8PERMUTATION: u16 = 0x21c;
	pub const VERTEXSHADERATTRIBUTESBUFFER8STRIDE: u16 = 0x21d;
	pub const VERTEXSHADERATTRIBUTESBUFFER9ADDRESS: u16 = 0x21e;
	pub const VERTEXSHADERATTRIBUTESBUFFER9PERMUTATION: u16 = 0x21f;
	pub const VERTEXSHADERATTRIBUTESBUFFER9STRIDE: u16 = 0x220;
	pub const VERTEXSHADERATTRIBUTESBUFFER10ADDRESS: u16 = 0x221;
	pub const VERTEXSHADERATTRIBUTESBUFFER10PERMUTATION: u16 = 0x222;
	pub const VERTEXSHADERATTRIBUTESBUFFER10STRIDE: u16 = 0x223;
	pub const VERTEXSHADERATTRIBUTESBUFFER11ADDRESS: u16 = 0x224;
	pub const VERTEXSHADERATTRIBUTESBUFFER11PERMUTATION: u16 = 0x225;
	pub const VERTEXSHADERATTRIBUTESBUFFER11STRIDE: u16 = 0x226;
	pub const INDEXBUFFERCONFIG: u16 = 0x227;
	pub const INDEXBUFFERTOTALVERTICES: u16 = 0x228;
	pub const BLOCKEND: u16 = 0x23d;
	pub const VERTEXSHADERTOTALATTRIBUTES: u16 = 0x242;
	pub const PRIMITIVECONFIG: u16 = 0x25e;
	pub const VERTEXSHADERBOOLEANUNIFORMS: u16 = 0x2b0;
	pub const VERTEXSHADERINTEGERUNIFORMS0: u16 = 0x2b1;
	pub const VERTEXSHADERINTEGERUNIFORMS1: u16 = 0x2b2;
	pub const VERTEXSHADERINTEGERUNIFORMS2: u16 = 0x2b3;
	pub const VERTEXSHADERINTEGERUNIFORMS3: u16 = 0x2b4;
	pub const VERTEXSHADERINPUTBUFFERCONFIG: u16 = 0x2b9;
	pub const VERTEXSHADERENTRYPOINT: u16 = 0x2ba;
	pub const VERTEXSHADERATTRIBUTESPERMUTATIONLOW: u16 = 0x2bb;
	pub const VERTEXSHADERATTRIBUTESPERMUTATIONHIGH: u16 = 0x2bc;
	pub const VERTEXSHADEROUTMAPMASK: u16 = 0x2bd;
	pub const VERTEXSHADERCODETRANSFEREND: u16 = 0x2bf;
	pub const VERTEXSHADERFLOATUNIFORMCONFIG: u16 = 0x2c0;
	pub const VERTEXSHADERFLOATUNIFORMDATA: u16 = 0x2c1;
}

/// Vertex attribute definition
#[derive(Debug, Clone)]
pub struct VertexAttribute {
	/// Semantic meaning of the value (ex. position, normal)
	pub value_type: VertexAttributeValueType,
	/// Data layout of the value (ex. u8, f32)
	pub storage_type: VertexAttributeStorageType,
	/// Number of items. 3 for positions/normals, 4 for color, etc.
	pub num_items: u32,
}
impl VertexAttribute {
	/// Reads an attribute from a reader.
	///
	/// Returns a `Vector4`, but elements past `num_items` will be zero.
	pub fn read<Reader: Read>(&self, reader: &mut Reader) -> Result<Vector4<f32>> {
		let mut value = Vector4::<f32>::new(0., 0., 0., 0.);

		for axis in 0..self.num_items {
			let component = self.storage_type.read(reader)?;
			value[axis as usize] = component;
		}
		Ok(value)
	}

	/// Gets the size, in bytes, of the attribute.
	pub fn size(&self) -> usize {
		self.num_items as usize * self.storage_type.size()
	}
}

/// Semantic meaning of an attribute
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, FromPrimitive, ToPrimitive)]
pub enum VertexAttributeValueType {
	Position = 0,
	Normal = 1,
	Tangent = 2,
	Color = 3,
	TextureCoordinate0 = 4,
	TextureCoordinate1 = 5,
	TextureCoordinate2 = 6,
	BoneIndex = 7,
	BoneWeight = 8,
	UserAttribute0 = 9,
	UserAttribute1 = 0xa,
	UserAttribute2 = 0xb,
	UserAttribute3 = 0xc,
	UserAttribute4 = 0xd,
	UserAttribute5 = 0xe,
	UserAttribute6 = 0xf,
	UserAttribute7 = 0x10,
	UserAttribute8 = 0x11,
	UserAttribute9 = 0x12,
	UserAttribute10 = 0x13,
	UserAttribute11 = 0x14,
	Interleave = 0x15,
	Quantity = 0x16,
}

/// Storage format for a vertex attribute
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, FromPrimitive, ToPrimitive)]
pub enum VertexAttributeStorageType {
	SignedByte = 0,
	UnsignedByte = 1,
	SignedShort = 2,
	Float = 3,
}
impl VertexAttributeStorageType {
	/// Reads a value of the specified data type from a reader.
	///
	/// All types are casted to `f32` as-is; ex. an `UnsignedByte` of 42 results
	/// in the floating-point number 42.0.
	pub fn read<Reader: Read>(&self, reader: &mut Reader) -> Result<f32> {
		match self {
			VertexAttributeStorageType::SignedByte => Ok(reader.read_i8()? as f32),
			VertexAttributeStorageType::UnsignedByte => Ok(reader.read_u8()? as f32),
			VertexAttributeStorageType::SignedShort => {
				Ok(reader.read_i16::<LittleEndian>()? as f32)
			}
			VertexAttributeStorageType::Float => Ok(reader.read_f32::<LittleEndian>()?),
		}
	}

	/// Gets the size, in bytes, of the attribute.
	pub fn size(&self) -> usize {
		match self {
			VertexAttributeStorageType::SignedByte => 1,
			VertexAttributeStorageType::UnsignedByte => 1,
			VertexAttributeStorageType::SignedShort => 2,
			VertexAttributeStorageType::Float => 4,
		}
	}
}

/// Storage format of index buffer elements
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, FromPrimitive, ToPrimitive)]
pub enum IndexAttributeType {
	UnsignedByte = 0,
	UnsignedShort = 1,
}
impl IndexAttributeType {
	/// Reads a value of the specified data type from a reader.
	///
	/// All types are casted to `usize`.
	pub fn read<Reader: Read>(&self, reader: &mut Reader) -> Result<usize> {
		match self {
			IndexAttributeType::UnsignedByte => Ok(reader.read_u8()? as usize),
			IndexAttributeType::UnsignedShort => Ok(reader.read_u16::<LittleEndian>()? as usize),
		}
	}

	/// Gets the size, in bytes, of one index.
	pub fn size(&self) -> usize {
		match self {
			IndexAttributeType::UnsignedByte => 1,
			IndexAttributeType::UnsignedShort => 2,
		}
	}
}

/// Primitive types
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, FromPrimitive, ToPrimitive)]
pub enum PrimitiveType {
	Triangles = 0,
	TriangleStrip = 1,
	TriangleFan = 2,
	//Geometry = 3, // We don't support this
}
impl PrimitiveType {
	/// Checks if there are a proper amount of elements in an index array for the
	/// primitive type.
	///
	/// For Triangles mode, the length should be a multiple of 3. For the TriangleStrip
	/// and TriangleFan modes, the length should be either 3 or larger, or zero.
	pub fn check_num_indices(&self, number: usize) -> Result<()> {
		match self {
			PrimitiveType::Triangles => {
				if number % 3 != 0 {
					return Err(Error::parsing(format!(
						"{:?} mesh has {} indices, which is not a multiple of 3",
						self, number
					)));
				}
			}
			PrimitiveType::TriangleStrip | PrimitiveType::TriangleFan => {
				if number != 0 && number < 3 {
					return Err(Error::parsing(format!(
						"{:?} mesh has {} indices, which is less than the minimum of 3",
						self, number
					)));
				}
			}
		}
		Ok(())
	}

	/// Iterates over triangles in an index buffer, flattening the primitive strip
	/// into triangles.
	pub fn iter_triangles<Iter: Iterator>(&self, iter: Iter) -> PrimitiveTypeIter<Iter>
	where
		Iter::Item: Clone,
	{
		PrimitiveTypeIter::new(*self, iter)
	}
}

/// Type for iterating over an index buffer. See `PrimitiveType::iter_triangles`.
pub struct PrimitiveTypeIter<Iter: Iterator>
where
	Iter::Item: Clone,
{
	typ: PrimitiveType,
	underlying: iter::Fuse<Iter>,
	// Triangles: Unused
	// TriangleStrip: Previous two elements
	// TriangleFan: First element, previous element
	buffer: Option<[Iter::Item; 2]>,
}
impl<Iter: Iterator> PrimitiveTypeIter<Iter>
where
	Iter::Item: Clone,
{
	fn new(typ: PrimitiveType, underlying: Iter) -> Self {
		// Need fuse to ensure that if initializing the buffer returns None,
		// we don't spontaneously get Some during the first next call.
		let mut underlying = underlying.fuse();

		let buffer = match typ {
			PrimitiveType::TriangleStrip | PrimitiveType::TriangleFan => {
				let first = underlying.next();
				let second = underlying.next();
				match (first, second) {
					(Some(first), Some(second)) => Some([first, second]),
					_ => None, // Zero or one elements, don't bother with buffer
				}
			}
			_ => None,
		};

		Self {
			typ,
			underlying,
			buffer,
		}
	}
}
impl<Iter: Iterator> Iterator for PrimitiveTypeIter<Iter>
where
	Iter::Item: Clone,
{
	type Item = [Iter::Item; 3];

	fn next(&mut self) -> Option<Self::Item> {
		match self.typ {
			PrimitiveType::Triangles => {
				let item0 = self.underlying.next();
				let item1 = self.underlying.next();
				let item2 = self.underlying.next();
				match (item0, item1, item2) {
					(Some(item0), Some(item1), Some(item2)) => Some([item0, item1, item2]),
					_ => None,
				}
			}
			PrimitiveType::TriangleStrip => {
				let next = if let Some(next) = self.underlying.next() {
					next
				} else {
					return None;
				};

				let buffer = self.buffer.as_mut().unwrap();
				let this_triple = [buffer[0].clone(), buffer[1].clone(), next.clone()];
				buffer[0] = buffer[1].clone();
				buffer[1] = next;
				Some(this_triple)
			}
			PrimitiveType::TriangleFan => {
				let next = if let Some(next) = self.underlying.next() {
					next
				} else {
					return None;
				};

				let buffer = self.buffer.as_mut().unwrap();
				let this_triple = [buffer[0].clone(), buffer[1].clone(), next.clone()];
				buffer[1] = next;
				Some(this_triple)
			}
		}
	}
}
