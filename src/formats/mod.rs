//! Data formats and structures

pub mod animations;
pub mod containers;
pub mod export;
pub mod misc;
pub mod models;
pub mod textures;
pub mod visitor;

use std::{
	cell::RefCell,
	fmt,
	io,
	rc::Rc,
};

use crate::error::{
	self,
	Result,
};

/// Tries to identify a file.
///
/// Rewinds the file to the start point, unless `Err` is returned.
///
/// Returns `Some` format if the format is recognized, or `None` if not.
pub fn identify<Reader: io::Read + io::Seek + ?Sized>(
	reader: &mut Reader,
) -> io::Result<Option<Format>> {
	let mut header_bytes = [0, 0, 0, 0];
	reader.seek(io::SeekFrom::Start(0))?;
	let num_read = reader.read(&mut header_bytes)?;
	reader.seek(io::SeekFrom::Start(0))?;

	if num_read != header_bytes.len() {
		// Too small
		return Ok(None);
	}

	if &header_bytes[0..4] == &containers::arc::MAGIC {
		return Ok(Some(Format::Container(ContainerFormat::ARC)));
	}
	if &header_bytes[0..4] == &containers::sarc::MAGIC {
		return Ok(Some(Format::Container(ContainerFormat::SARC)));
	}
	if &header_bytes[0..4] == &[0x00, 0x00, 0x01, 0x00] {
		return Ok(Some(Format::Container(ContainerFormat::GfModelContainer)));
	}
	if &header_bytes[0..4] == &[0x00, 0x00, 0x06, 0x00] {
		return Ok(Some(Format::Animation(AnimationFormat::GfMotion)));
	}
	if &header_bytes[0..4] == &[0x17, 0x21, 0x12, 0x15] {
		return Ok(Some(Format::Model(ModelFormat::GfModel)));
	}
	if &header_bytes[0..4] == &[0x13, 0x12, 0x04, 0x15] {
		// Some files use the GfTexture magic number but instead store shader data. Check that.
		reader.seek(io::SeekFrom::Start(0x8))?;
		let mut magic = [0, 0, 0, 0, 0, 0, 0];
		let num_read = reader.read(&mut magic)?;
		reader.seek(io::SeekFrom::Start(0))?;

		if num_read != magic.len() {
			return Ok(None);
		}
		if magic == *b"texture" {
			return Ok(Some(Format::Texture(TextureFormat::GfTexture)));
		} else {
			return Ok(None);
		}
	}
	if &header_bytes[0..4] == b"CRAG" {
		return Ok(Some(Format::Container(ContainerFormat::GARC)));
	}

	if header_bytes[0].is_ascii_uppercase() && header_bytes[1].is_ascii_uppercase() {
		return Ok(Some(Format::Container(ContainerFormat::Pokemon)));
	}

	return Ok(None);
}

/// Tries to identify a container entry.
///
/// Doesn't require reading the entire entry. See [`identify`](#method.identify)
/// for more info.
pub fn identify_entry<C: containers::Container + ?Sized>(
	container: &C,
	entry_index: usize,
) -> Result<Option<Format>> {
	let data = container.read_entry(entry_index, 0, Some(20))?; // Adjust this number as needed
	let mut cursor = io::Cursor::new(data);
	Ok(identify(&mut cursor)?)
}

/// File types
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Format {
	/// A container format that holds multiple subfiles
	Container(ContainerFormat),
	/// A model
	Model(ModelFormat),
	/// A texture or image
	Texture(TextureFormat),
	/// An animation
	Animation(AnimationFormat),
}
impl fmt::Display for Format {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			Format::Container(info) => write!(f, "Container: {}", info),
			Format::Model(info) => write!(f, "Model: {}", info),
			Format::Texture(info) => write!(f, "Texture: {}", info),
			Format::Animation(info) => write!(f, "Animation: {}", info),
		}
	}
}

/// Container file formats
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ContainerFormat {
	ARC,
	GARC,
	SARC,
	GfModelContainer,
	Pokemon,
}
impl ContainerFormat {
	pub fn read<'a, Reader: io::Read + io::Seek + 'a>(
		&self,
		reader: Rc<RefCell<Reader>>,
	) -> Result<Box<dyn containers::Container + 'a>> {
		let _loc = error::Location::new(format!("{:?} file", self));
		match self {
			ContainerFormat::ARC => containers::arc::ArcContainer::new(reader)
				.map(|v| Box::new(v) as Box<dyn containers::Container>),
			ContainerFormat::GARC => containers::garc::GarcContainer::new(reader)
				.map(|v| Box::new(v) as Box<dyn containers::Container>),
			ContainerFormat::SARC => containers::sarc::SarcContainer::new(reader)
				.map(|v| Box::new(v) as Box<dyn containers::Container>),
			ContainerFormat::GfModelContainer => {
				containers::gfmodel_container::GfModelContainer::new(reader)
					.map(|v| Box::new(v) as Box<dyn containers::Container>)
			}
			ContainerFormat::Pokemon => {
				containers::pokemon_container::PokemonContainer::new(reader)
					.map(|v| Box::new(v) as Box<dyn containers::Container>)
			}
		}
	}
}
impl fmt::Display for ContainerFormat {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			ContainerFormat::ARC => f.write_str("ARC"),
			ContainerFormat::GARC => f.write_str("GARC"),
			ContainerFormat::SARC => f.write_str("SARC"),
			ContainerFormat::GfModelContainer => f.write_str("GfModel Container"),
			ContainerFormat::Pokemon => write!(f, "Pokemon Container"),
		}
	}
}

/// Model file formats
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ModelFormat {
	GfModel,
}
impl ModelFormat {
	pub fn read<Reader: io::Read + io::Seek>(
		&self,
		reader: &mut Reader,
	) -> Result<models::geom::Model> {
		match self {
			ModelFormat::GfModel => {
				models::gfmodel::read_gfmodel(reader, &mut error::LogWarningSink)
			}
		}
	}
}
impl fmt::Display for ModelFormat {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			ModelFormat::GfModel => f.write_str("GfModel"),
		}
	}
}

/// Texture file formats
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TextureFormat {
	GfTexture,
}
impl TextureFormat {
	pub fn read<Reader: io::Read + io::Seek>(
		&self,
		reader: &mut Reader,
	) -> Result<textures::Texture> {
		let _loc = error::Location::new(format!("{:?} file", self));
		match self {
			TextureFormat::GfTexture => textures::gftexture::read_gftexture(reader),
		}
	}
}
impl fmt::Display for TextureFormat {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			TextureFormat::GfTexture => f.write_str("GfTexture"),
		}
	}
}

/// Animation file formats
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AnimationFormat {
	GfMotion,
}
impl AnimationFormat {
	pub fn read<Reader: io::Read + io::Seek>(
		&self,
		reader: &mut Reader,
	) -> Result<animations::geom::Animation> {
		let _loc = error::Location::new(format!("{:?} file", self));
		match self {
			AnimationFormat::GfMotion => {
				animations::gfmotion::read_gfmotion_anim(reader, "Animation".to_string())
			}
		}
	}
}
impl fmt::Display for AnimationFormat {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			AnimationFormat::GfMotion => f.write_str("GfMotion"),
		}
	}
}
