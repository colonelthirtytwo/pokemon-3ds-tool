//! Exporter for the COLLADA format.
//!
//! Still a WIP; could theoretically store more information than SMD, but it's
//! also more complicated to write.

use nalgebra::{
	allocator::Allocator,
	base::VectorN,
	dimension::Dim,
	DefaultAllocator,
};
use std::{
	collections::BTreeMap,
	fmt::Write,
	io,
};
use xml::writer::{
	self,
	EmitterConfig,
	EventWriter,
	XmlEvent,
};

use crate::{
	formats::models::geom::*,
	nonnan::NonNaN,
};

/// Export options.
#[derive(Debug, Clone)]
pub struct ColladaExportParameters {
	/// Model up-axis.
	///
	/// This doesn't transform the vertices, it just sets the up axis metadata.
	pub up_axis: UpAxis,
}
impl Default for ColladaExportParameters {
	fn default() -> Self {
		Self { up_axis: UpAxis::Y }
	}
}

/// Which axis represents up
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum UpAxis {
	X,
	Y,
	Z,
}
impl UpAxis {
	fn as_collada(&self) -> &'static str {
		match self {
			UpAxis::X => "X_UP",
			UpAxis::Y => "Y_UP",
			UpAxis::Z => "Z_UP",
		}
	}
}

/// Writes a model to a file in the COLLADA format.
pub fn write_collada<Writer: io::Write>(
	writer: &mut Writer,
	model: &Model,
	params: &ColladaExportParameters,
) -> writer::Result<()> {
	let mut writer = EmitterConfig::new()
		.perform_indent(true)
		.create_writer(writer);
	writer.write(
		XmlEvent::start_element("COLLADA")
			.attr("version", "1.4.1")
			.ns("", "http://www.collada.org/2005/11/COLLADASchema"),
	)?;

	writer.write(XmlEvent::start_element("asset"))?;
	writer.write(XmlEvent::start_element("up_axis"))?;
	writer.write(XmlEvent::characters(params.up_axis.as_collada()))?;
	writer.write(XmlEvent::end_element().name("up_axis"))?;
	writer.write(XmlEvent::end_element().name("asset"))?;

	let model_name = "model";

	write_materials(&mut writer, model.materials(), model_name)?;

	writer.write(XmlEvent::start_element("library_geometries"))?;
	write_geometry(&mut writer, model, model_name)?;
	writer.write(XmlEvent::end_element().name("library_geometries"))?;

	// Write bones
	if let Some(bone_weights) = model.mesh().bone_weights() {
		write_bone_weights(&mut writer, bone_weights, model, model_name)?;
	}

	// Scene
	writer.write(XmlEvent::start_element("library_visual_scenes"))?;
	writer.write(
		XmlEvent::start_element("visual_scene")
			.attr("id", "scene")
			.attr("name", "Scene"),
	)?;

	if model.bones().is_empty() {
		writer.write(
			XmlEvent::start_element("node")
				.attr("id", &format!("{}-model-node", model_name))
				.attr("name", model_name),
		)?;
		writer.write(
			XmlEvent::start_element("instance_geometry")
				.attr("url", &format!("#{}-geometry", model_name)),
		)?;
		writer.write(XmlEvent::end_element().name("instance_geometry"))?;
		writer.write(XmlEvent::end_element().name("node"))?;
	} else {
		writer.write(
			XmlEvent::start_element("node")
				.attr("id", &format!("{}-skeleton", model_name))
				.attr("name", &format!("{} Skeleton", model_name))
				.attr("type", "NODE"),
		)?;
		for root_bone in model.bones().children_of(None) {
			write_bone_node(&mut writer, model, model_name, root_bone)?;
		}
		writer.write(XmlEvent::end_element().name("node"))?;

		writer.write(
			XmlEvent::start_element("node")
				.attr("id", &format!("{}-model", model_name))
				.attr("name", model_name)
				.attr("type", "NODE"),
		)?;
		writer.write(
			XmlEvent::start_element("instance_controller")
				.attr("url", &format!("#{}-controller", model_name)),
		)?;
		let first_root = model.bones().children_of(None).next().unwrap();
		writer.write(XmlEvent::start_element("skeleton"))?;
		writer.write(XmlEvent::characters(&format!(
			"#{}-bone-{}",
			model_name, first_root.name
		)))?;
		writer.write(XmlEvent::end_element().name("skeleton"))?;
		writer.write(XmlEvent::end_element().name("instance_controller"))?;
		writer.write(XmlEvent::end_element().name("node"))?;
	}

	writer.write(XmlEvent::end_element().name("visual_scene"))?;
	writer.write(XmlEvent::end_element().name("library_visual_scenes"))?;

	writer.write(XmlEvent::start_element("scene"))?;
	writer.write(XmlEvent::start_element("instance_visual_scene").attr("url", "#scene"))?;
	writer.write(XmlEvent::end_element().name("instance_visual_scene"))?;
	writer.write(XmlEvent::end_element().name("scene"))?;

	// End file
	writer.write(XmlEvent::end_element().name("COLLADA"))?;
	Ok(())
}

fn write_materials<Writer: io::Write>(
	writer: &mut EventWriter<Writer>,
	materials: &[Material],
	model_name: &str,
) -> writer::Result<()> {
	writer.write(XmlEvent::start_element("library_effects"))?;

	for material in materials.iter() {
		writer.write(
			XmlEvent::start_element("effect")
				.attr("id", &format!("{}-{}-fx", model_name, material.name)),
		)?;
		writer.write(XmlEvent::start_element("profile_COMMON"))?;
		writer.write(XmlEvent::start_element("technique").attr("sid", "common"))?;
		writer.write(XmlEvent::start_element("phong"))?;

		// TODO: material data

		writer.write(XmlEvent::end_element().name("phong"))?;
		writer.write(XmlEvent::end_element().name("technique"))?;
		writer.write(XmlEvent::end_element().name("profile_COMMON"))?;
		writer.write(XmlEvent::end_element().name("effect"))?;
	}

	writer.write(XmlEvent::end_element().name("library_effects"))?;

	writer.write(XmlEvent::start_element("library_materials"))?;
	for material in materials.iter() {
		writer.write(
			XmlEvent::start_element("material")
				.attr("id", &format!("{}-{}-material", model_name, material.name))
				.attr("name", &material.name),
		)?;
		writer.write(
			XmlEvent::start_element("instance_effect")
				.attr("url", &format!("#{}-{}-fx", model_name, material.name)),
		)?;
		writer.write(XmlEvent::end_element().name("instance_effect"))?;
		writer.write(XmlEvent::end_element().name("material"))?;
	}
	writer.write(XmlEvent::end_element().name("library_materials"))?;
	Ok(())
}

fn write_geometry<Writer: io::Write>(
	writer: &mut EventWriter<Writer>,
	model: &Model,
	model_name: &str,
) -> writer::Result<()> {
	writer.write(
		XmlEvent::start_element("geometry")
			.attr("id", &format!("{}-geometry", model_name))
			.attr("name", model_name),
	)?;
	writer.write(XmlEvent::start_element("mesh"))?;

	for (attr_index, attr) in model.mesh().attributes().iter().enumerate() {
		match attr {
			VertexAttribute::Positions(ref v) => write_geometry_source(
				writer,
				model_name,
				&v,
				attr_index,
				"positions",
				&["X", "Y", "Z"],
			),
			VertexAttribute::Normals(ref v) => write_geometry_source(
				writer,
				model_name,
				&v,
				attr_index,
				"normals",
				&["X", "Y", "Z"],
			),
			VertexAttribute::Tangents(ref v) => write_geometry_source(
				writer,
				model_name,
				&v,
				attr_index,
				"tangents",
				&["X", "Y", "Z"],
			),
			VertexAttribute::Colors(ref v) => write_geometry_source(
				writer,
				model_name,
				&v,
				attr_index,
				"colors",
				&["R", "G", "B", "A"],
			),
			VertexAttribute::TextureCoords(ref v) => {
				write_geometry_source(writer, model_name, &v, attr_index, "texcoords", &["U", "V"])
			}
			_ => Ok(()),
		}?;
	}

	writer.write(
		XmlEvent::start_element("vertices").attr("id", &format!("#{}-vertices", model_name)),
	)?;

	for (attr_index, attr) in model.mesh().attributes().iter().enumerate() {
		match attr {
			VertexAttribute::Positions(_) => writer.write(
				XmlEvent::start_element("input")
					.attr("semantic", "POSITION")
					.attr(
						"source",
						&format!("#{}-attr{}-positions", model_name, attr_index),
					),
			),
			VertexAttribute::Normals(_) => writer.write(
				XmlEvent::start_element("input")
					.attr("semantic", "NORMAL")
					.attr(
						"source",
						&format!("#{}-attr{}-normals", model_name, attr_index),
					),
			),
			VertexAttribute::Tangents(_) => writer.write(
				XmlEvent::start_element("input")
					.attr("semantic", "TANGENT")
					.attr(
						"source",
						&format!("#{}-attr{}-tangents", model_name, attr_index),
					),
			),
			VertexAttribute::Colors(_) => writer.write(
				XmlEvent::start_element("input")
					.attr("semantic", "COLOR")
					.attr(
						"source",
						&format!("#{}-attr{}-colors", model_name, attr_index),
					),
			),
			VertexAttribute::TextureCoords(_) => writer.write(
				XmlEvent::start_element("input")
					.attr("semantic", "TEXCOORD")
					.attr(
						"source",
						&format!("#{}-attr{}-texcoords", model_name, attr_index),
					),
			),
			_ => {
				continue;
			}
		}?;
		writer.write(XmlEvent::end_element())?;
	}

	writer.write(XmlEvent::end_element().name("vertices"))?;

	for (material_i, material) in model.materials().iter().enumerate() {
		let count = model
			.mesh()
			.faces()
			.iter()
			.filter(|face| face.material == material_i)
			.count();
		writer.write(
			XmlEvent::start_element("triangles")
				.attr("count", &count.to_string())
				.attr(
					"material",
					&format!("{}-{}-material", model_name, material.name),
				),
		)?;

		writer.write(
			XmlEvent::start_element("input")
				.attr("offset", "0")
				.attr("semantic", "VERTEX")
				.attr("source", &format!("#{}-vertices", model_name)),
		)?;
		writer.write(XmlEvent::end_element().name("input"))?;

		writer.write(XmlEvent::start_element("p"))?;
		let mut buf = String::new();
		for face in model
			.mesh()
			.faces()
			.iter()
			.filter(|face| face.material == material_i)
		{
			buf.clear();
			write!(
				&mut buf,
				"{} {} {} ",
				face.indices[0], face.indices[1], face.indices[2],
			)
			.unwrap();
			writer.write(XmlEvent::characters(&buf))?;
		}
		writer.write(XmlEvent::end_element().name("p"))?;

		writer.write(XmlEvent::end_element().name("triangles"))?;
	}

	// End meshes
	writer.write(XmlEvent::end_element().name("mesh"))?;
	writer.write(XmlEvent::end_element().name("geometry"))?;
	Ok(())
}

fn write_geometry_source<Writer: io::Write, D: Dim>(
	writer: &mut EventWriter<Writer>,
	model_name: &str,
	attr_array: &[VectorN<f32, D>],
	attr_index: usize,
	attr_name: &str,
	value_names: &[&str],
) -> writer::Result<()>
where
	DefaultAllocator: Allocator<f32, D>,
{
	let values_per_attr = D::try_to_usize().unwrap();
	assert_eq!(values_per_attr, value_names.len());

	writer.write(
		XmlEvent::start_element("source")
			.attr(
				"id",
				&format!("{}-attr{}-{}", model_name, attr_index, attr_name),
			)
			.attr("name", attr_name),
	)?;

	writer.write(
		XmlEvent::start_element("float_array")
			.attr(
				"id",
				&format!("{}-attr{}-{}-array", model_name, attr_index, attr_name),
			)
			.attr("count", &(attr_array.len() * values_per_attr).to_string()),
	)?;

	let mut buf = String::new();
	for v in attr_array.iter() {
		buf.clear();
		for val in v.iter() {
			write!(&mut buf, "{} ", val).unwrap();
		}

		writer.write(XmlEvent::characters(&buf))?;
	}

	writer.write(XmlEvent::end_element().name("float_array"))?;
	writer.write(XmlEvent::start_element("technique_common"))?;

	writer.write(
		XmlEvent::start_element("accessor")
			.attr(
				"source",
				&format!("{}-attr{}-{}-array", model_name, attr_index, attr_name),
			)
			.attr("count", &attr_array.len().to_string())
			.attr("offset", "0")
			.attr("stride", &values_per_attr.to_string()),
	)?;
	for axis in value_names {
		writer.write(
			XmlEvent::start_element("param")
				.attr("name", axis)
				.attr("type", "float"),
		)?;
		writer.write(XmlEvent::end_element().name("param"))?;
	}
	writer.write(XmlEvent::end_element().name("accessor"))?;

	writer.write(XmlEvent::end_element().name("technique_common"))?;
	writer.write(XmlEvent::end_element().name("source"))?;
	Ok(())
}

fn write_bone_weights<Writer: io::Write>(
	writer: &mut EventWriter<Writer>,
	bone_weights: &[BTreeMap<usize, NonNaN>],
	model: &Model,
	model_name: &str,
) -> writer::Result<()> {
	let mut buf = String::new();
	let bones = model.bones();

	writer.write(XmlEvent::start_element("library_controllers"))?;
	writer.write(
		XmlEvent::start_element("controller")
			.attr("id", &format!("{}-controller", model_name))
			.attr("name", &format!("{} Skeleton", model_name)),
	)?;
	writer.write(
		XmlEvent::start_element("skin").attr("source", &format!("#{}-geometry", model_name)),
	)?;

	writer.write(XmlEvent::start_element("bind_shape_matrix"))?;
	writer.write(XmlEvent::characters("1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1"))?;
	writer.write(XmlEvent::end_element().name("bind_shape_matrix"))?;

	// Joint names
	writer.write(
		XmlEvent::start_element("source").attr("id", &format!("{}-joint-names", model_name)),
	)?;
	writer.write(
		XmlEvent::start_element("Name_array")
			.attr("id", &format!("{}-joint-names-array", model_name))
			.attr("count", &bones.len().to_string()),
	)?;
	for (i, bone) in bones.iter().enumerate() {
		if i != 0 {
			writer.write(XmlEvent::characters(" "))?;
		}
		writer.write(XmlEvent::characters(&bone.name))?;
	}
	writer.write(XmlEvent::end_element().name("Name_array"))?;
	writer.write(XmlEvent::start_element("technique_common"))?;
	writer.write(
		XmlEvent::start_element("accessor")
			.attr("source", &format!("#{}-joint-names", model_name))
			.attr("count", &bones.len().to_string())
			.attr("stride", "1"),
	)?;
	writer.write(
		XmlEvent::start_element("param")
			.attr("name", "JOINT")
			.attr("type", "name"),
	)?;
	writer.write(XmlEvent::end_element().name("param"))?;
	writer.write(XmlEvent::end_element().name("accessor"))?;
	writer.write(XmlEvent::end_element().name("technique_common"))?;
	writer.write(XmlEvent::end_element().name("source"))?;

	// Bind poses
	writer.write(
		XmlEvent::start_element("source").attr("id", &format!("{}-bind-poses", model_name)),
	)?;
	writer.write(
		XmlEvent::start_element("float_array")
			.attr("id", &format!("{}-bind-poses-array", model_name))
			.attr("count", &(bones.len() * 4 * 4).to_string()),
	)?;
	for bone in bones.iter() {
		buf.clear();
		let mut matrix = bones.inv_global_xform_matrix(bone);
		matrix.transpose_mut(); // Column to row major
		for v in matrix.iter() {
			write!(&mut buf, "{} ", v).unwrap();
		}
		writer.write(XmlEvent::characters(&buf))?;
	}
	writer.write(XmlEvent::end_element().name("float_array"))?;
	writer.write(XmlEvent::start_element("technique_common"))?;
	writer.write(
		XmlEvent::start_element("accessor")
			.attr("source", &format!("#{}-bind-poses", model_name))
			.attr("count", &bones.len().to_string())
			.attr("stride", "16"),
	)?;
	writer.write(
		XmlEvent::start_element("param")
			.attr("name", "TRANSFORM")
			.attr("type", "float4x4"),
	)?;
	writer.write(XmlEvent::end_element().name("param"))?;
	writer.write(XmlEvent::end_element().name("accessor"))?;
	writer.write(XmlEvent::end_element().name("technique_common"))?;
	writer.write(XmlEvent::end_element().name("source"))?;

	// Weights
	writer
		.write(XmlEvent::start_element("source").attr("id", &format!("{}-weights", model_name)))?;
	let num_weights = bone_weights
		.iter()
		.map(|b| b.len())
		.sum::<usize>()
		.to_string();
	writer.write(
		XmlEvent::start_element("float_array")
			.attr("id", &format!("{}-weights-array", model_name))
			.attr("count", &num_weights),
	)?;
	for (_, weight) in bone_weights.iter().flat_map(|v| v.iter()) {
		buf.clear();
		write!(&mut buf, "{} ", weight).unwrap();
		writer.write(XmlEvent::characters(&buf))?;
	}
	writer.write(XmlEvent::end_element().name("float_array"))?;
	writer.write(XmlEvent::start_element("technique_common"))?;
	writer.write(
		XmlEvent::start_element("accessor")
			.attr("source", &format!("#{}-weights-array", model_name))
			.attr("count", &num_weights)
			.attr("stride", "1"),
	)?;
	writer.write(
		XmlEvent::start_element("param")
			.attr("name", "WEIGHT")
			.attr("type", "float"),
	)?;
	writer.write(XmlEvent::end_element().name("param"))?;
	writer.write(XmlEvent::end_element().name("accessor"))?;
	writer.write(XmlEvent::end_element().name("technique_common"))?;
	writer.write(XmlEvent::end_element().name("source"))?;

	// <joints>
	writer.write(XmlEvent::start_element("joints"))?;
	writer.write(
		XmlEvent::start_element("input")
			.attr("semantic", "JOINT")
			.attr("source", &format!("#{}-joint-names", model_name)),
	)?;
	writer.write(XmlEvent::end_element().name("input"))?;
	writer.write(
		XmlEvent::start_element("input")
			.attr("semantic", "INV_BIND_MATRIX")
			.attr("source", &format!("#{}-bind-poses", model_name)),
	)?;
	writer.write(XmlEvent::end_element().name("input"))?;
	writer.write(XmlEvent::end_element().name("joints"))?;

	// <vertex_weights>
	writer.write(
		XmlEvent::start_element("vertex_weights").attr("count", &bone_weights.len().to_string()),
	)?;
	writer.write(
		XmlEvent::start_element("input")
			.attr("semantic", "JOINT")
			.attr("source", &format!("#{}-joint-names", model_name))
			.attr("offset", "0"),
	)?;
	writer.write(XmlEvent::end_element().name("input"))?;
	writer.write(
		XmlEvent::start_element("input")
			.attr("semantic", "WEIGHT")
			.attr("source", &format!("#{}-weights", model_name))
			.attr("offset", "1"),
	)?;
	writer.write(XmlEvent::end_element().name("input"))?;
	writer.write(XmlEvent::start_element("vcount"))?;
	for bones in bone_weights.iter() {
		buf.clear();
		write!(&mut buf, "{} ", bones.len()).unwrap();
		writer.write(XmlEvent::characters(&buf))?;
	}
	writer.write(XmlEvent::end_element().name("vcount"))?;
	writer.write(XmlEvent::start_element("v"))?;
	let mut index = 0;
	for bones in bone_weights.iter() {
		buf.clear();
		for (bone_id, _) in bones.iter() {
			write!(&mut buf, "{} {} ", bone_id, index).unwrap();
			index += 1;
		}
		writer.write(XmlEvent::characters(&buf))?;
	}
	writer.write(XmlEvent::end_element().name("v"))?;
	writer.write(XmlEvent::end_element().name("vertex_weights"))?;

	writer.write(XmlEvent::end_element().name("skin"))?;
	writer.write(XmlEvent::end_element().name("controller"))?;
	writer.write(XmlEvent::end_element().name("library_controllers"))?;
	Ok(())
}

fn write_bone_node<Writer: io::Write>(
	writer: &mut EventWriter<Writer>,
	model: &Model,
	model_name: &str,
	bone: &Bone,
) -> writer::Result<()> {
	let bones = model.bones();
	writer.write(
		XmlEvent::start_element("node")
			.attr("id", &format!("{}-bone-{}", model_name, bone.name))
			.attr("name", &bone.name)
			.attr("sid", &bone.name)
			.attr("type", "JOINT"),
	)?;

	writer.write(XmlEvent::start_element("matrix").attr("sid", "transform"))?;
	let mut matrix = bones.global_xform_matrix(bone);
	matrix.transpose_mut(); // Column to row major
	let mut buf = String::new();
	for v in matrix.iter() {
		write!(&mut buf, "{} ", v).unwrap();
	}
	writer.write(XmlEvent::characters(&buf))?;
	writer.write(XmlEvent::end_element().name("matrix"))?;

	for child in bones.children_of(Some(bone)) {
		write_bone_node(writer, model, model_name, child)?;
	}

	writer.write(XmlEvent::end_element().name("node"))?;
	Ok(())
}
