//! Exporter for Source Engine SMD format. Currently the most stable format.

use nalgebra::core::{
	Vector2,
	Vector3,
};
use num_traits::Zero;
use std::{
	collections::HashMap,
	io::{
		self,
		Write,
	},
};

use crate::{
	error::Result,
	formats::{
		animations::geom::*,
		models::geom::*,
	},
};

fn write_nodes<Writer: Write>(writer: &mut Writer, bones: &[Bone]) -> io::Result<()> {
	write!(writer, "nodes\r\n")?;
	for (i, bone) in bones.iter().enumerate() {
		let parent_i = bone
			.parent
			.as_ref()
			.and_then(|parent_name| bones.iter().position(|b| &b.name == parent_name))
			.map(|v| v as i64)
			.unwrap_or(-1);
		write!(writer, "{} {:?} {}\r\n", i, bone.name, parent_i)?;
	}
	write!(writer, "end\r\n")?;
	Ok(())
}

pub fn write_smd_model<Writer: Write>(writer: &mut Writer, model: &Model) -> io::Result<()> {
	write!(writer, "version 1\r\n")?;

	// write bones
	let bones = model.bones();
	if !bones.is_empty() {
		write_nodes(writer, &bones)?;

		write!(writer, "skeleton\r\n")?;
		write!(writer, "time 0\r\n")?;
		for (i, bone) in bones.iter().enumerate() {
			let rotation_euler = bone.rotation.euler_angles();
			write!(
				writer,
				"{} {} {} {} {} {} {}\r\n",
				i,
				bone.translation[0],
				bone.translation[1],
				bone.translation[2],
				rotation_euler.0,
				rotation_euler.1,
				rotation_euler.2,
			)?;
		}
		write!(writer, "end\r\n")?;
	} else {
		write!(writer, "nodes\r\n")?;
		write!(writer, "0 \"Origin\" -1\r\n")?;
		write!(writer, "end\r\n")?;

		write!(writer, "skeleton\r\n")?;
		write!(writer, "time 0\r\n")?;
		write!(writer, "0 0 0 0 0 0 0\r\n")?;
		write!(writer, "end\r\n")?;
	}

	let mesh = model.mesh();
	let positions = mesh.positions().ok_or_else(|| {
		io::Error::new(
			io::ErrorKind::Other,
			"Cannot write mesh with no posiitons to SMD model",
		)
	})?;
	let normals = mesh.normals();
	let texture_coords = mesh.texture_coords(0);
	let bone_weights = mesh.bone_weights();

	write!(writer, "triangles\r\n")?;
	for face in mesh.faces().iter() {
		let material = model.materials().get(face.material).ok_or_else(|| {
			io::Error::new(
				io::ErrorKind::Other,
				format!("no material at index {}", face.material),
			)
		})?;
		writeln!(writer, "{}", material.name)?;

		for index in face.indices.iter().cloned() {
			let parent_bone = bone_weights
				.and_then(|weights| weights[index].iter().next())
				.map(|(k, _v)| *k)
				.unwrap_or(0);

			let position = positions[index];
			let normal = normals.map(|v| v[index]).unwrap_or(Vector3::zero());
			let texture_coords = texture_coords.map(|v| v[index]).unwrap_or(Vector2::zero());
			let bone_weights = bone_weights.map(|v| &v[index]);

			write!(
				writer,
				"{} {} {} {} {} {} {} {} {}",
				parent_bone,
				position[0],
				position[1],
				position[2],
				normal[0],
				normal[1],
				normal[2],
				texture_coords[0],
				texture_coords[1],
			)?;

			if let Some(bone_weights) = bone_weights {
				if !bone_weights.is_empty() {
					write!(writer, " {}", bone_weights.len())?;
					for (bone, weight) in bone_weights.iter() {
						write!(writer, " {} {}", *bone, *weight)?;
					}
				}
			}
			write!(writer, "\r\n")?;
		}
	}
	write!(writer, "end\r\n")?;
	Ok(())
}

pub fn write_smd_animation<Writer: Write>(
	writer: &mut Writer,
	base: &[Bone],
	anim: &Animation,
) -> Result<()> {
	write!(writer, "version 1\r\n")?;

	write_nodes(writer, &base)?;

	let bone_to_anim_bone: HashMap<usize, usize> = base
		.iter()
		.enumerate()
		.filter_map(|(bone_i, bone)| {
			let anim_bone_i = anim
				.bones
				.iter()
				.position(|anim_bone| anim_bone.name() == &bone.name)?;
			Some((bone_i, anim_bone_i))
		})
		.collect();

	let last_frame = anim.last_frame();
	write!(writer, "skeleton\r\n")?;
	for frame in 0..(last_frame + 1) {
		write!(writer, "time {}\r\n", frame)?;
		for (bone_i, bone) in base.iter().enumerate() {
			let (position, rotation) = if let Some(anim_bone_i) = bone_to_anim_bone.get(&bone_i) {
				let anim_bone = &anim.bones[*anim_bone_i];
				assert_eq!(bone.name, anim_bone.name());
				let position = anim_bone.position_at(frame as f32, &bone.translation);
				let rotation = anim_bone.rotation_at(frame as f32, &bone.rotation);
				(position, rotation)
			} else {
				(bone.translation, bone.rotation)
			};

			let rotation_euler = rotation.euler_angles();
			write!(
				writer,
				"{} {} {} {} {} {} {}\r\n",
				bone_i,
				position[0],
				position[1],
				position[2],
				rotation_euler.0,
				rotation_euler.1,
				rotation_euler.2,
			)?;
		}
	}
	write!(writer, "end\r\n")?;
	Ok(())
}
