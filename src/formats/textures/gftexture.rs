//! GFTexture format, used for most textures in the Pokemon games.

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use num_traits::FromPrimitive;
use rgb::{
	alt::{
		GRAY8,
		GRAYA8,
	},
	RGB8,
	RGBA8,
};
use std::io::{
	self,
	Read,
	Seek,
};

use crate::{
	error::{
		Error,
		Result,
	},
	formats::{
		misc::etc1,
		textures,
	},
};

/// Reads a GFTexture
pub fn read_gftexture<Reader: Read + Seek>(reader: &mut Reader) -> Result<textures::Texture> {
	let start = reader.seek(io::SeekFrom::Current(0))?;

	reader.seek(io::SeekFrom::Start(start + 0x8))?;
	let mut magic = [0, 0, 0, 0, 0, 0, 0];
	reader.read_exact(&mut magic)?;
	if magic != *b"texture" {
		return Err(Error::parsing(format!(
			"unrecognized gftexture magic: {:?}",
			magic
		)));
	}

	reader.seek(io::SeekFrom::Start(start + 0x18))?;
	let tex_length = reader.read_u32::<LittleEndian>()? as usize;

	reader.seek(io::SeekFrom::Start(start + 0x28))?;
	let mut name_buf = vec![0; 0x40];
	reader.read_exact(&mut name_buf)?;
	let null_loc = name_buf
		.iter()
		.position(|v| *v == 0)
		.unwrap_or(name_buf.len());
	let name = String::from_utf8_lossy(&name_buf[0..null_loc]).into_owned();

	reader.seek(io::SeekFrom::Start(start + 0x68))?;
	let width = reader.read_u16::<LittleEndian>()? as usize;
	let height = reader.read_u16::<LittleEndian>()? as usize;
	let format = reader.read_u16::<LittleEndian>()?;
	let _mipmaps = reader.read_u16::<LittleEndian>()?;

	let format = TextureFormat::from_u16(format)
		.ok_or_else(|| Error::parsing(format!("unrecognized texture format: {:04x}", format)))?;

	reader.seek(io::SeekFrom::Current(0x10))?;
	let mut raw_data = vec![0; tex_length];
	reader.read_exact(&mut raw_data)?;

	let texture_data = decode_texture(&raw_data, &format, width, height)?;
	Ok(textures::Texture::new(name, width, height, texture_data))
}

/// GFTexture data formats
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, FromPrimitive, ToPrimitive)]
pub enum TextureFormat {
	Rgb565 = 0x2,
	Rgb8 = 0x3,
	Rgba8 = 0x4,
	Rgba4 = 0x16,
	Rgba5551 = 0x17,
	La8 = 0x23,
	Hilo8 = 0x24,
	L8 = 0x25,
	A8 = 0x26,
	La4 = 0x27,
	L4 = 0x28,
	A4 = 0x29,
	Etc1 = 0x2a,
	Etc1a4 = 0x2b,
}

/// Pixels in this format aren't stored in left-to-right order, but in a per-tile
/// winding curve (presumably matching the GPU's internal format).
const TILE_ORDER: &'static [usize] = &[
	0, 1, 8, 9, 2, 3, 10, 11, 16, 17, 24, 25, 18, 19, 26, 27, 4, 5, 12, 13, 6, 7, 14, 15, 20, 21,
	28, 29, 22, 23, 30, 31, 32, 33, 40, 41, 34, 35, 42, 43, 48, 49, 56, 57, 50, 51, 58, 59, 36, 37,
	44, 45, 38, 39, 46, 47, 52, 53, 60, 61, 54, 55, 62, 63,
];

/// Decodes the texture and returns a generic texture.
fn decode_texture(
	raw_data: &[u8],
	format: &TextureFormat,
	width: usize,
	height: usize,
) -> Result<textures::TextureData> {
	let decoded_data = match format {
		TextureFormat::Rgba8 => {
			let mut data = vec![
				RGBA8 {
					r: 0,
					g: 0,
					b: 0,
					a: 0
				};
				width * height
			];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				data[dest_offset] = RGBA8 {
					r: get(raw_data, src_offset * 4 + 3)?,
					g: get(raw_data, src_offset * 4 + 2)?,
					b: get(raw_data, src_offset * 4 + 1)?,
					a: get(raw_data, src_offset * 4 + 0)?,
				};
			}
			textures::TextureData::RGBA(data)
		}
		TextureFormat::Rgb8 => {
			let mut data = vec![RGB8 { r: 0, g: 0, b: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				data[dest_offset] = RGB8 {
					r: get(raw_data, src_offset * 3 + 2)?,
					g: get(raw_data, src_offset * 3 + 1)?,
					b: get(raw_data, src_offset * 3 + 0)?,
				};
			}
			textures::TextureData::RGB(data)
		}
		TextureFormat::Rgba4 => {
			let mut data = vec![
				RGBA8 {
					r: 0,
					g: 0,
					b: 0,
					a: 0
				};
				width * height
			];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let pixel = (get(raw_data, src_offset * 2)? as u16)
					| ((get(raw_data, src_offset * 2 + 1)? as u16) << 8);
				let r = ((pixel >> 4) & 0xf) as u8;
				let g = ((pixel >> 8) & 0xf) as u8;
				let b = ((pixel >> 12) & 0xf) as u8;
				let a = ((pixel) & 0xf) as u8;

				data[dest_offset] = RGBA8 {
					r: (r | (r << 4)),
					g: (g | (g << 4)),
					b: (b | (b << 4)),
					a: (a | (a << 4)),
				};
			}
			textures::TextureData::RGBA(data)
		}
		TextureFormat::Rgba5551 => {
			let mut data = vec![
				RGBA8 {
					r: 0,
					g: 0,
					b: 0,
					a: 0
				};
				width * height
			];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let pixel = (get(raw_data, src_offset * 2)? as u16)
					| ((get(raw_data, src_offset * 2 + 1)? as u16) << 8);
				let r = ((pixel >> 1) & 0x1f) << 3;
				let g = ((pixel >> 6) & 0x1f) << 3;
				let b = ((pixel >> 11) & 0x1f) << 3;
				let a = (pixel & 1) * 0xff;

				data[dest_offset] = RGBA8 {
					r: (r | (r >> 5)) as u8,
					g: (g | (g >> 5)) as u8,
					b: (b | (b >> 5)) as u8,
					a: a as u8,
				};
			}
			textures::TextureData::RGBA(data)
		}
		TextureFormat::Rgb565 => {
			let mut data = vec![RGB8 { r: 0, g: 0, b: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let pixel = (get(raw_data, src_offset * 2)? as u16)
					| ((get(raw_data, src_offset * 2 + 1)? as u16) << 8);
				let r = ((pixel) & 0x1f) << 3;
				let g = ((pixel >> 5) & 0x3f) << 2;
				let b = ((pixel >> 11) & 0x1f) << 3;

				data[dest_offset] = RGB8 {
					r: (r | (r >> 5)) as u8,
					g: (g | (g >> 6)) as u8,
					b: (b | (b >> 5)) as u8,
				};
			}
			textures::TextureData::RGB(data)
		}
		TextureFormat::L8 => {
			let mut data = vec![GRAY8 { 0: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				data[dest_offset] = GRAY8 {
					0: get(raw_data, src_offset)?,
				};
			}
			textures::TextureData::Grayscale(data)
		}
		TextureFormat::A8 => {
			let mut data = vec![GRAYA8 { 0: 0, 1: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				data[dest_offset] = GRAYA8 {
					0: 0xff,
					1: get(raw_data, src_offset)?,
				};
			}
			textures::TextureData::GrayscaleAlpha(data)
		}
		TextureFormat::La8 | TextureFormat::Hilo8 => {
			let mut data = vec![GRAYA8 { 0: 0, 1: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let l = get(raw_data, src_offset * 2)?;
				let a = get(raw_data, src_offset * 2 + 1)?;
				data[dest_offset] = GRAYA8 { 0: l, 1: a };
			}
			textures::TextureData::GrayscaleAlpha(data)
		}
		TextureFormat::L4 => {
			let mut data = vec![GRAY8 { 0: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let src_data = get(raw_data, src_offset / 2)?;
				let pixel = if src_offset % 2 == 0 {
					src_data & 0xf
				} else {
					src_data >> 4
				};
				data[dest_offset] = GRAY8 {
					0: pixel | (pixel << 4),
				};
			}
			textures::TextureData::Grayscale(data)
		}
		TextureFormat::La4 => {
			let mut data = vec![GRAYA8 { 0: 0, 1: 0 }; width * height];
			for (src_offset, dest_offset) in tile_iter(width, height).enumerate() {
				let l = get(raw_data, src_offset)? >> 4;
				let a = get(raw_data, src_offset)? & 0xf;
				data[dest_offset] = GRAYA8 {
					0: (l << 4) | l,
					1: (a << 4) | a,
				};
			}
			textures::TextureData::GrayscaleAlpha(data)
		}
		TextureFormat::Etc1 => {
			let mut cursor = io::Cursor::new(raw_data);
			let data = with_loc!("etc1 data", etc1::read_etc1(&mut cursor, width, height))?;
			textures::TextureData::RGB(data)
		}
		TextureFormat::Etc1a4 => {
			let mut cursor = io::Cursor::new(raw_data);
			let data = with_loc!("etc1a4 data", etc1::read_etc1a4(&mut cursor, width, height))?;
			textures::TextureData::RGBA(data)
		}
		_ => {
			return Err(Error::parsing(format!(
				"Decoding from GFTexture format {:?} not yet implemented",
				format
			)));
		}
	};
	Ok(decoded_data)
}

fn get<T: Clone>(arr: &[T], index: usize) -> Result<T> {
	arr.get(index)
		.map(|r| r.clone())
		.ok_or_else(|| Error::parsing("Not enough texture data for image"))
}

/// Iterator to help with tile traversal.
fn tile_iter(width: usize, height: usize) -> impl Iterator<Item = usize> {
	(0..(height / 8))
		.flat_map(move |tile_y| {
			(0..(width / 8)).flat_map(move |tile_x| {
				(0..TILE_ORDER.len()).map(move |tile| (tile_x, tile_y, tile))
			})
		})
		.map(move |(tile_x, tile_y, tile)| {
			let pixel_x = TILE_ORDER[tile] % 8;
			let pixel_y = (TILE_ORDER[tile] - pixel_x) / 8;
			let output_offset = (tile_x * 8) + pixel_x + (tile_y * 8 + pixel_y) * width;
			output_offset
		})
}
