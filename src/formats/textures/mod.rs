//! Texture format parsers

pub mod gftexture;

use png::{
	self,
	HasParameters,
};
use rgb::{
	alt::{
		GRAY8,
		GRAYA8,
	},
	ComponentBytes,
	RGB8,
	RGBA8,
};
use std::io::{
	self,
	Write,
};

/// Underlying texture data
#[derive(Debug, Clone)]
pub enum TextureData {
	RGB(Vec<RGB8>),
	RGBA(Vec<RGBA8>),
	Grayscale(Vec<GRAY8>),
	GrayscaleAlpha(Vec<GRAYA8>),
}
impl TextureData {
	/// Gets the number of pixels in the data (i.e. the length of the underlying Vec).
	pub fn num_pixels(&self) -> usize {
		match self {
			TextureData::RGB(v) => v.len(),
			TextureData::RGBA(v) => v.len(),
			TextureData::Grayscale(v) => v.len(),
			TextureData::GrayscaleAlpha(v) => v.len(),
		}
	}
}

/// Generic texture type, consisting of a flat array of pixel values.
#[derive(Debug, Clone)]
pub struct Texture {
	name: String,
	width: usize,
	height: usize,
	data: TextureData,
}
impl Texture {
	/// Creates a new texture
	///
	/// # Panics
	///
	/// If `data.num_pixels() != width*height`.
	pub fn new(name: String, width: usize, height: usize, data: TextureData) -> Self {
		if width * height != data.num_pixels() {
			panic!(
				"Tried to create {}x{} image with {} pixels in it (should have {})",
				width,
				height,
				data.num_pixels(),
				width * height
			);
		}

		Self {
			name,
			width,
			height,
			data,
		}
	}

	/// Gets the texture name
	pub fn name(&self) -> &str {
		&self.name
	}

	/// Gets the texture width
	pub fn width(&self) -> usize {
		self.width
	}

	/// Gets the texture height
	pub fn height(&self) -> usize {
		self.height
	}

	/// Gets the texture data
	pub fn data(&self) -> &TextureData {
		&self.data
	}

	/// Gets a pixel in the texture and converts it to RGBA.
	///
	/// Returns `None` if the coordinates are out of range.
	pub fn get_rgba(&self, x: usize, y: usize) -> Option<RGBA8> {
		if x >= self.width || y >= self.height {
			return None;
		}

		let i = y * self.width + x;
		Some(match &self.data {
			TextureData::RGB(v) => RGBA8 {
				r: v[i].r,
				g: v[i].g,
				b: v[i].b,
				a: 0xff,
			},
			TextureData::RGBA(v) => v[i],
			TextureData::Grayscale(v) => RGBA8 {
				r: v[i].0,
				g: v[i].0,
				b: v[i].0,
				a: 0xff,
			},
			TextureData::GrayscaleAlpha(v) => RGBA8 {
				r: v[i].0,
				g: v[i].0,
				b: v[i].0,
				a: v[i].1,
			},
		})
	}

	/// Encodes the texture as a PNG file and writes it to a `Write` object.
	///
	/// The PNG color format will match the underlying `TextureData` format.
	pub fn write_png<Writer: Write>(&self, writer: &mut Writer) -> io::Result<()> {
		let mut encoder = png::Encoder::new(writer, self.width as u32, self.height as u32);
		encoder.set(match &self.data {
			TextureData::RGB(_) => png::ColorType::RGB,
			TextureData::RGBA(_) => png::ColorType::RGBA,
			TextureData::Grayscale(_) => png::ColorType::Grayscale,
			TextureData::GrayscaleAlpha(_) => png::ColorType::GrayscaleAlpha,
		});
		encoder.set(png::BitDepth::Eight);
		let mut writer = encoder.write_header()?;
		writer.write_image_data(match &self.data {
			TextureData::RGB(v) => v.as_bytes(),
			TextureData::RGBA(v) => v.as_bytes(),
			TextureData::Grayscale(v) => v.as_bytes(),
			TextureData::GrayscaleAlpha(v) => v.as_bytes(),
		})?;
		Ok(())
	}
}
