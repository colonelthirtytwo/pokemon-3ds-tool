//! Utilities that several container parsers use.

use byteorder::ReadBytesExt;
use std::io::{
	self,
	Read,
	Seek,
};

use crate::{
	error::Result,
	ioutils::skip,
	lzss::LzssDecompressor,
};

/// Helper for storing + reading from a section of a file.
pub struct RawSection {
	pub offset: u64,
	pub size: u64,
}
impl RawSection {
	pub fn read<Reader: Read + Seek>(
		&self,
		reader: &mut Reader,
		offset: u64,
		max_size: Option<usize>,
	) -> io::Result<Vec<u8>> {
		if offset >= self.size {
			return Ok(vec![]);
		}
		reader.seek(io::SeekFrom::Start(self.offset + offset))?;
		let mut partial_reader = reader.by_ref().take(self.size);

		let mut buffer = vec![];
		if let Some(max_size) = max_size {
			partial_reader
				.take(max_size as u64)
				.read_to_end(&mut buffer)?;
		} else {
			partial_reader.read_to_end(&mut buffer)?;
		}
		Ok(buffer)
	}
}

pub struct LzssSection {
	pub offset: u64,
	pub raw_size: u64,
	pub decompressed_size: u64,
	pub is_compressed: bool,
}
impl LzssSection {
	pub fn identify<Reader: Read + Seek>(
		reader: &mut Reader,
		offset: u64,
		raw_size: u64,
	) -> Result<Self> {
		reader.seek(io::SeekFrom::Start(offset))?;
		let is_compressed = reader.read_u8()? == 0x11;

		if is_compressed {
			reader.seek(io::SeekFrom::Start(offset))?;
			let compressed_reader = LzssDecompressor::new(reader)?;
			let decompressed_size = compressed_reader.decompressed_total_size() as u64;
			Ok(Self {
				offset,
				raw_size,
				decompressed_size,
				is_compressed: true,
			})
		} else {
			Ok(Self {
				offset,
				raw_size,
				decompressed_size: raw_size,
				is_compressed: false,
			})
		}
	}

	pub fn read<Reader: Read + Seek>(
		&self,
		reader: &mut Reader,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		if !self.is_compressed {
			let plain_section = RawSection {
				offset: self.offset,
				size: self.decompressed_size,
			};
			return Ok(plain_section.read(reader, offset, max_size)?);
		}

		reader.seek(io::SeekFrom::Start(self.offset))?;
		let partial_reader = reader.by_ref().take(self.raw_size);
		let mut compressed_reader = LzssDecompressor::new(partial_reader)?;
		skip(&mut compressed_reader, offset)?;

		let mut vec = vec![];
		if let Some(max_size) = max_size {
			compressed_reader
				.take(max_size as u64)
				.read_to_end(&mut vec)?;
		} else {
			compressed_reader.read_to_end(&mut vec)?;
		}
		return Ok(vec);
	}
}
