//! ARC file format

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::{
	cell::RefCell,
	io,
	rc::Rc,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::containers::{
		self,
		common::LzssSection,
	},
};

pub const MAGIC: [u8; 4] = *b"ARC\0";

pub struct ArcContainer<Reader: io::Read + io::Seek> {
	reader: Rc<RefCell<Reader>>,
	entries: Vec<containers::Entry>,
	sections: Vec<LzssSection>,
}
impl<Reader: io::Read + io::Seek> ArcContainer<Reader> {
	pub fn new(reader_rc: Rc<RefCell<Reader>>) -> Result<Self> {
		let reader_rc_cloned = Rc::clone(&reader_rc);
		let mut reader = reader_rc.borrow_mut();

		let mut magic: [u8; 4] = [0, 0, 0, 0];
		with_loc!("magic number", reader.read_exact(&mut magic))?;
		if &magic != &MAGIC {
			return Err(Error::parsing(format!(
				"incorrect magic number: {:X?}",
				magic
			)));
		}

		let _endian_or_version_maybe =
			with_loc!("endian/version", reader.read_u16::<LittleEndian>())?;
		let num_entries =
			with_loc!("number of entries", reader.read_u16::<LittleEndian>())? as usize;
		with_loc!("padding", reader.read_u32::<LittleEndian>())?; // Padding maybe?

		let mut temp_entries: Vec<(String, u64, u64)> = Vec::with_capacity(num_entries);

		for i in 0..num_entries {
			let _loc = Location::new(format!("entry {} metadata", i));

			let mut name_buf = vec![0; 0x40];
			with_loc!("name", reader.read_exact(&mut name_buf))?;
			let null_loc = name_buf
				.iter()
				.position(|v| *v == 0)
				.unwrap_or(name_buf.len());
			let name = String::from_utf8_lossy(&name_buf[0..null_loc]).into_owned();

			let _unknown1 = with_loc!("unknown field 1", reader.read_u32::<LittleEndian>())?;
			let length = with_loc!("entry length", reader.read_u32::<LittleEndian>())? as u64;
			let _unknown2 = with_loc!("unknown field 2", reader.read_u32::<LittleEndian>())?;
			let start = with_loc!("entry start offset", reader.read_u32::<LittleEndian>())? as u64;

			temp_entries.push((name, start, length));
		}

		let mut sections = Vec::with_capacity(num_entries);
		let mut entries = Vec::with_capacity(num_entries);

		for (i, (name, start, length)) in temp_entries.into_iter().enumerate() {
			let section = with_loc!(
				format!("entry {:?} ({})", name, i),
				LzssSection::identify(&mut *reader, start, length)
			)?;
			let entry = containers::Entry {
				index: i,
				path: containers::EntryPath::new(
					name.split('\\').map(|c| String::from(c)).collect(),
				)
				.unwrap(),
				size: section.decompressed_size,
				format_override: None,
			};
			sections.push(section);
			entries.push(entry);
		}

		Ok(Self {
			reader: reader_rc_cloned,
			entries,
			sections,
		})
	}
}
impl<Reader: io::Read + io::Seek> containers::Container for ArcContainer<Reader> {
	fn metadata(&self) -> String {
		"".to_string()
	}

	fn entries(&self) -> &[containers::Entry] {
		self.entries.as_slice()
	}

	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		let mut reader = self.reader.borrow_mut();
		self.sections[entry_index].read(&mut *reader, offset, max_size)
	}
}
