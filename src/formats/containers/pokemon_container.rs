//! Pokemon container format parser.

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::{
	cell::RefCell,
	fmt,
	io::{
		Read,
		Seek,
	},
	rc::Rc,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::containers::{
		self,
		common::LzssSection,
	},
};

/// Magic numbers, stored in the first two bytes of the container and defines
/// what data to expect in it.
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Magic {
	AD,
	BM,
	BS,
	/// CM Overworld character model.
	/// One GF model followed by zero or more GF motion animations.
	CM,
	/// CP Overworld character model.
	/// Contains one CM container at index 1 (?).
	CP,
	/// GR Map
	GR,
	// MM overworld "chibi" character model.
	// Contains one BCH format.
	MM,
	/// PC monster model.
	/// Can contain models, animations, and textures.
	PC,
	PT,
	/// Unrecognized code.
	Unrecognized([u8; 2]),
}
impl Magic {
	/// Returns the two-byte code for a magic number.
	pub fn code(&self) -> [u8; 2] {
		let slice = match self {
			Magic::AD => b"AD",
			Magic::BM => b"BM",
			Magic::BS => b"BS",
			Magic::CM => b"CM",
			Magic::CP => b"CP",
			Magic::GR => b"GR",
			Magic::MM => b"MM",
			Magic::PC => b"PC",
			Magic::PT => b"PT",
			Magic::Unrecognized(v) => &v,
		};
		[slice[0], slice[1]]
	}

	/// Returns a constant array of the known magic codes.
	pub fn recognized_values() -> &'static [Magic] {
		const VALUES: &'static [Magic] = &[
			Magic::AD,
			Magic::BM,
			Magic::BS,
			Magic::CM,
			Magic::CP,
			Magic::GR,
			Magic::MM,
			Magic::PC,
			Magic::PT,
		];
		VALUES
	}
}
impl From<[u8; 2]> for Magic {
	fn from(from: [u8; 2]) -> Self {
		Magic::recognized_values()
			.iter()
			.find(|v| v.code() == from)
			.map(|v| *v)
			.unwrap_or_else(|| Magic::Unrecognized(from))
	}
}
impl fmt::Display for Magic {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			Magic::AD => f.write_str("Unknown (AD)"),
			Magic::BM => f.write_str("Unknown (BM)"),
			Magic::BS => f.write_str("Unknown (BS)"),
			Magic::CM => f.write_str("Overworld Character Model (CM)"),
			Magic::CP => f.write_str("Overworld Character Model (CP)"),
			Magic::GR => f.write_str("Map (GR)"),
			Magic::MM => f.write_str("Overworld Character Model (MM)"),
			Magic::PC => f.write_str("Pokemon Model (PC)"),
			Magic::PT => f.write_str("Unknown (PT)"),
			Magic::Unrecognized(code) => write!(f, "Unrecognized ({:?})", code),
		}
	}
}

/// Pokemon container format.
///
/// Usually found in GARC containers, encompassing a set of related resources.
/// Has several magic numbers that help define the semantics of the contained
/// data.
pub struct PokemonContainer<Reader: Read + Seek> {
	reader: Rc<RefCell<Reader>>,
	entries: Vec<containers::Entry>,
	sections: Vec<LzssSection>,
	magic: Magic,
}
impl<Reader: Read + Seek> PokemonContainer<Reader> {
	pub fn new(reader_rc: Rc<RefCell<Reader>>) -> Result<Self> {
		let reader_rc_cloned = Rc::clone(&reader_rc);
		let mut reader = reader_rc.borrow_mut();

		let mut magic: [u8; 2] = [0, 0];
		with_loc!("magic", reader.read_exact(&mut magic))?;
		let magic = Magic::from(magic);

		let num_sections = with_loc!("number of sections", reader.read_u16::<LittleEndian>())?;
		let mut section_bounds = Vec::with_capacity(num_sections as usize);
		let mut prev_end = with_loc!("first offset", reader.read_u32::<LittleEndian>())?;
		for i in 0..num_sections {
			let _loc = Location::new(format!("end offset for section {}", i));
			let start = prev_end;
			let end = reader.read_u32::<LittleEndian>()?;
			prev_end = end;

			if start > end {
				return Err(Error::parsing(format!(
					"start offset (byte {}) > end offset (byte {}) for entry {}",
					start, end, i
				)));
			}

			section_bounds.push((start, end))
		}

		let mut sections = Vec::with_capacity(num_sections as usize);
		let mut entries = Vec::with_capacity(num_sections as usize);

		for (i, (start, end)) in section_bounds.into_iter().enumerate() {
			let _loc = Location::new(format!("section {}", i));

			let section = LzssSection::identify(&mut *reader, start as u64, (end - start) as u64)?;
			let entry = containers::Entry {
				index: i,
				path: containers::EntryPath::new(vec![i.to_string()]).unwrap(),
				size: section.decompressed_size,
				format_override: None,
			};

			sections.push(section);
			entries.push(entry);
		}

		Ok(Self {
			reader: reader_rc_cloned,
			entries,
			sections,
			magic,
		})
	}

	/// Gets the parsed magic string. See the `Magic` enum for more info.
	pub fn magic(&self) -> Magic {
		self.magic
	}
}
impl<Reader: Read + Seek> containers::Container for PokemonContainer<Reader> {
	fn metadata(&self) -> String {
		format!(
			"Type: Pokemon Container\nMagic Number: {}{}",
			self.magic.code()[0] as char,
			self.magic.code()[1] as char
		)
	}

	fn entries(&self) -> &[containers::Entry] {
		self.entries.as_slice()
	}

	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		let mut reader = self.reader.borrow_mut();
		self.sections[entry_index].read(&mut *reader, offset, max_size)
	}
}
