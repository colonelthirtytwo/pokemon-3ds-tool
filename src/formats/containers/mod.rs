//! Formats for containers, which pack other data into one file.
//!
//! Containers are a bit more complicated to use than other formats. Because they
//! are usually quite large, they don't load the entire data set in memory.
//! Rather, they take an `Rc<RefCell<Reader>>` and use it to read entries as they
//! are needed.
//!
//! `Container::new` and `Entry::read` will borrow the file mutably to read the
//! container or entry. Other trait functions will not borrow the reader (though
//! container-specific functions may).
//!
//! After the container is parsed, other code may read and seek from the
//! underlying reader. `Entry::read` will seek the reader to the appropriate
//! location when reading.

pub mod arc;
pub(crate) mod common;
pub mod garc;
pub mod gfmodel_container;
pub mod pokemon_container;
pub mod sarc;

use std::{
	error,
	fmt,
	iter,
	ops,
	result::Result as StdResult,
	str,
};

use crate::error::Result;

pub use self::{
	garc::GarcContainer,
	pokemon_container::PokemonContainer,
};

/// Container entry metadata
#[derive(Debug, Clone)]
pub struct Entry {
	/// Index that the entry is at in the array that `Container.entries()` returns.
	pub index: usize,
	/// File path of the container entry. May include folder components, if the
	/// container supports subfolders.
	pub path: EntryPath,
	/// Entry data size, in bytes.
	pub size: u64,
	/// If set, forces reading the entry as a particular format.
	pub format_override: Option<crate::formats::Format>,
}

/// Trait for container formats.
///
/// Supports being a dynamic trait, to allow using different container formats
/// at runtime.
///
/// Most implementors of this trait have a `new` method that parses the container.
pub trait Container {
	/// Gets the metadata for the entries that this container contains.
	fn entries(&self) -> &[Entry];

	/// Reads an entry.
	///
	/// # Parameters
	/// * `entry_index`: Index of the entry to read. Corresponds to items in the
	///   array that `entries` returns.
	/// * `offset`: Offset, in bytes, into the entry to begin reading from.
	/// * `max_size`: Maximum amount of bytes to read from the entry. If `None`,
	///   read the entire entry.
	///
	/// # Panics
	/// If `entry_index` is greater than or equal to the number of entries in the
	/// container. Check the `entries` array before using an index.
	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>>;

	/// Returns a string describing any format-specific metadata.
	fn metadata(&self) -> String;
}

/// Entry path
///
/// This is an OS-agnostic version of `std::path` for entries in containers.
/// It stores a list of `String`s.
///
/// Paths must have at least one component.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct EntryPath {
	path: Vec<String>,
}
impl EntryPath {
	pub fn new(path: Vec<String>) -> StdResult<Self, PathError> {
		if path.is_empty() {
			return Err(PathError::Empty);
		}
		if path.iter().any(|component| component.contains('/')) {
			return Err(PathError::ContainedSeparator);
		}

		Ok(Self { path })
	}

	pub fn single_component<S: ToString>(name: S) -> StdResult<Self, PathError> {
		Self::new(vec![name.to_string()])
	}

	/// Tries to create a path from an iterator.
	pub fn try_from_iter<I: Iterator<Item = S>, S: Into<String>>(
		components: I,
	) -> StdResult<Self, PathError> {
		Self::new(components.map(|s| s.into()).collect::<Vec<String>>())
	}

	/// Pushes a component onto the end of the path
	pub fn push(&mut self, component: String) -> StdResult<(), PathError> {
		if component.contains('/') {
			return Err(PathError::ContainedSeparator);
		}
		self.path.push(component);
		Ok(())
	}
}
impl ops::Deref for EntryPath {
	type Target = [String];

	fn deref(&self) -> &Self::Target {
		&self.path
	}
}
impl fmt::Display for EntryPath {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		for (i, component) in self.iter().enumerate() {
			if i != 0 {
				f.write_str("/")?;
			}
			f.write_str(component)?;
		}
		Ok(())
	}
}
impl str::FromStr for EntryPath {
	type Err = PathError;

	fn from_str(s: &str) -> StdResult<Self, Self::Err> {
		Self::new(s.split('/').map(|s| String::from(s)).collect::<Vec<_>>())
	}
}
impl<S: Into<String>> iter::FromIterator<S> for EntryPath {
	fn from_iter<I: IntoIterator<Item = S>>(iter: I) -> Self {
		Self::try_from_iter(iter.into_iter())
			.expect("EntryPath::from_iter got zero-length iterator")
	}
}
impl<'a> iter::FromIterator<&'a EntryPath> for EntryPath {
	fn from_iter<I: IntoIterator<Item = &'a EntryPath>>(iter: I) -> Self {
		Self::try_from_iter(iter.into_iter().flat_map(|p| p.iter().cloned()))
			.expect("EntryPath::from_iter got zero-length iterator")
	}
}

/// Error returned when creating an invalid path
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PathError {
	/// No components
	Empty,
	/// Component had a `"/"` in it
	ContainedSeparator,
}
impl fmt::Display for PathError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			PathError::Empty => f.write_str("Tried to create path with no components"),
			PathError::ContainedSeparator => {
				f.write_str("Tried to create path with a component containing '/'")
			}
		}
	}
}
impl error::Error for PathError {}

#[cfg(test)]
mod tests {
	use super::*;

	fn _check_can_return_dyn_container() -> Box<dyn Container> {
		unimplemented!();
	}

	#[test]
	fn entry_path_empty() {
		assert!(EntryPath::new(vec![]).is_err());
	}

	#[test]
	fn entry_path_some() {
		let path = EntryPath::new(vec!["one".to_string(), "two".to_string()]).unwrap();
		assert_eq!(path[0], "one");
		assert_eq!(path[1], "two");
		assert_eq!(path.to_string(), "one/two");
	}
}
