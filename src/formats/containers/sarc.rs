use std::{
	cell::RefCell,
	io::{
		self,
		Read,
		Seek,
	},
	rc::Rc,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::containers::{
		common::RawSection,
		Container,
		Entry,
		EntryPath,
	},
	parsing,
};

pub const MAGIC: [u8; 4] = *b"SARC";

pub struct SarcContainer<Reader: Read + Seek> {
	reader: Rc<RefCell<Reader>>,
	entries: Vec<Entry>,
	data_sections: Vec<RawSection>,
}
impl<Reader: Read + Seek> SarcContainer<Reader> {
	pub fn new(reader_rc: Rc<RefCell<Reader>>) -> Result<Self> {
		let reader_rc_cloned = Rc::clone(&reader_rc);
		let mut r = reader_rc.borrow_mut();

		let sarc_header = with_loc!("sarc header", SarcHeader::parse(&mut *r))?;
		let sfat_header = with_loc!(
			"sfat header",
			SfatHeader::parse(&mut *r, sarc_header.endian)
		)?;
		let sfat_nodes = parsing::repeat(sfat_header.node_count, "sfat node", || {
			SfatNode::parse(&mut *r, sarc_header.endian)
		})?;
		let _sfnt_header = with_loc!(
			"sfnt header",
			SfntHeader::parse(&mut *r, sarc_header.endian)
		)?;

		let sfnt_begin = r.seek(io::SeekFrom::Current(0))?;

		let mut data_sections = vec![];
		let mut entries = vec![];

		for (i, fat_node) in sfat_nodes.iter().enumerate() {
			let _loc = Location::new(format!("node {} filename", i));

			let name = if let Some(offset) = fat_node.sfnt_filename_offset {
				r.seek(io::SeekFrom::Start(sfnt_begin + offset as u64))?;
				parsing::read_null_terminated_str(&mut *r)?
			} else {
				format!("file{:03}.noname.bin", i)
			};

			let offset = sarc_header.data_offset as u64 + fat_node.file_data_start as u64;
			let size = fat_node.file_data_end as u64 - fat_node.file_data_start as u64;

			let section = RawSection { offset, size };
			let entry = Entry {
				index: i,
				path: EntryPath::single_component(name).unwrap(),
				size,
				format_override: None,
			};

			entries.push(entry);
			data_sections.push(section);
		}

		Ok(Self {
			reader: reader_rc_cloned,
			entries,
			data_sections,
		})
	}
}
impl<Reader: Read + Seek> Container for SarcContainer<Reader> {
	fn metadata(&self) -> String {
		"Type: SARC".into()
	}

	fn entries(&self) -> &[Entry] {
		self.entries.as_slice()
	}

	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		let mut reader = self.reader.borrow_mut();
		self.data_sections[entry_index]
			.read(&mut *reader, offset, max_size)
			.map_err(Into::into)
	}
}

#[derive(Debug, Clone)]
struct SarcHeader {
	pub endian: parsing::Endian,
	pub header_len: u16,
	pub file_len: u32,
	pub data_offset: u32,
}
impl SarcHeader {
	pub const SIZE: usize = 20;

	pub fn parse<R: Read>(r: &mut R) -> Result<Self> {
		parsing::magic(r, &MAGIC)?;
		let header_len_raw = with_loc!("header length", parsing::take(r, 2))?;
		let endian = with_loc!("endian", bom_to_endian(&parsing::take(r, 2)?)?);
		let header_len = match endian {
			parsing::Endian::Big => u16::from_be_bytes([header_len_raw[0], header_len_raw[1]]),
			parsing::Endian::Little => u16::from_le_bytes([header_len_raw[0], header_len_raw[1]]),
		};
		parsing::assert_eq(header_len, Self::SIZE as u16, "Unrecognized header length")?;

		let file_len = with_loc!("file length", parsing::read_u32(r, endian))?;
		let data_offset = with_loc!("data offset", parsing::read_u32(r, endian))?;
		with_loc!("padding", parsing::take(r, 4))?;

		Ok(Self {
			endian,
			header_len,
			file_len,
			data_offset,
		})
	}
}

fn bom_to_endian(bytes: &[u8]) -> Result<parsing::Endian> {
	assert_eq!(bytes.len(), 2);
	if bytes == &[0xff, 0xfe] {
		Ok(parsing::Endian::Little)
	} else if bytes == &[0xfe, 0xff] {
		Ok(parsing::Endian::Big)
	} else {
		Err(Error::parsing(format!("Unrecognized BOM: {:X?}", bytes)))
	}
}

#[derive(Debug, Clone)]
struct SfatHeader {
	pub header_len: u16,
	pub node_count: u16,
	pub hash_multiplier: u32,
}
impl SfatHeader {
	pub const SIZE: usize = 12;

	pub fn parse<R: Read>(r: &mut R, endian: parsing::Endian) -> Result<Self> {
		parsing::magic(r, b"SFAT")?;
		let header_len = with_loc!("header len", parsing::read_u16(r, endian))?;
		parsing::assert_eq(header_len, Self::SIZE as u16, "Unrecognized header length")?;
		let node_count = with_loc!("node count", parsing::read_u16(r, endian))?;
		let hash_multiplier = with_loc!("hash multiplier", parsing::read_u32(r, endian))?;

		Ok(Self {
			header_len,
			node_count,
			hash_multiplier,
		})
	}
}

#[derive(Debug, Clone)]
struct SfatNode {
	pub name_hash: u32,
	pub sfnt_filename_offset: Option<u32>,
	pub file_data_start: u32,
	pub file_data_end: u32,
}
impl SfatNode {
	//pub const SIZE: usize = 16;

	pub fn parse<R: Read>(r: &mut R, endian: parsing::Endian) -> Result<Self> {
		let name_hash = parsing::read_u32(r, endian)?;
		let raw_sfnt_filename_offset = parsing::read_u32(r, endian)?;
		let file_data_start = parsing::read_u32(r, endian)?;
		let file_data_end = parsing::read_u32(r, endian)?;

		let sfnt_filename_offset = if (raw_sfnt_filename_offset >> 24) != 0 {
			Some(raw_sfnt_filename_offset & 0xffffff)
		} else {
			None
		};

		parsing::assert_valid_range(file_data_start, file_data_end, "Invalid file data range")?;

		Ok(Self {
			name_hash,
			sfnt_filename_offset,
			file_data_start,
			file_data_end,
		})
	}
}

#[derive(Debug, Clone)]
struct SfntHeader {
	pub header_len: u16,
}
impl SfntHeader {
	pub const SIZE: usize = 8;

	pub fn parse<R: Read>(r: &mut R, endian: parsing::Endian) -> Result<Self> {
		parsing::magic(r, b"SFNT")?;
		let header_len = parsing::read_u16(r, endian)?;
		parsing::assert_eq(header_len, Self::SIZE as u16, "Unrecognized sfnt header")?;
		parsing::take(r, 2)?;

		Ok(Self { header_len })
	}
}
