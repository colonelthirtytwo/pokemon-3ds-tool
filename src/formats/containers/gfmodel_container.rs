//! Parser for the GFModel Container format

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::{
	cell::RefCell,
	io,
	rc::Rc,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::containers::{
		self,
		common::RawSection,
	},
	ioutils::read_pascal_str_u8,
};

/// GfModel container magic sequence, at the start of the file
pub const MAGIC: [u8; 4] = [0x00, 0x00, 0x01, 0x00];

/// GFModel container
///
/// Contains GFModels and GFTextures
pub struct GfModelContainer<Reader: io::Read + io::Seek> {
	reader: Rc<RefCell<Reader>>,
	entries: Vec<containers::Entry>,
	sections: Vec<RawSection>,
}
impl<Reader: io::Read + io::Seek> GfModelContainer<Reader> {
	pub fn new(reader_rc: Rc<RefCell<Reader>>) -> Result<Self> {
		let reader_rc_cloned = Rc::clone(&reader_rc);
		let mut reader = reader_rc.borrow_mut();
		let mut magic: [u8; 4] = [0, 0, 0, 0];
		reader.read_exact(&mut magic)?;
		if &magic != &MAGIC {
			return Err(Error::parsing("incorrect magic number"));
		}

		let section_item_counts = ((0..5)
			.map(|_| reader.read_u32::<LittleEndian>())
			.collect::<io::Result<Vec<_>>>())?;

		let item_metadata_offsets = (section_item_counts
			.iter()
			.enumerate()
			.flat_map(|(section, &num_items)| {
				(0..num_items)
					.map(|_| {
						reader
							.read_u32::<LittleEndian>()
							.map(|v| (section as u8, v))
					})
					.collect::<Vec<_>>() // Need this: https://stackoverflow.com/questions/28521637/how-can-i-move-a-captured-variable-into-a-closure-within-a-closure
					.into_iter()
			})
			.collect::<io::Result<Vec<_>>>())?;

		let mut items = (item_metadata_offsets
			.iter()
			.map(|&(section, metadata_offset)| -> Result<_> {
				let _loc = Location::new(format!("section {}", section));

				reader.seek(io::SeekFrom::Start(metadata_offset as u64))?;
				let name = read_pascal_str_u8(&mut *reader)?;
				let data_address = reader.read_u32::<LittleEndian>()?;
				Ok((section, name, data_address))
			})
			.collect::<Result<Vec<_>>>())?;

		// GFModel files don't contain end offsets, so guess them based on the
		// beginnings of the next entries.
		items.sort_by_key(|&(_, _, offset)| offset);

		let mut entries = Vec::with_capacity(items.len());
		let mut sections = Vec::with_capacity(items.len());

		for (i, &(section, ref name, begin_offset)) in items.iter().enumerate() {
			let _loc = Location::new(format!("section {} ({:?})", section, name));

			let end_offset = if i == items.len() - 1 {
				reader.seek(io::SeekFrom::End(0))?
			} else {
				items[i + 1].2 as u64
			};

			let section_name = match section {
				0 => "0_models".to_string(),
				1 => "1_textures".to_string(),
				_ => section.to_string(),
			};

			let path = containers::EntryPath::new(vec![section_name, name.clone()]).unwrap();
			let section = RawSection {
				offset: begin_offset as u64,
				size: (end_offset - begin_offset as u64),
			};
			let entry = containers::Entry {
				index: i,
				path,
				size: section.size,
				format_override: None,
			};

			sections.push(section);
			entries.push(entry);
		}

		Ok(Self {
			reader: reader_rc_cloned,
			entries,
			sections,
		})
	}
}
impl<Reader: io::Read + io::Seek> containers::Container for GfModelContainer<Reader> {
	fn metadata(&self) -> String {
		"Type: GfModel Container".to_string()
	}

	fn entries(&self) -> &[containers::Entry] {
		self.entries.as_slice()
	}

	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		let mut reader = self.reader.borrow_mut();
		Ok(self.sections[entry_index].read(&mut *reader, offset, max_size)?)
	}
}
