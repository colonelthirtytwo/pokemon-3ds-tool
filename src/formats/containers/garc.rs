//! GARC container format parser.

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::{
	cell::RefCell,
	io::{
		self,
		Read,
		Seek,
	},
	rc::Rc,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::containers::{
		self,
		common::LzssSection,
	},
};

/// GARC container magic sequence, at the start of the file
pub const MAGIC: [u8; 4] = *b"CRAG";

/// GARC container format.
///
/// Usually the top-level container, one of the `a/#/#/#` files on the ROM.
pub struct GarcContainer<Reader: Read + Seek> {
	reader: Rc<RefCell<Reader>>,
	entries: Vec<containers::Entry>,
	sections: Vec<LzssSection>,
	version: u16,
}
impl<Reader: Read + Seek> GarcContainer<Reader> {
	/// Parses a GARC container.
	pub fn new(reader_rc: Rc<RefCell<Reader>>) -> Result<Self> {
		let reader_rc_cloned = Rc::clone(&reader_rc);
		let mut reader = reader_rc.borrow_mut();

		let mut magic: [u8; 4] = [0, 0, 0, 0];
		reader.read_exact(&mut magic)?;
		if &magic != &MAGIC {
			return Err(Error::parsing("incorrect magic number"));
		}
		let header_len = reader.read_u32::<LittleEndian>()?;
		let _endian = reader.read_u16::<LittleEndian>()?; // TODO: ignored in ohana
		let version = reader.read_u16::<LittleEndian>()?;
		let section_count = reader.read_u32::<LittleEndian>()?;
		if section_count != 4 {
			return Err(Error::parsing(format!(
				"unsupported section count: {}",
				section_count
			)));
		}
		let data_offset = reader.read_u32::<LittleEndian>()?;
		let _decompressed_length = reader.read_u32::<LittleEndian>()?;

		let (_content_largest_padded, _content_largest_unpadded, _content_pad_to_nearest) =
			match version {
				0x400 => {
					let content_largest_unpadded = reader.read_u32::<LittleEndian>()?;
					(None, content_largest_unpadded, 4)
				}
				0x600 => {
					let content_largest_padded = reader.read_u32::<LittleEndian>()?;
					let content_largest_unpadded = reader.read_u32::<LittleEndian>()?;
					let content_pad_to_nearest = reader.read_u32::<LittleEndian>()?;
					(
						Some(content_largest_padded),
						content_largest_unpadded,
						content_pad_to_nearest,
					)
				}
				_ => {
					return Err(Error::parsing(format!(
						"unrecognized GARC version: {:#03x}",
						version
					)));
				}
			};

		// FATO (File Allocation Table Offsets)
		reader.seek(io::SeekFrom::Start(header_len as u64))?;
		let mut _fato_magic: [u8; 4] = [0, 0, 0, 0];
		reader.read_exact(&mut _fato_magic)?; // TODO: what is the magic sequence?
		let fato_length = reader.read_u32::<LittleEndian>()?;
		let fato_num_entries = reader.read_u16::<LittleEndian>()?;
		reader.read_u16::<LittleEndian>()?; // padding

		let entry_offsets: Vec<u32> = ((0..fato_num_entries)
			.map(|_| reader.read_u32::<LittleEndian>())
			.collect::<io::Result<Vec<_>>>())?;

		// FATB (File Allocation Table Bits)
		reader.seek(io::SeekFrom::Start(header_len as u64 + fato_length as u64))?;
		let mut _fatb_magic: [u8; 4] = [0, 0, 0, 0];
		reader.read_exact(&mut _fatb_magic)?; // TODO: what is the magic sequence?
		let _fatb_header_len = reader.read_u32::<LittleEndian>()?;
		let _fatb_file_count = reader.read_u32::<LittleEndian>()?;

		// 4*3 = number of bytes in FATB above
		let content_start = header_len as u64 + fato_length as u64 + 4 * 3;

		let mut sections = Vec::with_capacity(fato_num_entries as usize);
		let mut entries = Vec::with_capacity(fato_num_entries as usize);

		for entry_idx in 0..fato_num_entries {
			let _loc = Location::new(format!("entry {}", entry_idx));

			reader.seek(io::SeekFrom::Start(
				content_start + entry_offsets[entry_idx as usize] as u64,
			))?;

			let flags = reader.read_u32::<LittleEndian>()?;
			let single_file = flags.count_ones() == 1;
			for subentry_idx in 0..32 {
				let _loc = Location::new(format!("subentry {}", subentry_idx));

				let entry_exists = (flags & (1 << subentry_idx)) != 0;
				if !entry_exists {
					continue;
				}

				let start = with_loc!("subentry start offset", reader.read_u32::<LittleEndian>())?;
				let _end = with_loc!("subentry end offset", reader.read_u32::<LittleEndian>())?;
				let length = with_loc!("subentry length", reader.read_u32::<LittleEndian>())?;

				let path = if single_file {
					vec![entry_idx.to_string()]
				} else {
					vec![entry_idx.to_string(), subentry_idx.to_string()]
				};

				let section = LzssSection::identify(
					&mut *reader,
					data_offset as u64 + start as u64,
					length as u64,
				)?;

				let entry = containers::Entry {
					index: entries.len(),
					path: containers::EntryPath::new(path).unwrap(),
					size: section.decompressed_size,
					format_override: None,
				};
				sections.push(section);
				entries.push(entry);
			}
		}

		Ok(Self {
			reader: reader_rc_cloned,
			entries,
			sections,
			version,
		})
	}

	/// Gets the GARC version.
	///
	/// The only two known versions are 0x0400 and 0x0600.
	pub fn version(&self) -> u16 {
		self.version
	}
}
impl<Reader: Read + Seek> containers::Container for GarcContainer<Reader> {
	fn metadata(&self) -> String {
		format!("Type: GARC\nVersion: 0x{:x}", self.version())
	}

	fn entries(&self) -> &[containers::Entry] {
		self.entries.as_slice()
	}

	fn read_entry(
		&self,
		entry_index: usize,
		offset: u64,
		max_size: Option<usize>,
	) -> Result<Vec<u8>> {
		let mut reader = self.reader.borrow_mut();
		self.sections[entry_index].read(&mut *reader, offset, max_size)
	}
}
