//! GFMotion file format.

use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::io::{
	self,
	Read,
	Seek,
};

use crate::{
	error::{
		Error,
		Location,
		Result,
	},
	formats::animations::geom::*,
	ioutils::read_pascal_str_u8,
};

/// Reads a GFMotion file, which can contain multiple animations.
///
/// TODO: Move this to be a container, if it's actually used anywhere
/*pub fn read_gfmotion<Reader: Read+Seek>(reader: &mut Reader) -> Result<Vec<Animation>> {
	let begin = reader.seek(io::SeekFrom::Current(0))?;
	let num_anims = reader.read_u32::<LittleEndian>()?;
	let anim_offsets = (0..num_anims).map(|_| {
		reader.read_u32::<LittleEndian>()
	}).collect::<io::Result<Vec<_>>>()?;

	let animations = anim_offsets.into_iter().enumerate().map(|(i, offset)| {
		reader.seek(io::SeekFrom::Start(begin + offset as u64))?;
		read_gfmotion_anim(reader, i.to_string())
			.map_err(|e| e.at(format!("animation {}", i)))
	}).collect::<Result<Vec<_>>>()?;
	Ok(animations)
}*/

/// Reads an individual GFMotion animation
///
/// These are usually found in the [`PokemonContainer`] files.
///
/// [`PokemonContainer`]: ../../containers/pokemon_container/struct.PokemonContainer.html
pub fn read_gfmotion_anim<Reader: Read + Seek>(
	reader: &mut Reader,
	name: String,
) -> Result<Animation> {
	let mut magic = [0, 0, 0, 0];
	reader.read_exact(&mut magic)?;
	if magic != [0x00, 0x00, 0x06, 0x00] {
		return Err(Error::parsing(format!(
			"Unrecognized gfmotion magic number: {:#?}",
			magic
		)));
	}

	let unknown_count = reader.read_u32::<LittleEndian>()?;
	reader.seek(io::SeekFrom::Current(0x24 + unknown_count as i64 * 0xc))?;

	let num_bones = reader.read_u32::<LittleEndian>()?;
	let bone_names_len = reader.read_u32::<LittleEndian>()?;
	let bone_names_start = reader.seek(io::SeekFrom::Current(0))?;

	let bone_names = ((0..num_bones)
		.map(|_| read_pascal_str_u8(reader))
		.collect::<io::Result<Vec<_>>>())?;

	reader.seek(io::SeekFrom::Start(
		bone_names_start + bone_names_len as u64,
	))?;

	let mut anim_bones = vec![];

	for (i, bone_name) in bone_names.into_iter().enumerate() {
		let _loc = Location::new(format!("bone {:?} ({})", bone_name, i));

		let flags = with_loc!("bone flags", reader.read_u32::<LittleEndian>())?;
		let _frame_length = with_loc!("bone frame length", reader.read_u32::<LittleEndian>())?;

		let is_axis_angle = (flags >> 31) == 0;

		let axises = ((0..9)
			.map(|axis| -> Result<_> {
				let _loc = Location::new(format!("axis {}", axis));
				read_axis(reader, axis, flags, is_axis_angle)
			})
			.collect::<Result<Vec<_>>>())?;

		let mut iter = axises.into_iter();

		anim_bones.push(AnimBone::new(
			bone_name,
			[
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
			],
			if is_axis_angle {
				RotationMode::AxisAngle
			} else {
				RotationMode::Euler
			},
		)?);
	}
	Ok(Animation {
		name: name,
		bones: anim_bones,
	})
}

/// Reads an individual animation axis
fn read_axis<Reader: Read + Seek>(
	reader: &mut Reader,
	axis: u32,
	flags: u32,
	is_axis_angle: bool,
) -> Result<AnimAxis> {
	let axis_exists = ((flags >> (2 + axis * 3)) & 1) != 0;
	let axis_const = ((flags >> (axis * 3)) & 3) == 3;
	let scale = if axis > 2 && axis < 6 && is_axis_angle {
		2.0
	} else {
		1.0
	};

	let mut keyframes_for_axis = vec![];

	if axis_const {
		let value = with_loc!("const value", reader.read_f32::<LittleEndian>())?;
		let keyframe = Keyframe {
			frame: 0,
			value: value * scale,
			interpolation: Interpolation::Constant,
		};
		keyframes_for_axis.push(keyframe);
	}

	if !axis_exists {
		return AnimAxis::new(keyframes_for_axis);
	}

	let mut num_keyframes = with_loc!("num keyframes", reader.read_u32::<LittleEndian>())?;
	let keyframes = ((0..num_keyframes)
		.map(|_| reader.read_u8())
		.collect::<io::Result<Vec<_>>>())?;
	while num_keyframes % 4 != 0 {
		with_loc!("padding", reader.read_u8())?;
		num_keyframes += 1;
	}

	let value_scale = with_loc!("value scale", reader.read_f32::<LittleEndian>())?;
	let value_offset = with_loc!("value offset", reader.read_f32::<LittleEndian>())?;
	let slope_scale = with_loc!("slope scale", reader.read_f32::<LittleEndian>())?;
	let slope_offset = with_loc!("slope offset", reader.read_f32::<LittleEndian>())?;

	for (i, frame) in keyframes.into_iter().enumerate() {
		let qvalue = with_loc!("q value", reader.read_u16::<LittleEndian>())?;
		let qslope = with_loc!("q slope", reader.read_u16::<LittleEndian>())?;

		let value = value_offset + (qvalue as f32 / 0xffff as f32) * value_scale;
		let slope = slope_offset + (qslope as f32 / 0xffff as f32) * slope_scale;

		// The file specifies the slope for either side of the keyframe,
		// which isn't convenient for mixed-interpolation keyframes that
		// we use. So the `next_frame_in_slope` field is set later.
		if i != 0 {
			match keyframes_for_axis[i - 1].interpolation {
				Interpolation::Hermite {
					ref mut next_frame_in_slope,
					..
				} => {
					*next_frame_in_slope = slope;
				}
				_ => {}
			}
		}

		let keyframe = Keyframe {
			frame: frame as u32,
			value: value * scale,
			interpolation: Interpolation::Hermite {
				out_slope: slope,
				next_frame_in_slope: 0., // placeholder, next frame will fill this
			},
		};
		keyframes_for_axis.push(keyframe);
	}

	return AnimAxis::new(keyframes_for_axis);
}
