//! Animation file formats

pub mod gfmotion;

/// Animation common types
pub mod geom {
	use crate::error::{
		Error,
		Result,
	};
	use nalgebra::{
		base::Vector3,
		geometry::UnitQuaternion,
	};

	pub const ANIM_NUM_AXISES: usize = 9;

	/// Skeletal animation
	#[derive(Debug)]
	pub struct Animation {
		/// Name of the animation from the file
		pub name: String,
		/// List of animation bones.
		///
		/// This may be missing a few bones from the corresponding model, if
		/// those bones aren't animated.
		pub bones: Vec<AnimBone>,
	}
	impl Animation {
		/// Gets the frame number of the last keyframe in this bone's animation
		pub fn last_frame(&self) -> u32 {
			self.bones
				.iter()
				.map(|bone| bone.last_frame())
				.max()
				.unwrap_or(0)
		}
	}

	/// Bone animation data
	#[derive(Debug)]
	pub struct AnimBone {
		name: String,
		axises: [AnimAxis; ANIM_NUM_AXISES],
		rotation_mode: RotationMode,
		last_frame: u32,
	}
	impl AnimBone {
		/// Creates a new animated bone
		///
		/// # Parameters
		///
		/// * `name`: Bone name, corresponding to names of bones in a `Model`.
		/// * `axises`: Animations for each orientation axis. Order is: Scale.X,Y,Z, Rotation.X,Y,Z, Translation.X,Y,Z.
		/// * `rotation_mode`: How to interpret the rotation values
		pub fn new(
			name: String,
			axises: [AnimAxis; ANIM_NUM_AXISES],
			rotation_mode: RotationMode,
		) -> Result<Self> {
			let last_frame = axises
				.iter()
				.map(|list| list.last_frame())
				.max()
				.unwrap_or(0);

			Ok(Self {
				name,
				axises,
				rotation_mode,
				last_frame,
			})
		}

		/// Gets the name of the bone
		pub fn name(&self) -> &str {
			&self.name
		}

		/// Gets the frame number of the last keyframe in this bone's animation
		pub fn last_frame(&self) -> u32 {
			self.last_frame
		}

		/// Gets the rotation mode
		pub fn rotation_mode(&self) -> RotationMode {
			self.rotation_mode
		}

		/// Checks if the bone has no keyframes
		pub fn is_empty(&self) -> bool {
			(0..ANIM_NUM_AXISES).all(|i| self.axis(i).unwrap().is_empty())
		}

		/// Gets the list of keyframes for an axis
		///
		/// Axises are XYZ for scale, rotation, translation, in that order.
		/// Returns None if axis is out of range.
		pub fn axis(&self, axis: usize) -> Option<&AnimAxis> {
			self.axises.get(axis)
		}

		/// Gets the interpolated value for an axis at a frame.
		///
		/// Returns None if the axis is out of bounds or the axis has no keyframes.
		pub fn value_at(&self, axis: usize, frame: f32) -> Option<f32> {
			let axis = self.axis(axis)?;
			axis.value_at(frame)
		}

		/// Gets the rotation of the bone at a specific frame.
		///
		/// `initial_rotation` is used to fill in missing data in case an axis
		/// doesn't have any keyframes. It should be the model bone's rotation.
		///
		/// This function applies the rotation mode to the animated data and thus
		/// operates on unambiguous Quaternions.
		pub fn rotation_at(
			&self,
			frame: f32,
			initial_rotation: &UnitQuaternion<f32>,
		) -> UnitQuaternion<f32> {
			let initial_rotation_in_mode = self.rotation_mode.quat_to_vec(initial_rotation);
			let interpolated_vec = Vector3::new(
				self.value_at(3, frame)
					.unwrap_or(initial_rotation_in_mode[0]),
				self.value_at(4, frame)
					.unwrap_or(initial_rotation_in_mode[1]),
				self.value_at(5, frame)
					.unwrap_or(initial_rotation_in_mode[2]),
			);
			self.rotation_mode.vec_to_quat(&interpolated_vec)
		}

		/// Gets the scale of the bone at a specific frame.
		///
		/// `initial_scale` is used to fill in missing data in case an axis
		/// doesn't have any keyframes. It should be the model bone's scale.
		pub fn scale_at(&self, frame: f32, initial_scale: &Vector3<f32>) -> Vector3<f32> {
			Vector3::new(
				self.value_at(0, frame).unwrap_or(initial_scale[0]),
				self.value_at(1, frame).unwrap_or(initial_scale[1]),
				self.value_at(2, frame).unwrap_or(initial_scale[2]),
			)
		}

		/// Gets the position of the bone at a specific frame.
		///
		/// `initial_pos` is used to fill in missing data in case an axis
		/// doesn't have any keyframes. It should be the model bone's position.
		pub fn position_at(&self, frame: f32, initial_pos: &Vector3<f32>) -> Vector3<f32> {
			Vector3::new(
				self.value_at(6, frame).unwrap_or(initial_pos[0]),
				self.value_at(7, frame).unwrap_or(initial_pos[1]),
				self.value_at(8, frame).unwrap_or(initial_pos[2]),
			)
		}

		/// Gets a pretty name for an axis.
		pub fn axis_name(index: usize) -> Option<&'static str> {
			const AXIS_NAMES: &'static [&'static str] = &[
				"scale.x",
				"scale.y",
				"scale.z",
				"rotation.x",
				"rotation.y",
				"rotation.z",
				"translation.x",
				"translation.y",
				"translation.z",
			];
			AXIS_NAMES.get(index).map(|v| *v)
		}
	}

	/// List of keyframes, providing animation for a single `f32` value.
	#[derive(Debug, Clone)]
	pub struct AnimAxis {
		keyframes: Vec<Keyframe>,
	}
	impl AnimAxis {
		/// Creates a new `AnimAxis` from a list of keyframes.
		///
		/// `new` will sort the list by the keyframe's frame number.
		///
		/// Returns an error if two or more keyframes share a frame number.
		pub fn new(mut keyframes: Vec<Keyframe>) -> Result<Self> {
			keyframes.sort_unstable_by_key(|kf| kf.frame);
			if let Some(duplicate) = keyframes
				.windows(2)
				.position(|slice| slice[0].frame == slice[1].frame)
			{
				return Err(with_loc!(
					format!("keyframe {}", duplicate),
					Error::parsing("duplicate frame number in keyframe")
				));
			}
			Ok(Self { keyframes })
		}

		/// Gets the keyframes, as a slice
		pub fn keyframes(&self) -> &[Keyframe] {
			&self.keyframes
		}

		/// Checks if there are any keyframes in this list
		pub fn is_empty(&self) -> bool {
			self.keyframes.is_empty()
		}

		/// Gets the frame number of the last keyframe in this animation
		pub fn last_frame(&self) -> u32 {
			self.keyframes.last().map(|kf| kf.frame).unwrap_or(0)
		}

		/// Gets the interpolated value at a frame time.
		///
		/// Floating point frame times are supported, to help with resampling.
		///
		/// Frame values before the first keyframe or after the last keyframe
		/// use the closest keyframe value without interpolation.
		///
		/// Returns `None` if there are no keyframes in this list.
		pub fn value_at(&self, frame: f32) -> Option<f32> {
			let frame_u32 = if frame < 0. { 0 } else { frame as u32 };
			let frame_index = match self
				.keyframes
				.binary_search_by_key(&frame_u32, |kf| kf.frame)
			{
				Ok(i) | Err(i) => i,
			};
			let start_frame = if frame_index == 0 {
				None
			} else {
				self.keyframes.get(frame_index - 1)
			};
			let end_frame = self.keyframes.get(frame_index);
			match (start_frame, end_frame) {
				(None, None) => None,
				(Some(frame), None) => Some(frame.value),
				(None, Some(frame)) => Some(frame.value),
				(Some(start), Some(end)) => {
					assert!(end.frame > start.frame);
					let position = frame - start.frame as f32;
					let length = (end.frame - start.frame) as f32;
					Some(
						start
							.interpolation
							.interpolate(start.value, end.value, position, length),
					)
				}
			}
		}
	}

	/// Individual keyframe
	#[derive(Debug, Clone)]
	pub struct Keyframe {
		/// Keyframe frame
		pub frame: u32,
		/// Keyframe value
		pub value: f32,
		/// Interpolation mode for values between this keyframe and the next one.
		/// Ignored for the last keyframe.
		pub interpolation: Interpolation,
	}

	/// Interpolation mode to use to interpolate values between the current
	/// frame and the next frame.
	#[derive(Debug, Clone)]
	pub enum Interpolation {
		/// No interpolation; use the current value until the next frame
		Constant,
		/// Linear interpolation between the left and right values.
		Linear,
		/// Hermite interpolation between the left and right values, parameterized
		/// by slopes.
		Hermite {
			out_slope: f32,
			next_frame_in_slope: f32,
		},
	}
	impl Interpolation {
		/// Applies the interpolation
		///
		/// # Parameters
		/// * `left`: Starting value (the left-side value on a timeline)
		/// * `right`: Ending value (the right-side value on a timeline)
		/// * `frame` and `out_of`: Specifies the interpolation point.
		///
		pub fn interpolate(&self, left: f32, right: f32, frame: f32, out_of: f32) -> f32 {
			assert!(out_of != 0., "Cannot interpolate using zero timespan");
			match self {
				Interpolation::Constant => left,
				Interpolation::Linear => {
					let fraction = frame / out_of;
					left * (1. - fraction) + right * fraction
				}
				Interpolation::Hermite {
					out_slope,
					next_frame_in_slope,
				} => {
					let t = frame / out_of;
					let t1 = t - 1.;
					(left + ((((left - right) * ((2. * t) - 3.)) * t) * t))
						+ ((frame * t1) * ((t1 * out_slope) + (t * next_frame_in_slope)))
				}
			}
		}
	}

	/// What the rotation values indicate.
	#[derive(Debug, PartialEq, Eq, Clone, Copy)]
	pub enum RotationMode {
		/// Direction = rotation axis, magnitude = rotation amount.
		AxisAngle,
		/// Euler rotation in XYZ order, radians.
		Euler,
	}
	impl RotationMode {
		/// Converts a quaternion to a vector using the specified rotation mode.
		pub fn quat_to_vec(&self, rotation: &UnitQuaternion<f32>) -> Vector3<f32> {
			match self {
				RotationMode::AxisAngle => rotation.scaled_axis(),
				RotationMode::Euler => {
					let angles = rotation.euler_angles();
					Vector3::new(angles.0, angles.1, angles.2)
				}
			}
		}

		/// Converts a vector to a quaternion using the specified rotation mode.
		pub fn vec_to_quat(&self, rotation: &Vector3<f32>) -> UnitQuaternion<f32> {
			match self {
				RotationMode::AxisAngle => UnitQuaternion::from_scaled_axis(rotation.clone()),
				RotationMode::Euler => {
					UnitQuaternion::from_euler_angles(rotation[0], rotation[1], rotation[2])
				}
			}
		}
	}

	#[cfg(test)]
	mod tests {
		use super::*;

		#[test]
		fn anim_axis() {
			let anim = AnimAxis::new(vec![
				Keyframe {
					frame: 0,
					value: 10.,
					interpolation: Interpolation::Linear,
				},
				Keyframe {
					frame: 10,
					value: 20.,
					interpolation: Interpolation::Linear,
				},
				Keyframe {
					frame: 20,
					value: 28.,
					interpolation: Interpolation::Linear,
				},
			])
			.unwrap();

			assert_eq!(anim.value_at(-1.).unwrap(), 10.);
			assert_eq!(anim.value_at(20.).unwrap(), 28.);
			assert_relative_eq!(anim.value_at(5.).unwrap(), 15.);
			assert_relative_eq!(anim.value_at(15.).unwrap(), 24.);
		}
	}
}
