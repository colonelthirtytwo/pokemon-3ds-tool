use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use nalgebra::{
	base::{
		Matrix4,
		Vector2,
		Vector3,
		Vector4,
	},
	geometry::UnitQuaternion,
};
use num_traits::Zero;
use std::{
	collections::{
		BTreeMap,
		HashMap,
	},
	io::{
		self,
		Read,
		Seek,
	},
};

use crate::{
	error::{
		Error,
		Location,
		Push,
		Result,
	},
	formats::{
		misc::pica200::{
			self,
			PicaBuffer,
		},
		models::geom::*,
	},
	ioutils::read_pascal_str_u8,
	nonnan::NonNaN,
};

fn wrapping_mode_from_u32(v: u32) -> Option<WrappingMode> {
	match v {
		0 => Some(WrappingMode::ClampToEdge),
		1 => Some(WrappingMode::ClampToBorder),
		2 => Some(WrappingMode::Repeat),
		3 => Some(WrappingMode::MirroredRepeat),
		_ => None,
	}
}

#[derive(Debug, Default)]
struct GfModelAttributes {
	pub faces: Vec<Face>,

	pub positions: Vec<Vector3<f32>>,
	pub normals: Vec<Vector3<f32>>,
	pub colors: Vec<Vector4<f32>>,
	pub texture_coords_0: Vec<Vector2<f32>>,
	pub texture_coords_1: Vec<Vector2<f32>>,
	pub texture_coords_2: Vec<Vector2<f32>>,
	pub bone_weights: Vec<BTreeMap<usize, NonNaN>>,
}

pub fn read_gfmodel<Reader: Read + Seek, W: Push<Error>>(
	reader: &mut Reader,
	warnings: &mut W,
) -> Result<Model> {
	let start = reader.seek(io::SeekFrom::Current(0))?;
	reader.seek(io::SeekFrom::Current(0x10))?;

	let mut magic: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	with_loc!("magic number", reader.read_exact(&mut magic))?;
	if magic != *b"gfmodel\0" {
		return Err(Error::parsing(format!(
			"unrecognized gfmodel magic: {:x?}",
			magic
		)));
	}

	let length = with_loc!("length", reader.read_u32::<LittleEndian>())?;
	with_loc!("padding", reader.read_u32::<LittleEndian>())?; // Padding?

	let _effect_names = with_loc!("effect_names", read_gfmodel_str_table(reader))?;
	let _texture_names = with_loc!("texture names", read_gfmodel_str_table(reader))?;
	let material_names = with_loc!("material names", read_gfmodel_str_table(reader))?;
	let mesh_names = with_loc!("mesh names", read_gfmodel_str_table(reader))?;

	let _float1 = with_loc!(
		"unused float4",
		(0..4)
			.map(|_| reader.read_f32::<LittleEndian>())
			.collect::<io::Result<Vec<_>>>()
	)?;
	let _float2 = with_loc!(
		"unused float4",
		(0..4)
			.map(|_| reader.read_f32::<LittleEndian>())
			.collect::<io::Result<Vec<_>>>()
	)?;

	let transform = with_loc!("model transformation", read_mat4(reader))?;

	let unknown_data_len = reader.read_u32::<LittleEndian>()?;
	let unknown_data_start = reader.read_u32::<LittleEndian>()?;
	reader.read_u32::<LittleEndian>()?;
	reader.read_u32::<LittleEndian>()?;
	reader.seek(io::SeekFrom::Current(
		unknown_data_start as i64 + unknown_data_len as i64,
	))?;

	// bones
	let num_bones = with_loc!("number of bones", reader.read_u32::<LittleEndian>())?;
	reader.seek(io::SeekFrom::Current(0xc))?;

	let bones = (0..num_bones)
		.map(|i| with_loc!(format!("bone {}", i), read_gfmodel_bone(reader)))
		.collect::<Result<Vec<Bone>>>()?;
	let bones = BoneList::new(bones)?;

	// Materials
	reader.seek(io::SeekFrom::Start(start + length as u64 + 0x20))?;
	let mut materials = vec![];
	let mut submesh_material_bindings = HashMap::new();
	for (i, name) in material_names.into_iter().enumerate() {
		let _loc = Location::new(format!("material {:?} ({})", name, i));

		let (material, submesh_name) = read_gfmodel_material(reader, &name, warnings)?;
		materials.push(material);
		submesh_material_bindings.insert(submesh_name, materials.len() - 1);
	}

	// Meshes
	let mut attrs = GfModelAttributes::default();
	for (i, name) in mesh_names.into_iter().enumerate() {
		let _loc = Location::new(format!("mesh {:?} ({})", name, i));
		read_gfmodel_mesh(reader, &mut attrs, &submesh_material_bindings, warnings)?;
	}

	let mut attrs_vec = vec![
		VertexAttribute::Positions(attrs.positions),
		VertexAttribute::Normals(attrs.normals),
	];
	if attrs
		.colors
		.iter()
		.any(|color| *color != Vector4::new(1., 1., 1., 1.))
	{
		attrs_vec.push(VertexAttribute::Colors(attrs.colors));
	}
	if attrs.texture_coords_0.iter().any(|v| *v != Vector2::zero()) {
		attrs_vec.push(VertexAttribute::TextureCoords(attrs.texture_coords_0));
	}
	if attrs.texture_coords_1.iter().any(|v| *v != Vector2::zero()) {
		attrs_vec.push(VertexAttribute::TextureCoords(attrs.texture_coords_1));
	}
	if attrs.texture_coords_2.iter().any(|v| *v != Vector2::zero()) {
		attrs_vec.push(VertexAttribute::TextureCoords(attrs.texture_coords_2));
	}
	if attrs
		.bone_weights
		.iter()
		.flat_map(|v| v.iter())
		.any(|(_bone_id, weight)| *weight != 0.)
	{
		attrs_vec.push(VertexAttribute::BoneWeights(attrs.bone_weights));
	}

	let mesh = Mesh::new(attrs.faces, attrs_vec)?;

	let model = Model::new(mesh, bones, materials, transform)?;

	return Ok(model);
}

fn read_gfmodel_bone<Reader: Read>(reader: &mut Reader) -> Result<Bone> {
	let name = with_loc!("name", read_pascal_str_u8(reader))?;
	let parent = with_loc!("parent name", read_pascal_str_u8(reader))?;
	let _bytes = reader.read_u8()?;
	let scale = with_loc!("scale", read_vec3(reader))?;
	let rotation = with_loc!("rotation", read_vec3(reader))?; // Euler XYZ in radians
	let translation = with_loc!("translation", read_vec3(reader))?;
	Ok(Bone {
		name,
		parent: if parent != "" { Some(parent) } else { None },
		scale,
		rotation: UnitQuaternion::from_euler_angles(rotation[0], rotation[1], rotation[2]),
		translation,
	})
}

fn read_gfmodel_material<Reader: Read + Seek, W: Push<Error>>(
	reader: &mut Reader,
	name: &str,
	warnings: &mut W,
) -> Result<(Material, String)> {
	let mut magic: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	with_loc!("magic", reader.read_exact(&mut magic))?;
	if magic != *b"material" {
		return Err(Error::parsing(format!(
			"unrecognized gfmaterial magic: {:x?}",
			magic
		)));
	}

	let mtl_length = with_loc!("material lengths", reader.read_u32::<LittleEndian>())?;
	reader.read_u32::<LittleEndian>()?; // What is this?

	let mtl_start = with_loc!("material start", reader.seek(io::SeekFrom::Current(0)))?;

	let names = (0..4)
		.map(|i| -> Result<_> {
			let _loc = Location::new(format!("model name {}", i));
			reader.read_u32::<LittleEndian>()?; // What is this? Hash?
			Ok(read_pascal_str_u8(reader)?)
		})
		.collect::<Result<Vec<String>>>()?;
	let model_name = names.into_iter().nth(0).unwrap();

	reader.seek(io::SeekFrom::Current(0xac))?;

	let mut textures = Vec::with_capacity(3);
	for i in 0..3 {
		with_loc!(format!("texture {}", i), reader.read_u32::<LittleEndian>())?; // What is this? Hash?
		let tex_name = with_loc!(format!("texture {} name", i), read_pascal_str_u8(reader))?;
		if tex_name == "" {
			break;
		}

		let _loc = Location::new(format!("material texture {:?} ({})", tex_name, i));

		let unit_idx = with_loc!("texture index", reader.read_u16::<LittleEndian>())?;
		if unit_idx != i {
			warnings.push(Error::parsing(format!(
				"Material texture slot doesn't match: expected {}, got {}. Ignoring issue.",
				i, unit_idx
			)));
		}

		let scale_u = with_loc!("U scale", reader.read_f32::<LittleEndian>())?;
		let scale_v = with_loc!("V scale", reader.read_f32::<LittleEndian>())?;
		let rotation = with_loc!("rotation", reader.read_f32::<LittleEndian>())?;
		let translate_u = with_loc!("U translation", reader.read_f32::<LittleEndian>())?;
		let translate_v = with_loc!("V translation", reader.read_f32::<LittleEndian>())?;
		let wrap_mode_u_raw = with_loc!("U wrap mode", reader.read_u32::<LittleEndian>())?;
		let wrap_mode_u = wrapping_mode_from_u32(wrap_mode_u_raw).unwrap_or_else(|| {
			warnings.push(Error::parsing(format!(
				"Unrecognized U wrapping mode {:?}, defaulting to Repeat",
				wrap_mode_u_raw
			)));
			WrappingMode::Repeat
		});
		let wrap_mode_v_raw = reader.read_u32::<LittleEndian>()?;
		let wrap_mode_v = wrapping_mode_from_u32(wrap_mode_v_raw).unwrap_or_else(|| {
			warnings.push(Error::parsing(format!(
				"Unrecognized V wrapping mode {:?}, defaulting to Repeat",
				wrap_mode_v_raw
			)));
			WrappingMode::Repeat
		});

		reader.read_u32::<LittleEndian>()?; // Unknown? Possibly texture type (0 = diffuse, 1 = normal)
		reader.read_u32::<LittleEndian>()?; // Unknown? 2 for diffuse, 5 for normal
		reader.read_u32::<LittleEndian>()?; // Unknown? Always zero

		textures.push(MaterialTexture {
			name: tex_name,
			scale: Vector2::new(scale_u, scale_v),
			rotation,
			translation: Vector2::new(translate_u, translate_v),
			wrap_mode: [wrap_mode_u, wrap_mode_v],
		});
	}

	reader.seek(io::SeekFrom::Start(mtl_start + mtl_length as u64))?;
	return Ok((
		Material {
			name: name.to_string(),
			textures: textures,
		},
		model_name,
	));
}

fn read_gfmodel_mesh<Reader: Read + Seek, W: Push<Error>>(
	reader: &mut Reader,
	out_attrs: &mut GfModelAttributes,
	submesh_material_bindings: &HashMap<String, usize>,
	warnings: &mut W,
) -> Result<()> {
	let mut magic: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	with_loc!("magic", reader.read_exact(&mut magic))?;
	if magic != *b"mesh\0\0\0\0" {
		return Err(Error::parsing(format!(
			"unrecognized gfmodel magic: {:x?}",
			magic
		)));
	}

	let mesh_length = reader.read_u32::<LittleEndian>()?;
	reader.read_u32::<LittleEndian>()?; // What is this?

	let mesh_start = reader.seek(io::SeekFrom::Current(0))?;
	// Mesh name and other stuff goes here. Should we parse it?
	reader.seek(io::SeekFrom::Current(0x80))?;

	let sub_mesh_infos = read_gfmodel_sub_mesh_info(reader)?;

	for (i, sub_mesh_info) in sub_mesh_infos.iter().enumerate() {
		let _loc = Location::new(format!("submesh {:?} ({})", sub_mesh_info.name, i));
		let material_index = (submesh_material_bindings
			.get(&sub_mesh_info.name)
			.cloned()
			.ok_or_else(|| Error::parsing("no material entry for submesh")))?;

		read_gfmodel_sub_mesh(reader, out_attrs, sub_mesh_info, material_index, warnings)?;
	}

	reader.seek(io::SeekFrom::Start(mesh_start + mesh_length as u64))?;

	Ok(())
}

fn read_gfmodel_sub_mesh<Reader: Read + Seek, W: Push<Error>>(
	reader: &mut Reader,
	out_attrs: &mut GfModelAttributes,
	sub_mesh_info: &SubMeshInfo,
	material: usize,
	warnings: &mut W,
) -> Result<()> {
	let start_index = out_attrs.positions.len();

	// Read vertices
	let vtx_cmd_buffer = &sub_mesh_info.buffers[0];
	let vertex_stride = vtx_cmd_buffer.vertex_buffer_vertex_stride(0);
	let vertex_attributes = vtx_cmd_buffer
		.vertex_buffer_attributes(0)
		.into_iter()
		.map(|attr| {
			if attr.value_type != pica200::VertexAttributeValueType::BoneIndex {
				attr
			} else {
				// bone indexes are always u8, despite what the format says
				pica200::VertexAttribute {
					storage_type: pica200::VertexAttributeStorageType::UnsignedByte,
					..attr
				}
			}
		})
		.collect::<Vec<_>>();

	let num_vertices = sub_mesh_info.vtx_length as usize / vertex_stride;
	let vtx_buffer_extra_bytes = sub_mesh_info.vtx_length as i64 % vertex_stride as i64;

	let vertex_length: i64 = vertex_attributes.iter().map(|a| a.size() as i64).sum();
	let padding_bytes = vertex_stride as i64 - vertex_length;
	if padding_bytes < 0 {
		return Err(Error::parsing(format!(
			"submesh vertex stride ({}) is smaller than the vertex data ({})",
			vertex_stride, vertex_length
		)));
	}

	for vertex_i in 0..num_vertices {
		let _loc = Location::new(format!("vertex {}", vertex_i));

		let mut position = Vector3::zero();
		let mut normal = Vector3::zero();
		let mut color = Vector4::new(1., 1., 1., 1.);
		let mut texture_coords_0 = Vector2::zero();
		let mut texture_coords_1 = Vector2::zero();
		let mut texture_coords_2 = Vector2::zero();
		let mut bone_indices = [0, 0, 0, 0];
		let mut bone_weights = [0.0, 0.0, 0.0, 0.0];
		let mut num_bones: usize = 0;

		for (attribute_i, attribute) in vertex_attributes.iter().enumerate() {
			let _loc = Location::new(format!("attribute {}", attribute_i));

			let value = attribute.read(reader)?;
			match attribute.value_type {
				pica200::VertexAttributeValueType::Position => {
					position = Vector3::new(value[0], value[1], value[2])
				}
				pica200::VertexAttributeValueType::Normal => {
					normal = Vector3::new(value[0], value[1], value[2])
				}
				pica200::VertexAttributeValueType::Color => color = value,
				pica200::VertexAttributeValueType::TextureCoordinate0 => {
					texture_coords_0 = Vector2::new(value[0], value[1])
				}
				pica200::VertexAttributeValueType::TextureCoordinate1 => {
					texture_coords_1 = Vector2::new(value[0], value[1])
				}
				pica200::VertexAttributeValueType::TextureCoordinate2 => {
					texture_coords_2 = Vector2::new(value[0], value[1])
				}
				pica200::VertexAttributeValueType::BoneIndex => {
					num_bones = attribute.num_items as usize;
					for i in 0..num_bones {
						bone_indices[i] = value[i] as usize;
						if bone_weights[i] == 0.0 {
							bone_weights[i] = 1.0;
						}
					}
				}
				pica200::VertexAttributeValueType::BoneWeight => {
					for i in 0..attribute.num_items as usize {
						bone_weights[i] = value[i] / 255.0;
					}
				}
				_ => {}
			}
		}

		if num_bones == 0 && sub_mesh_info.bone_nodes.len() <= 4 {
			num_bones = sub_mesh_info.bone_nodes.len();
			for i in 0..num_bones {
				bone_indices[i] = i;
				if bone_weights[i] == 0.0 {
					bone_weights[i] = 1.0;
				}
			}
		}

		let mut bones = BTreeMap::new();
		for (index, weight) in (0..num_bones).map(|i| (bone_indices[i], bone_weights[i])) {
			if index == 0xff {
				continue;
			}

			let index = *sub_mesh_info.bone_nodes.get(index).ok_or_else(|| {
				Error::parsing(format!(
					"Bone list of length {} has no entry for index {}",
					sub_mesh_info.bone_nodes.len(),
					index
				))
			})?;
			let weight = NonNaN::new(weight)?;
			bones.insert(index as usize, weight);
		}

		//vertex_buffer.push(vertex);
		out_attrs.positions.push(position);
		out_attrs.normals.push(normal);
		out_attrs.colors.push(color);
		out_attrs.texture_coords_0.push(texture_coords_0);
		out_attrs.texture_coords_1.push(texture_coords_1);
		out_attrs.texture_coords_2.push(texture_coords_2);
		out_attrs.bone_weights.push(bones);

		reader.seek(io::SeekFrom::Current(padding_bytes))?;
	}
	reader.seek(io::SeekFrom::Current(vtx_buffer_extra_bytes))?;

	// Read index buffer, use it to index the vertex buffer
	let idx_cmd_buffer = &sub_mesh_info.buffers[2];
	let num_indexes = idx_cmd_buffer.index_buffer_num_vertices();
	let index_buffer_format = idx_cmd_buffer.index_buffer_format();
	let primitive_type = idx_cmd_buffer.index_buffer_primitive_type()?;

	if num_indexes * index_buffer_format.size() as u32 > sub_mesh_info.idx_length {
		return Err(Error::parsing(format!(
			"Index buffer of size {} cannot fit {} indices of type {:?}",
			sub_mesh_info.idx_length, num_indexes, index_buffer_format
		)));
	}

	let idx_buffer_extra_bytes =
		(sub_mesh_info.idx_length as i64) - num_indexes as i64 * index_buffer_format.size() as i64;

	let indices = (0..num_indexes)
		.map(|i| {
			let _loc = Location::new(format!("index {}", i));
			let stored_index = index_buffer_format.read(reader)?;
			let real_index = start_index + stored_index;

			if real_index >= out_attrs.positions.len() {
				return Err(Error::parsing(format!(
					"Index {} exceeds vertex buffer size",
					stored_index
				)));
			}
			Ok(real_index)
		})
		.collect::<Result<Vec<_>>>()?;

	reader.seek(io::SeekFrom::Current(idx_buffer_extra_bytes))?;

	if let Err(err) = primitive_type.check_num_indices(indices.len()) {
		warnings.push(err);
	}

	for chunk in primitive_type.iter_triangles(indices.into_iter()) {
		out_attrs.faces.push(Face {
			indices: [chunk[0], chunk[1], chunk[2]],
			material,
		});
	}

	return Ok(());
}

fn read_vec3<Reader: Read>(reader: &mut Reader) -> Result<Vector3<f32>> {
	Ok(Vector3::new(
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
	))
}

fn read_mat4<Reader: Read>(reader: &mut Reader) -> Result<Matrix4<f32>> {
	Ok(Matrix4::new(
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
		reader.read_f32::<LittleEndian>()?,
	))
}

fn read_gfmodel_str_table<Reader: Read>(reader: &mut Reader) -> Result<Vec<String>> {
	let mut read_buffer: Vec<u8> = vec![0; 0x40];
	let num_strings = reader.read_u32::<LittleEndian>()?;
	let mut strings = Vec::<String>::with_capacity(num_strings as usize);
	for i in 0..num_strings {
		let _loc = Location::new(format!("string table string {}/{}", i, num_strings));

		reader.read_u32::<LittleEndian>()?; // Hash?

		reader.read_exact(&mut read_buffer)?;

		let len = read_buffer
			.iter()
			.position(|v| *v == 0)
			.unwrap_or(read_buffer.len());
		let string = String::from_utf8_lossy(&read_buffer[0..len]).into_owned();
		strings.push(string);
	}
	Ok(strings)
}

struct SubMeshInfo {
	/// Submesh name
	name: String,
	/// first is vertex buffer, third is index buffer. second is unknown?
	buffers: [PicaBuffer; 3],
	/// Mapping of bone indices used in the vertex coordinates to the bone indices
	/// in the mesh.
	bone_nodes: Vec<u8>,
	/// Vertex buffer length in bytes
	vtx_length: u32,
	/// Index buffer length in bytes
	idx_length: u32,
}

fn read_gfmodel_sub_mesh_info<Reader: Read + Seek>(
	reader: &mut Reader,
) -> Result<Vec<SubMeshInfo>> {
	let mut current_cmd_idx = 0;
	let mut total_cmds = 0;
	// Vec of options here since the loop after will need to `take` ownership
	// while iterating with `chunks_mut`
	let mut buffers: Vec<Option<PicaBuffer>> = vec![];

	while (current_cmd_idx + 1 < total_cmds) || current_cmd_idx == 0 {
		let cmd_length = reader.read_u32::<LittleEndian>()?;
		current_cmd_idx = reader.read_u32::<LittleEndian>()?;
		total_cmds = reader.read_u32::<LittleEndian>()?;
		reader.read_u32::<LittleEndian>()?;
		buffers.push(Some(PicaBuffer::read(
			reader,
			cmd_length as usize / 4,
			true,
		)?));
	}

	if total_cmds as usize != buffers.len() {
		return Err(Error::parsing(format!(
			"Mismatch between total_cmds ({}) and number of PICA buffers ({})",
			total_cmds,
			buffers.len()
		)));
	}
	if total_cmds % 3 != 0 {
		return Err(Error::parsing(format!(
			"Number of PICA buffers not a multiple of 3 ({})",
			total_cmds
		)));
	}

	let mut infos = vec![];
	for this_buffers in buffers.chunks_mut(3) {
		reader.read_u32::<LittleEndian>()?; // Maybe hash?

		let name_len = reader.read_u32::<LittleEndian>()?;
		let mut name_data = vec![0; name_len as usize];
		reader.read_exact(&mut name_data)?;

		// null bytes are sometimes included in the name, strip those out
		let real_name_len = name_data
			.iter()
			.position(|v| *v == 0)
			.unwrap_or(name_data.len());
		let name = String::from_utf8_lossy(&name_data[0..real_name_len]).into_owned();

		let node_list_start = reader.seek(io::SeekFrom::Current(0))?;
		let node_list_len = reader.read_u8()?;
		let bone_nodes = ((0..node_list_len)
			.map(|_| reader.read_u8())
			.collect::<io::Result<Vec<_>>>())?;

		reader.seek(io::SeekFrom::Start(node_list_start + 0x20))?;

		let _num_vertices = reader.read_u32::<LittleEndian>()?;
		let _num_indices = reader.read_u32::<LittleEndian>()?;
		let vtx_length = reader.read_u32::<LittleEndian>()?;
		let idx_length = reader.read_u32::<LittleEndian>()?;

		infos.push(SubMeshInfo {
			buffers: [
				this_buffers[0].take().unwrap(),
				this_buffers[1].take().unwrap(),
				this_buffers[2].take().unwrap(),
			],
			bone_nodes,
			vtx_length,
			idx_length,
			name,
		});
	}

	return Ok(infos);
}
