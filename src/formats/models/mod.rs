//! Model format parsers

pub mod gfmodel;

/// Common types for models
pub mod geom {
	use nalgebra::{
		base::{
			Matrix4,
			Vector2,
			Vector3,
			Vector4,
		},
		geometry::UnitQuaternion,
	};
	use std::{
		collections::{
			BTreeMap,
			HashMap,
		},
		ops,
	};

	use crate::{
		error::{
			Error,
			Result,
		},
		nonnan::NonNaN,
	};

	/// A model, a combination of a mesh, bones, and materials.
	#[derive(Debug, Clone)]
	pub struct Model {
		mesh: Mesh,
		bones: BoneList,
		materials: Vec<Material>,
		transform: Matrix4<f32>,
	}
	impl Model {
		pub fn new(
			mesh: Mesh,
			bones: BoneList,
			materials: Vec<Material>,
			transform: Matrix4<f32>,
		) -> Result<Self> {
			if let Some(bad_face) = mesh
				.faces()
				.iter()
				.position(|face| face.material >= materials.len())
			{
				return Err(Error::parsing(format!(
					"Face {} has out-of-bound material index",
					bad_face
				)));
			}

			Ok(Self {
				mesh,
				bones,
				materials,
				transform,
			})
		}

		/// Gets this model's mesh.
		pub fn mesh(&self) -> &Mesh {
			&self.mesh
		}

		/// Gets this model's bones.
		pub fn bones(&self) -> &BoneList {
			&self.bones
		}

		/// Gets this model's materials.
		pub fn materials(&self) -> &[Material] {
			&self.materials
		}

		/// Gets this model's transform.
		pub fn transform(&self) -> &Matrix4<f32> {
			&self.transform
		}
	}

	/// List of bones.
	///
	/// Similar to `Vec<Bone>`, but performs some validation on creation.
	#[derive(Debug, Clone)]
	pub struct BoneList {
		/// Primary list of bones
		bones: Vec<Bone>,
		/// Lookup table of bone names to indices in the `bones` vector.
		name2index: HashMap<String, usize>,
		/// Lookup table of bone indices to the indices of their children.
		/// The `None` key corresponds to root bones.
		children: HashMap<Option<usize>, Vec<usize>>,
	}
	impl BoneList {
		/// Creates a list of bones.
		///
		/// Returns an error if there are two bones with the same name or if a
		/// parent bone doesn't exist. Assumes that there are no circular
		/// relationships; checking that is not yet implemented.
		pub fn new(bones: Vec<Bone>) -> Result<Self> {
			let mut name2index = HashMap::new();
			let mut children = HashMap::new();
			children.insert(None, vec![]);
			for (i, bone) in bones.iter().enumerate() {
				if name2index.contains_key(&bone.name) {
					return Err(Error::parsing(format!("Duplicate bone {:?}", bone.name)));
				}
				name2index.insert(bone.name.clone(), i);
				// Ensure every bone has a vector in the slot, so children_of can
				// assume that every bone has an entry.
				children.insert(Some(i), vec![]);
			}

			for (i, bone) in bones.iter().enumerate() {
				let parent_i = if let Some(ref parent_name) = bone.parent {
					let parent_i = *name2index.get(parent_name).ok_or_else(|| {
						Error::parsing(format!(
							"Bone {:?} has nonexistent parent: {:?}",
							bone.name, parent_name
						))
					})?;
					Some(parent_i)
				} else {
					None
				};
				children.get_mut(&parent_i).unwrap().push(i);
			}

			// TODO: check for cycles

			Ok(Self {
				bones,
				name2index,
				children,
			})
		}

		/// Gets a bone by name.
		///
		/// Returns `None` if there are no bones with that name.
		pub fn by_name(&self, name: &str) -> Option<&Bone> {
			let i = *(self.name2index.get(name)?);
			Some(&self.bones[i])
		}

		/// Gets a bone's parent, if it has one.
		///
		/// # Panics
		/// If the passed-in bone is not from this list (not guarenteed).
		pub fn parent_of(&self, bone: &Bone) -> Option<&Bone> {
			let parent_i = *self
				.name2index
				.get(bone.parent.as_ref()?)
				.expect("Passed bone that was not part of this list");
			Some(&self.bones[parent_i])
		}

		/// Gets a bone's children.
		///
		/// # Panics
		/// If the passed-in bone is not from this list (not guarenteed).
		pub fn children_of<'a>(
			&'a self,
			bone: Option<&'a Bone>,
		) -> impl Iterator<Item = &Bone> + 'a {
			let bone_i: Option<usize> = bone.map(|bone| {
				*self
					.name2index
					.get(&bone.name)
					.expect("Passed bone that was not part of this list")
			});
			let children_indexes: &Vec<usize> = self.children.get(&bone_i).unwrap();
			children_indexes.iter().map(move |i| &self.bones[*i])
		}

		/// Gets a bone's global transform matrix.
		///
		/// This is the combination of the passed-in bone's local transform with
		/// its ancestors.
		///
		/// # Panics
		/// If the passed-in bone is not from this list (not guarenteed).
		pub fn global_xform_matrix(&self, bone: &Bone) -> Matrix4<f32> {
			let this_local_xform = bone.local_xform_matrix();
			if let Some(parent) = self.parent_of(bone) {
				let upper_xform = self.global_xform_matrix(parent);
				upper_xform * this_local_xform
			} else {
				this_local_xform
			}
		}

		/// Gets the inverse of a bone's global transform matrix.
		///
		/// This is the combination of the passed-in bone's inverse local transform
		/// with its ancestors.
		///
		/// # Panics
		/// If the passed-in bone is not from this list (not guarenteed).
		pub fn inv_global_xform_matrix(&self, bone: &Bone) -> Matrix4<f32> {
			let this_local_xform = bone.inv_local_xform_matrix();
			if let Some(parent) = self.parent_of(bone) {
				let upper_xform = self.inv_global_xform_matrix(parent);
				this_local_xform * upper_xform
			} else {
				this_local_xform
			}
		}
	}
	impl ops::Deref for BoneList {
		type Target = [Bone];

		fn deref(&self) -> &Self::Target {
			self.bones.deref()
		}
	}

	/// Model bone
	#[derive(Debug, Clone)]
	pub struct Bone {
		/// Bone name
		pub name: String,
		/// Name of this bone's parent, or None for root bones
		pub parent: Option<String>,
		/// Bone scale
		pub scale: Vector3<f32>,
		/// Bone rotation, relative to parent
		pub rotation: UnitQuaternion<f32>,
		/// Bone translation, relative to parent
		pub translation: Vector3<f32>,
	}
	impl Bone {
		/// Local transform matrix
		pub fn local_xform_matrix(&self) -> Matrix4<f32> {
			let scaling_matrix = Matrix4::new_nonuniform_scaling(&self.scale);
			let rotation_matrix = self.rotation.to_rotation_matrix();
			let translation_matrix = Matrix4::new_translation(&self.translation);
			translation_matrix * rotation_matrix.to_homogeneous() * scaling_matrix
		}

		/// Inverse of local transform matrix
		pub fn inv_local_xform_matrix(&self) -> Matrix4<f32> {
			let scaling_matrix = Matrix4::new_nonuniform_scaling(&self.scale.map(|v| 1.0 / v));
			let rotation_matrix = self.rotation.inverse().to_rotation_matrix();
			let translation_matrix = Matrix4::new_translation(&-self.translation);
			scaling_matrix * rotation_matrix.to_homogeneous() * translation_matrix
		}
	}

	/// Mesh data
	#[derive(Debug, Clone)]
	pub struct Mesh {
		faces: Vec<Face>,
		attributes: Vec<VertexAttribute>,
		num_vertices: usize,
	}
	impl Mesh {
		/// Makes a new mesh
		///
		/// Each attribute array should be the same length, and all indices should
		/// be a valid index into each attribute array.
		pub fn new(faces: Vec<Face>, attributes: Vec<VertexAttribute>) -> Result<Self> {
			if attributes.is_empty() {
				return Err(Error::parsing("No attributes"));
			}
			let num_vertices = attributes[0].len();
			for attr_array in attributes.iter() {
				if attr_array.len() != num_vertices {
					return Err(Error::parsing("Unequal number of vertices in attributes"));
				}
			}

			for (face_i, face) in faces.iter().enumerate() {
				for (vert_i, index) in face.indices.iter().enumerate() {
					if *index >= num_vertices {
						return Err(Error::parsing(format!("Face {} index {} was out of bounds: value is {}, number of vertices is {})", face_i, vert_i, index, num_vertices)));
					}
				}
			}

			Ok(Self {
				faces,
				attributes,
				num_vertices,
			})
		}

		/// Gets the faces of the mesh
		pub fn faces(&self) -> &[Face] {
			&self.faces
		}

		/// Gets an array of all mesh attributes
		pub fn attributes(&self) -> &[VertexAttribute] {
			&self.attributes
		}

		/// Gets the number of vertices in the mesh
		pub fn num_vertices(&self) -> usize {
			self.num_vertices
		}

		/// Gets the position data of the mesh, if it has any.
		pub fn positions(&self) -> Option<&[Vector3<f32>]> {
			self.attributes
				.iter()
				.filter_map(|attr| attr.positions())
				.next()
		}

		/// Gets the normal data of the mesh, if it has any.
		pub fn normals(&self) -> Option<&[Vector3<f32>]> {
			self.attributes
				.iter()
				.filter_map(|attr| attr.normals())
				.next()
		}

		/// Gets the color data of the mesh, if it has any.
		pub fn colors(&self) -> Option<&[Vector4<f32>]> {
			self.attributes
				.iter()
				.filter_map(|attr| attr.colors())
				.next()
		}

		/// Gets the `index`'th texture coordinate data of the mesh.
		///
		/// Takes an index as meshes may have more than one set of texture
		/// coordinates.
		pub fn texture_coords(&self, index: usize) -> Option<&[Vector2<f32>]> {
			self.attributes
				.iter()
				.filter_map(|attr| attr.texture_coords())
				.nth(index)
		}

		/// Gets the bone weights data of the mesh, if it has any.
		pub fn bone_weights(&self) -> Option<&[BTreeMap<usize, NonNaN>]> {
			self.attributes
				.iter()
				.filter_map(|attr| attr.bone_weights())
				.next()
		}

		/// Gets the min and max coordinates of the mesh vertices.
		///
		/// If the mesh doesn't have any position data, returns `None`.
		pub fn bounds(&self) -> Option<(Vector3<f32>, Vector3<f32>)> {
			let positions = self.positions()?;
			if positions.is_empty() {
				return None;
			}
			const INF: f32 = ::std::f32::INFINITY;

			let mut min = Vector3::new(INF, INF, INF);
			let mut max = Vector3::new(-INF, -INF, -INF);

			for vert in positions.iter() {
				for axis in 0..3 {
					if vert[axis] < min[axis] {
						min[axis] = vert[axis];
					}
					if vert[axis] > max[axis] {
						max[axis] = vert[axis];
					}
				}
			}
			Some((min, max))
		}
	}

	/// Types of attributes that make up a vertex.
	#[derive(Debug, Clone)]
	pub enum VertexAttribute {
		/// Vertex positions. Usually a mesh has one per vertex.
		Positions(Vec<Vector3<f32>>),
		/// Normal directions. Usually a mesh has zero or one per vertex.
		Normals(Vec<Vector3<f32>>),
		/// Tangents normal directions. Usually a mesh has zero or one per vertex.
		Tangents(Vec<Vector3<f32>>),
		/// Colors. Usually a mesh has zero or one per vertex.
		Colors(Vec<Vector4<f32>>),
		/// Texture positions. Mesh may have zero or more per vertex.
		TextureCoords(Vec<Vector2<f32>>),
		/// Bone weights. Usually a mesh has zero or one per vertex.
		///
		/// Keys are indexes into `Model.bones`. Values are from 0 to 1.
		BoneWeights(Vec<BTreeMap<usize, NonNaN>>),
	}
	impl VertexAttribute {
		/// Gets the position data, or None for other attributes.
		pub fn positions(&self) -> Option<&[Vector3<f32>]> {
			match self {
				VertexAttribute::Positions(v) => Some(v),
				_ => None,
			}
		}

		/// Gets the normal data, or None for other attributes.
		pub fn normals(&self) -> Option<&[Vector3<f32>]> {
			match self {
				VertexAttribute::Normals(v) => Some(v),
				_ => None,
			}
		}

		/// Gets the color data, or None for other attributes.
		pub fn colors(&self) -> Option<&[Vector4<f32>]> {
			match self {
				VertexAttribute::Colors(v) => Some(v),
				_ => None,
			}
		}

		/// Gets the texture coordinate data, or None for other attributes.
		pub fn texture_coords(&self) -> Option<&[Vector2<f32>]> {
			match self {
				VertexAttribute::TextureCoords(v) => Some(v),
				_ => None,
			}
		}

		/// Gets the bone weight data, or None for other attributes.
		pub fn bone_weights(&self) -> Option<&[BTreeMap<usize, NonNaN>]> {
			match self {
				VertexAttribute::BoneWeights(v) => Some(v),
				_ => None,
			}
		}

		/// Gets the length of the underlying array
		pub fn len(&self) -> usize {
			match self {
				VertexAttribute::Positions(v) => v.len(),
				VertexAttribute::Normals(v) => v.len(),
				VertexAttribute::Tangents(v) => v.len(),
				VertexAttribute::Colors(v) => v.len(),
				VertexAttribute::TextureCoords(v) => v.len(),
				VertexAttribute::BoneWeights(v) => v.len(),
			}
		}
	}

	/// Mesh face
	///
	/// All faces are triangles.
	#[derive(Debug, Clone)]
	pub struct Face {
		pub indices: [usize; 3],
		/// Face material, as an index into `Model.materials`.
		pub material: usize,
	}

	/// Mesh material
	#[derive(Debug, Clone)]
	pub struct Material {
		/// Material name
		pub name: String,
		/// Textures that make up the material
		pub textures: Vec<MaterialTexture>,
	}

	/// Mesh texture definition
	#[derive(Debug, Clone)]
	pub struct MaterialTexture {
		/// Texture name
		pub name: String,
		/// UV coordinate scale
		pub scale: Vector2<f32>,
		/// UV coordinate rotation (TODO: what units?)
		pub rotation: f32,
		/// UV coordinate translation
		pub translation: Vector2<f32>,
		/// Wrapping mode for U and V axises
		pub wrap_mode: [WrappingMode; 2],
	}

	/// Texture wrapping mode
	#[derive(Debug, Clone, Copy, PartialEq, Eq)]
	pub enum WrappingMode {
		ClampToEdge,
		ClampToBorder,
		Repeat,
		MirroredRepeat,
	}
}
