//! Module error types and utilities.
//!
//! Defines the `Error` type that this crate uses for parsing and IO errors.
//!
//! See the [`Error`](struct.Error.html) struct for more information.

use std::{
	cell::RefCell,
	error,
	fmt,
	io,
	iter,
	string::ToString,
};

use crate::nonnan::WasNaN;

/// Error kinds
#[derive(Debug)]
pub enum ErrorKind {
	/// An IO error occured while reading.
	IO(io::Error),
	/// A parsing error occured. For example, the parser got an an unrecognized
	/// value or ran out of data unexpectedly.
	Parsing(String),
	/// A parser encountered an unexpected NaN float in the data.
	WasNaN(WasNaN),
}

/// Struct for parsing errors.
///
/// To assist with debugging, the error structure stores the location in the file
/// being parsed. Use the `Location` object or the `with_loc` macro to push and pop
/// messages to be stored in the errors.
///
/// When converting IO errors using `From`, `ErrorKind::UnexpectedEof` errors are
/// converted to parsing errors with a prettier string, since this usually indicates
/// that the file is too small for the format. It also unwraps io errors containing
/// `Error` itself.
///
/// Some parsers may also emit warnings, by accepting as a parameter an object
/// that implements `Push<Error>`. The object can choose whether to log warnings
/// immediately (ex. by printing them or using the log as `LogWarningSink` does),
/// or store them to use later (ex. by showing a UI dialog).
#[derive(Debug)]
pub struct Error {
	kind: ErrorKind,
	place_stack: Vec<String>,
}
impl Error {
	/// Creates a new parsing error
	pub fn parsing<T: ToString>(desc: T) -> Self {
		Self {
			kind: ErrorKind::Parsing(desc.to_string()),
			place_stack: Location::stack(),
		}
	}

	/// Gets the underlying error
	pub fn kind(&self) -> &ErrorKind {
		&self.kind
	}

	/// Checks if the error is the result of an IO error.
	///
	/// IO errors are usually handled differently from other errors.
	pub fn is_io(&self) -> bool {
		match self.kind {
			ErrorKind::IO(_) => true,
			_ => false,
		}
	}
}
impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self.kind {
			ErrorKind::IO(ref err) => err.fmt(f)?,
			ErrorKind::WasNaN(ref err) => write!(f, "Parse error: {}", err)?,
			ErrorKind::Parsing(ref s) => write!(f, "Parse error: {}", s)?,
		};
		for place in self.place_stack.iter().rev() {
			write!(f, "\n...while parsing {}", place)?;
		}
		Ok(())
	}
}
impl From<io::Error> for Error {
	fn from(err: io::Error) -> Self {
		if err.kind() == io::ErrorKind::UnexpectedEof {
			return Self::parsing("Unexpected end of file while parsing");
		}
		// Is this IO Error wrapping our own Error? If so, just unwrap it
		if err.get_ref().map(|err| err.is::<Self>()).unwrap_or(false) {
			return *err.into_inner().unwrap().downcast::<Self>().unwrap();
		}
		return Self {
			kind: ErrorKind::IO(err),
			place_stack: Location::stack(),
		};
	}
}
impl From<WasNaN> for Error {
	fn from(err: WasNaN) -> Self {
		Self {
			kind: ErrorKind::WasNaN(err),
			place_stack: Location::stack(),
		}
	}
}
impl error::Error for Error {
	fn cause(&self) -> Option<&dyn error::Error> {
		match self.kind {
			ErrorKind::IO(ref err) => Some(err),
			ErrorKind::Parsing(_) => None,
			ErrorKind::WasNaN(ref err) => Some(err),
		}
	}
}

/// Result where the error is `Error`.
pub type Result<T> = ::std::result::Result<T, Error>;

/// Trait for inserting single objects into a collection or other container.
///
/// Automatically implemented for any type that implements `Extend`. This crate
/// mostly uses this trait for reporting warnings.
///
/// # Example
/// ```
/// # use pokemon_3ds::error::Push;
/// fn do_operation<Warnings: Push<String>>(warnings: &mut Warnings) {
/// 	// while doing an operation...
/// 	warnings.push("Tried to divide by zero, using placeholder instead".to_string());
/// }
///
/// let mut warnings = vec![];
/// do_operation(&mut warnings);
/// ```
pub trait Push<T> {
	/// Handles one element, in an implementation defined way.
	///
	/// For objects implementing this trait via `Extend`, this calls `extend`
	/// with a single-item iterator containing the passed value.
	fn push(&mut self, value: T);
}
impl<T, A: iter::Extend<T>> Push<T> for A {
	fn push(&mut self, value: T) {
		// Some(v) is a convenient IntoIterator with one value
		self.extend(Some(value))
	}
}

/// An object that implements `Extend<Error>` (and thus `Push<Error>`), logging
/// each error using the `warn` macro as soon as an error is received.
/// # Example
/// ```
/// # use pokemon_3ds::error::{Push, Error, LogWarningSink};
/// fn do_operation<Warnings: Push<Error>>(warnings: &mut Warnings) {
/// 	// while doing an operation...
/// 	warnings.push(Error::parsing("Tried to divide by zero, using placeholder instead"));
/// }
///
/// do_operation(&mut LogWarningSink); // will log warnings as they are emitted
/// ```
pub struct LogWarningSink;
impl Extend<Error> for LogWarningSink {
	fn extend<I: iter::IntoIterator<Item = Error>>(&mut self, iter: I) {
		for warning in iter.into_iter() {
			warn!("{}", warning);
		}
	}
}

thread_local! {
	/// Stack of locations in the current code flow
	static LOCATION_STACK: RefCell<Vec<String>> = RefCell::new(Vec::new());
}

/// Error location manager.
///
/// Creating a Location pushes a string to a thread-local stack. Dropping it
/// will pop from the stack. When an Error is created, it copies the location stack
/// for more info.
///
/// Note: Keep these Location objects on the stack. Moving the Locations will mess with
/// the order and cause Drop to panic.
///
/// This is a bit hack-y with the global variables, but it's cleaner than adding boilerplate to
/// every error in order to annotate it with the location information.
pub struct Location(usize);
impl Location {
	/// Outlines a new location.
	pub fn new<S: ToString>(location: S) -> Self {
		let my_index = LOCATION_STACK.with(|stack| {
			let mut stack = stack.borrow_mut();
			stack.push(location.to_string());
			stack.len()
		});

		Self(my_index)
	}

	/// Copies the location stack. Last value is most nested.
	pub fn stack() -> Vec<String> {
		LOCATION_STACK.with(|stack| stack.borrow().clone())
	}
}
impl Drop for Location {
	fn drop(&mut self) {
		LOCATION_STACK.with(|stack| {
			let mut stack = stack.borrow_mut();
			if stack.len() != self.0 {
				panic!("Unbalanced location drops. Was expecting stack to have {} elements but the stack has {}. Are you moving a Location off the stack? Location Stack: {:?}",
					self.0, stack.len(), &*stack);
			}
			stack.pop();
		});
	}
}

/// Helper for annotating an expression with a location.
macro_rules! with_loc {
	($location:expr, $expr:expr) => {{
		let _loc = $crate::error::Location::new($location);
		$expr
		}};
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::io;

	#[test]
	fn error_to_io_error_and_back() {
		let err1 = with_loc!("somewhere", Error::parsing("you broke it"));
		let err2 = io::Error::new(io::ErrorKind::Other, err1);
		let err3 = Error::from(err2);
		assert!(!err3.is_io());
		assert_eq!(&err3.place_stack, &["somewhere"]);
	}
}
