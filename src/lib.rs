//! Crate for parsing models, textures, and other data from the 3DS Pokemon
//! games (and possibly other games as well).

#[cfg(test)]
#[macro_use]
extern crate approx;
#[macro_use]
extern crate log;
#[macro_use]
extern crate num_derive;
extern crate byteorder;
extern crate nalgebra;
extern crate num_traits;
extern crate png;
extern crate rgb;
extern crate xml;

#[macro_use]
pub mod error;
pub mod formats;
pub mod ioutils;
pub(crate) mod lzss;
pub mod nonnan;
pub(crate) mod parsing;
pub mod tree;
