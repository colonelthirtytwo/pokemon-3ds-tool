use byteorder::{
	LittleEndian,
	ReadBytesExt,
};
use std::io::{
	self,
	Read,
};

use crate::error::Error;

/*  Data header (32bit)
	  Bit 0-3   Reserved
	  Bit 4-7   Compressed type (must be 1 for LZ77)
	  Bit 8-31  Size of decompressed data. if 0, the next 4 bytes are decompressed length
	Repeat below. Each Flag Byte followed by eight Blocks.
	Flag data (8bit)
	  Bit 0-7   Type Flags for next 8 Blocks, MSB first
	Block Type 0 - Uncompressed - Copy 1 Byte from Source to Dest
	  Bit 0-7   One data byte to be copied to dest
	Block Type 1 - Compressed - Copy LEN Bytes from Dest-Disp-1 to Dest
		If Reserved is 0: - Default
		  Bit 0-3   Disp MSBs
		  Bit 4-7   LEN - 3
		  Bit 8-15  Disp LSBs
		If Reserved is 1: - Higher compression rates for files with (lots of) long repetitions
		  Bit 4-7   Indicator
			If Indicator > 1:
				Bit 0-3    Disp MSBs
				Bit 4-7    LEN - 1 (same bits as Indicator)
				Bit 8-15   Disp LSBs
			If Indicator is 1: A(B CD E)(F GH)
				Bit 0-3     (LEN - 0x111) MSBs
				Bit 4-7     Indicator; unused
				Bit 8-15    (LEN- 0x111) 'middle'-SBs
				Bit 16-19   Disp MSBs
				Bit 20-23   (LEN - 0x111) LSBs
				Bit 24-31   Disp LSBs
			If Indicator is 0:
				Bit 0-3     (LEN - 0x11) MSBs
				Bit 4-7     Indicator; unused
				Bit 8-11    Disp MSBs
				Bit 12-15   (LEN - 0x11) LSBs
				Bit 16-23   Disp LSBs
*/

enum DecodeState {
	NoRemaining,
	DecompressionRemaining { offset: usize, length: usize },
}

/// Wraps an `io::Read` object to perform LZSS decompression on the data.
///
/// Since the LZSS format encodes the data size in the stream, reading will throw
/// an error if the underlying stream is too small. It will not throw an error if
/// the stream is too big; it will leave the stream one past the last needed byte.
pub struct LzssDecompressor<Reader: Read> {
	reader: Reader,

	decompressed_size: usize,
	num_read: usize,

	state: DecodeState,

	dictionary: Vec<u8>,
	dictionary_cursor: usize,

	flags: u8,
	remaining_flag_bits: u8,
}
impl<Reader: Read> LzssDecompressor<Reader> {
	pub fn new(mut reader: Reader) -> io::Result<Self> {
		// Read reserved+compressed type
		let magic = reader.read_u8()?;
		if magic != 0x11 {
			return Err(io::Error::new(
				io::ErrorKind::Other,
				Error::parsing("incorrect LZSS magic number"),
			));
		}

		// Read size
		let short_size = reader.read_u24::<LittleEndian>()?;
		let size = if short_size != 0 {
			short_size as usize
		} else {
			reader.read_u32::<LittleEndian>()? as usize
		};

		Ok(Self {
			reader: reader,
			state: DecodeState::NoRemaining,

			decompressed_size: size,
			num_read: 0,

			dictionary: vec![0; 4096],
			dictionary_cursor: 0,

			flags: 0,
			remaining_flag_bits: 0,
		})
	}
}
impl<Reader: Read> LzssDecompressor<Reader> {
	/// Returns the size of the decompressed data.
	///
	/// Includes bytes that have already been read.
	pub fn decompressed_total_size(&self) -> usize {
		self.decompressed_size
	}

	fn get_compressed_block_info(&mut self) -> io::Result<(usize, usize)> {
		let byte1 = self.reader.read_u8()? as usize;
		let indicator = (byte1 & 0xF0) >> 4;
		let (offset, length) = match indicator {
			0 => {
				// data = AB CD EF (with A=0)
				// LEN = ABC + 0x11 == BC + 0x11
				// DISP = DEF + 1
				let byte2 = self.reader.read_u8()? as usize;
				let byte3 = self.reader.read_u8()? as usize;
				let offset = (((byte2 & 0x0F) << 8) | byte3) + 0x1;
				let length = (((byte1 & 0x0F) << 4) | (byte2 >> 4)) + 0x11;
				(offset, length)
			}
			1 => {
				// data = AB CD EF GH (with A=1)
				// LEN = BCDE + 0x111
				// DISP = FGH + 1

				let byte2 = self.reader.read_u8()? as usize;
				let byte3 = self.reader.read_u8()? as usize;
				let byte4 = self.reader.read_u8()? as usize;

				let offset = (((byte3 & 0x0F) << 8) | byte4) + 0x1;
				let length = (((byte1 & 0x0F) << 12) | (byte2 << 4) | (byte3 >> 4)) + 0x111;
				(offset, length)
			}
			_ => {
				// case other:
				// data = AB CD
				// LEN = A + 1
				// DISP = BCD + 1

				let byte2 = self.reader.read_u8()? as usize;

				let offset = (((byte1 & 0x0F) << 8) | byte2) + 0x1;
				let length = ((byte1 & 0xF0) >> 4) + 0x1;
				(offset, length)
			}
		};
		return Ok((offset, length));
	}
}
impl<Reader: Read> Read for LzssDecompressor<Reader> {
	fn read(&mut self, out: &mut [u8]) -> io::Result<usize> {
		if out.len() == 0 || self.num_read == self.decompressed_size {
			return Ok(0);
		}

		let num_read_before_call = self.num_read;
		let mut out_cursor = 0;
		while out_cursor < out.len() && self.num_read < self.decompressed_size {
			self.state = match self.state {
				DecodeState::NoRemaining => {
					// Read next block from the input

					// If we have no in progress flags remaming, get another
					if self.remaining_flag_bits == 0 {
						self.flags = self.reader.read_u8()?;
						self.remaining_flag_bits = 8;
					}

					// Check if this upcoming block is compressed
					let is_compressed_block = (self.flags & 0x80) != 0;
					self.flags <<= 1;
					self.remaining_flag_bits -= 1;

					if is_compressed_block {
						// Parse block and begin decompression
						let (offset, length) = self.get_compressed_block_info()?;

						if offset > self.num_read {
							return Err(io::Error::new(io::ErrorKind::Other, Error::parsing("Invalid LZSS data: Compressed block tried to read before beginning of file")));
						}

						DecodeState::DecompressionRemaining { offset, length }
					} else {
						// Read one uncompressed byte
						let data = self.reader.read_u8()?;

						out[out_cursor] = data;
						out_cursor += 1;
						self.num_read += 1;
						self.dictionary[self.dictionary_cursor] = data;
						self.dictionary_cursor =
							(self.dictionary_cursor + 1) % self.dictionary.len();

						DecodeState::NoRemaining
					}
				}
				DecodeState::DecompressionRemaining { offset, length } => {
					// Decompression in progress, read until the block is over
					// or we run out of output buffer space.
					if length == 0 {
						DecodeState::NoRemaining
					} else {
						let dictionary_idx = (self.dictionary_cursor + self.dictionary.len()
							- offset) % self.dictionary.len();
						let data = self.dictionary[dictionary_idx];

						out[out_cursor] = data;
						out_cursor += 1;
						self.num_read += 1;
						self.dictionary[self.dictionary_cursor] = data;
						self.dictionary_cursor =
							(self.dictionary_cursor + 1) % self.dictionary.len();

						DecodeState::DecompressionRemaining {
							offset,
							length: length - 1,
						}
					}
				}
			};
		}
		return Ok(self.num_read - num_read_before_call);
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	const UNCOMPRESSED_FILE: &'static [u8] = b"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed finibus, libero at tristique faucibus, magna odio iaculis orci, quis lacinia diam diam et nisi. Curabitur a condimentum ante. Integer auctor vulputate tellus nec egestas. Praesent non imperdiet massa. Etiam eu vulputate mauris. In fermentum nibh turpis, a condimentum ex pretium ut. Pellentesque ut hendrerit leo. Pellentesque feugiat at sem vitae interdum. Sed ornare est at urna commodo, at varius odio fermentum. Nunc vulputate eleifend augue vel ultrices. Sed et nisl nec massa ornare faucibus.\n\nAliquam aliquam iaculis sapien. Etiam a metus eleifend, tincidunt sapien congue, suscipit libero. Aenean finibus egestas aliquam. Morbi dictum quam vitae nisl porttitor laoreet. Ut ac porttitor purus, id sagittis velit. Proin fringilla feugiat sem condimentum molestie. Morbi in leo sapien. Morbi et nulla sit amet quam pretium lacinia. Duis non nisi varius, molestie orci eu, convallis lorem. Fusce pretium maximus ullamcorper. Phasellus euismod placerat lorem id.";
	const COMPRESSED_FILE: &'static [u8] = b"\x11\x02\x04\x00\x00\x4c\x6f\x72\x65\x6d\x20\x69\x70\x00\x73\x75\x6d\x20\x64\x6f\x6c\x6f\x00\x72\x20\x73\x69\x74\x20\x61\x6d\x00\x65\x74\x2c\x20\x63\x6f\x6e\x73\x00\x65\x63\x74\x65\x74\x75\x72\x20\x00\x61\x64\x69\x70\x69\x73\x63\x69\x00\x6e\x67\x20\x65\x6c\x69\x74\x2e\x00\x20\x53\x65\x64\x20\x66\x69\x6e\x00\x69\x62\x75\x73\x2c\x20\x6c\x69\x00\x62\x65\x72\x6f\x20\x61\x74\x20\x00\x74\x72\x69\x73\x74\x69\x71\x75\x02\x65\x20\x66\x61\x75\x63\x50\x1d\x6d\x00\x61\x67\x6e\x61\x20\x6f\x64\x69\x00\x6f\x20\x69\x61\x63\x75\x6c\x69\x00\x73\x20\x6f\x72\x63\x69\x2c\x20\x24\x71\x75\x20\x0a\x6c\x61\x20\x55\x69\x61\x0c\x20\x64\x69\x61\x20\x82\x30\x04\x65\x74\x00\x20\x6e\x69\x73\x69\x2e\x20\x43\x06\x75\x72\x61\x62\x69\x40\x7e\x30\x8c\x64\x04\x69\x6d\x65\x6e\x74\x20\xa8\x61\x6e\x04\x74\x65\x2e\x20\x49\x20\x05\x67\x65\x88\x20\x9a\x75\x63\x74\x20\xb7\x76\x75\x6c\x00\x70\x75\x74\x61\x74\x65\x20\x74\x00\x65\x6c\x6c\x75\x73\x20\x6e\x65\x20\x63\x20\x20\x20\x73\x74\x61\x73\x2e\x02\x20\x50\x72\x61\x65\x73\x20\x3d\x20\x00\x6e\x6f\x6e\x20\x69\x6d\x70\x65\x10\x72\x64\x69\x20\x67\x6d\x61\x73\x73\x05\x61\x2e\x20\x45\x74\x40\x77\x75\xa0\x44\x18\x6d\x61\x75\x20\xca\x30\x65\x20\x66\x65\x66\x72\x60\x78\x20\xee\x68\x20\x21\x0e\x21\x0a\x2c\x80\xe0\x93\x65\x78\x20\x70\x72\x65\x74\x56\x69\x21\x47\x75\x21\x1e\x50\x20\x83\x20\xac\x65\x40\x73\x31\x0a\x75\x74\x20\x68\x65\x6e\x08\x64\x72\x65\x72\x21\x5b\x6c\x65\x6f\x83\xe0\x1e\x66\x65\x75\x67\x69\x21\x3b\x21\x3e\x40\x73\x21\x8c\x76\x69\x74\x61\x65\x20\x42\x69\x20\xe3\x72\x64\x75\x6d\x51\x69\x6f\x03\x72\x6e\x61\x72\x65\x20\x20\xcc\x31\x64\x60\x75\x20\x0d\x21\x9d\x6d\x6d\x6f\x64\x6f\x81\x20\x87\x74\x20\x76\x61\x72\x69\x20\xf1\xc0\x41\x62\x80\xad\x2e\x20\x4e\x75\x6e\x63\x83\xa1\x17\x65\x6c\x65\x69\x66\x20\x87\x21\x31\x40\x67\x21\xa0\x76\x65\x6c\x20\x75\x6c\x9d\x21\xb0\x63\x65\x21\x20\x31\xcf\x51\x78\x6c\x41\x39\xe0\x41\x19\x70\x77\x71\xcb\x2e\x0a\x0a\x41\x6c\xd8\x21\xdd\x21\xa9\x61\x60\x07\x71\xd1\x73\x61\x70\x13\x69\x65\x6e\x71\x4a\x61\x20\x22\x41\x21\x83\x80\x70\x72\x2c\x20\x74\x69\x6e\x63\x69\x3c\x64\x75\x21\x81\x50\x29\x32\x5d\x20\x85\x2c\x20\x2c\x73\x75\x22\x56\x70\x31\x18\x42\x44\x2e\x20\x07\x41\x65\x6e\x65\x61\x21\x75\x52\x5c\x71\xc2\x80\x70\x70\x2e\x20\x4d\x6f\x72\x62\x69\xbc\x22\x2e\x63\x32\x0a\x40\x8b\x51\x31\x40\xba\x70\x6f\x09\x72\x74\x74\x69\x32\x0f\x6c\x61\x22\xda\x54\x65\x22\xa9\x55\x22\xcf\x63\xa0\x18\x70\x75\x49\x72\x32\xb3\x69\x64\x20\xb3\x67\x69\x20\x2d\x61\x73\x31\x0a\x32\xd3\x50\x72\x6f\x69\x21\xee\x43\x72\x22\xe5\x69\x6c\x6c\x61\x81\x98\x31\x95\x8b\xb2\x7f\x6d\x6f\x6c\x22\x56\x69\x22\x83\x50\x8b\xfb\x20\x34\x21\xd5\x81\x02\x50\xa0\x32\xc5\x75\x30\x45\x73\x50\xe3\x50\xab\x72\x24\x62\xf6\x2e\x20\x44\x33\x04\x32\x95\xfe\x32\xf3\x61\xc9\x23\x33\x60\x62\x43\x29\x22\x99\x43\x8d\x76\x35\x61\x6c\x33\x3c\x23\xa5\x65\x22\x17\x46\x21\x38\x41\x65\x82\x74\x6d\x61\x78\x69\x6d\x22\xf5\x86\x30\x72\x6d\x63\x6f\x72\x22\xe1\x22\xf5\x68\x2c\x61\x73\x53\x0c\x65\x23\x6a\x22\x2e\x20\x70\x9c\x23\x6e\x65\x72\x23\xa8\x40\x44\x21\x04\x2e";

	#[test]
	fn decompress_lorem() {
		let decompressor = LzssDecompressor::new(io::Cursor::new(COMPRESSED_FILE)).unwrap();
		//let mut buffer: Vec<u8> = vec![];
		//decompressor.read_to_end(&mut buffer).unwrap();

		for (i, (expected_byte, actual_byte)) in UNCOMPRESSED_FILE
			.iter()
			.zip(decompressor.bytes())
			.enumerate()
		{
			let expected_byte = *expected_byte;
			let actual_byte = actual_byte.unwrap();
			assert_eq!(
				expected_byte, actual_byte,
				"Bytes at {} differ: expected 0x{:02x}, got 0x{:02x}",
				i, expected_byte, actual_byte
			);
		}
	}
}
