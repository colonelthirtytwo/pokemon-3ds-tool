//! Convenience utilities for working with files

use byteorder::ReadBytesExt;
use std::{
	cmp,
	io::{
		self,
		Read,
	},
};

/// Convenience trait that is a union of `io::Read` and `io::Seek`.
///
/// You can use `ReadSeek` as a trait object. For example, you can write
/// `Box<dyn ReadSeek>`, as opposed to `Box<dyn io::Read+io::Seek>` which won't
/// compile.
pub trait ReadSeek: io::Read + io::Seek {}
impl<T: io::Read + io::Seek> ReadSeek for T {}

/// Reads a string with a 1-byte length prefix.
///
/// Uses `String::from_utf8_lossy` for converting bytes.
pub(crate) fn read_pascal_str_u8<Reader: Read>(reader: &mut Reader) -> io::Result<String> {
	let len = reader.read_u8()?;
	let mut bytes = vec![0; len as usize];
	reader.read_exact(&mut bytes)?;
	Ok(String::from_utf8_lossy(&bytes).into_owned())
}

/// Reads up to `amount` bytes from `reader` and discards them.
///
/// # Returns
/// The actual amount skipped. May be less than `amount` if the end of the file
/// was reached.
pub(crate) fn skip<Reader: Read>(reader: &mut Reader, amount: u64) -> io::Result<u64> {
	let mut vec = vec![0; 4096];
	let mut total_read = 0u64;
	while total_read < amount {
		let num_to_read = cmp::min(amount, vec.len() as u64) as usize;
		let num_read = reader.read(&mut vec[0..num_to_read])?;
		if num_read == 0 {
			break;
		}
		total_read += num_read as u64;
	}
	return Ok(total_read);
}
