//! Wrapper for a `f32` that isn't NaN.

use std::{
	cmp,
	convert,
	error,
	fmt,
	hash,
	ops,
};

/// Error returned when trying to make a `NonNaN` out of a NaN value.
#[derive(Debug, Clone, Copy)]
pub struct WasNaN();
impl fmt::Display for WasNaN {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str("Value was NaN")
	}
}
impl error::Error for WasNaN {}

/// f32 that can't be NaN. Implements `Eq`, `Ord`, and `Hash`.
///
/// # Example
/// ```
/// # use pokemon_3ds::nonnan::NonNaN;
///
/// let a = NonNaN::new(123.0).expect("value was nan");
/// let b = NonNaN::assume(42.5); // Same as above
///
/// // Implements Ord, works with cmp functions
/// use std::cmp;
/// assert_eq!(cmp::min(a, b), 42.5);
///
/// // Implements Eq and Hash, works as keys
/// use std::collections::HashMap;
/// let mut map: HashMap<NonNaN, String> = HashMap::new();
/// map.insert(a, "hello".to_string());
/// map.insert(b, "world".to_string());
///
/// assert_eq!(map[&NonNaN::assume(42.5)], "world");
/// ```
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct NonNaN {
	v: f32,
}
impl NonNaN {
	/// Wraps a float into a NonNaN.
	///
	/// Returns the created if the float was not NaN, otherwise
	/// return an error.
	///
	/// # Example
	/// ```
	/// # use pokemon_3ds::nonnan::NonNaN;
	///
	/// assert!(NonNaN::new(123.0).is_ok());
	/// assert!(NonNaN::new(0.0/0.0).is_err());
	/// ```
	pub fn new(v: f32) -> Result<Self, WasNaN> {
		if !v.is_nan() {
			Ok(Self { v })
		} else {
			Err(WasNaN())
		}
	}

	/// Wraps a float into a NonNaN, panicing if it was NaN.
	pub fn assume(v: f32) -> Self {
		Self::new(v).expect("assume was passed a NaN float")
	}
}
impl convert::Into<f32> for NonNaN {
	fn into(self) -> f32 {
		self.v
	}
}
impl ops::Deref for NonNaN {
	type Target = f32;

	fn deref(&self) -> &f32 {
		&self.v
	}
}
impl fmt::Display for NonNaN {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		self.v.fmt(f)
	}
}
impl cmp::Eq for NonNaN {}
impl cmp::Ord for NonNaN {
	fn cmp(&self, other: &Self) -> cmp::Ordering {
		self.partial_cmp(other)
			.expect("f32.partial_cmp on NonNaN did not return an ordering")
	}
}
impl cmp::PartialEq<f32> for NonNaN {
	fn eq(&self, other: &f32) -> bool {
		self.v == *other
	}
}
impl cmp::PartialOrd<f32> for NonNaN {
	fn partial_cmp(&self, other: &f32) -> Option<cmp::Ordering> {
		self.v.partial_cmp(other)
	}
}
impl hash::Hash for NonNaN {
	fn hash<H: hash::Hasher>(&self, state: &mut H) {
		self.v.to_bits().hash(state)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::collections::HashMap;

	#[test]
	fn hash_map_of_nonnan() {
		let mut map: HashMap<NonNaN, u32> = HashMap::new();

		let v1 = NonNaN::new(5.0).unwrap();
		map.insert(v1, 123);

		let v2 = NonNaN::new(5.0).unwrap();

		assert!(map.contains_key(&v2));
		assert_eq!(map[&v2], 123);
	}
}
